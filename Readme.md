# chainmaker-tee介绍
ChainMaker-tee是和长安链·ChainMaker结合执行隐私合约的计算网关，负责初始化隐私计算环境以及完成长安链·ChainMaker交互完成合约部署、合约调用和
远程证明等功能。ChainMaker-tee在隐私计算环境的Enclave中基于TinyEvm(https://github.com/chrpro/TinyEVM, 一个C语言的evm虚拟机实现)
做了一些完善用于执行隐私合约。另外在Enclave代码中还参考使用hashmap.h(https://github.com/sheredom/hashmap.h) 项目。

# chainmaker-tee编译和启动

## 根据Enclave.edl生成Enclave_t.c等文件

```bash
cd Enclave
/opt/intel/sgxsdk/bin/x64/sgx_edger8r --untrusted ./Enclave.edl --search-path . --search-path /opt/intel/sgxsdk/include  --search-path ./openssl/include

/opt/intel/sgxsdk/bin/x64/sgx_edger8r --trusted ./Enclave.edl --search-path . --search-path /opt/intel/sgxsdk/include  --search-path ./openssl/include
```

## 复制Enclave_u.c和Enclave_u.h到gateway/bridge下

```bash
手动删除 Enclave_u.h 中的第8行 :  #include "sgx_edger8r.h"

cp Enclave_u.* ../gateway/bridge
cp user_types.* ../gateway/bridge
```

## 生成cgo代码
```bash
cd ../gateway/bridge
go tool cgo bridge.go
```

## 编译Enclave

```bash
cd ../../Enclave
make clean
cmake .
make
```

## 对libenclave.so进行签名

```bash
/opt/intel/sgxsdk/bin/x64/sgx_sign sign -key ./Enclave_private.pem -enclave libenclave.so -out ./enclave.signed.so -config ./Enclave.config.xml
```

# 编译隐私计算网关

```
cd ../gateway
go build 
```

# 启动隐私计算网关
```
./gateway start
```
