/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "uint256.h"

#ifndef TEEEVM_ENCLAVE_OCALL_H
#define TEEEVM_ENCLAVE_OCALL_H

void init_ocall();
void ocall_get(char* contractName, char* key, char** result, uint32_t* result_len);

#endif //TEEEVM_ENCLAVE_OCALL_H
