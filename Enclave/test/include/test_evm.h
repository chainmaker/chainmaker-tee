/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef TEEEVM_TEST_EVM_H
#define TEEEVM_TEST_EVM_H

int contract_deploy(char *const contract_name,
                    uint8_t *contract_data, uint32_t contract_size,
                    struct hashmap_s* read_set,  struct hashmap_s* write_set,
                    uint8_t *p_deployed_contract_data, uint32_t *p_deployed_contract_size);

int contract_call(char *const contract_name,
                  uint8_t *contract_data, uint32_t contract_size,
                  struct hashmap_s* read_set,  struct hashmap_s* write_set,
                  uint8_t *message_data, int message_len);

void test_evm_deploy_and_call();

#endif //TEEEVM_TEST_EVM_H
