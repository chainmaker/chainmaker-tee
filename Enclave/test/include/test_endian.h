//
// Created by caizhihong on 2021/6/25.
//

#ifndef TEEEVM_TEST_ENDIAN_H
#define TEEEVM_TEST_ENDIAN_H

void test_uint16();
void test_uint32();
void test_uint64();

#endif //TEEEVM_TEST_ENDIAN_H
