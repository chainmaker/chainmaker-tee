/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef TEEEVM_TEST_HEX_H
#define TEEEVM_TEST_HEX_H

void test_hex_to_bytes();

#endif //TEEEVM_TEST_HEX_H
