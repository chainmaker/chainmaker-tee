/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdlib.h>
#include "enclave_ocall.h"
#include "test_evm.h"
#include "test_endian.h"

int main() {
    //init_ocall();
    //test_evm_deploy_and_call("../testdata/test.hex");
    test_uint64();

    return 0;
}
