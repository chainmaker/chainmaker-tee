/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdlib.h>
#include "uint256.h"
#include "hashmap.h"

struct hashmap_s global_storage;

void init_ocall() {
    hashmap_create(64, &global_storage);
}

void destroy_ocall() {
    hashmap_destroy(&global_storage);
}

int ocall_get(char* contractName, char* key, char** result, uint32_t* result_len) {
    uint8_t *p_value = hashmap_get(&global_storage, key, strlen(key));
    if(p_value == HASHMAP_NULL) {
        p_value = malloc(32);
        if (p_value == NULL) {
            return SGX_ERROR_OUT_OF_MEMORY;
        }
        memset(p_value, '\0', 32);
    }

    *result = p_value;
    *result_len = 32 * sizeof(uint8_t);
    return 0;
}