/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "hex.h"
#include "hashmap.h"
#include "evm.h"

#define CONTRACT_MAX_LEN 40960

int contract_deploy(char *const contract_name,
                    uint8_t *contract_data, uint32_t contract_size,
                    struct hashmap_s* read_set,  struct hashmap_s* write_set,
                    uint8_t *p_deployed_contract_data, uint32_t *p_deployed_contract_size) {

    Machine MAIN_VM;

    // 设置读写集&合约部署后的内存地址
    init_machine(&MAIN_VM, read_set, write_set);

    // 设置调用参数相关的数据
    MAIN_VM.message.datasize = 0;

    // 设置代码段相关的数据
    MAIN_VM.message.codesize = contract_size;

    // 设置参数
    UPPER(UPPER(MAIN_VM.message.call_value)) = 0x00;
    UPPER(LOWER(MAIN_VM.message.call_value)) = 0x00;
    LOWER(UPPER(MAIN_VM.message.call_value)) = 0x00;
    LOWER(LOWER(MAIN_VM.message.call_value)) = 0x00;

    // 使用 EVM 初始化合约
    execute_contract(&MAIN_VM, contract_name, contract_data, contract_size);

    // 复制出部署后的合约
    memcpy(p_deployed_contract_data, MAIN_VM.result_data_ptr, MAIN_VM.result_data_size);
    *p_deployed_contract_size = MAIN_VM.result_data_size;

    // shutdown_machine(&MAIN_VM);
    return 0;
}

int contract_call(char *const contract_name,
                  uint8_t *deployed_contract_data, uint32_t deployed_contract_size,
                  struct hashmap_s* read_set,  struct hashmap_s* write_set,
                  uint8_t *message_data, int message_len) {

    Machine MAIN_VM;

    // 设置读写集
    init_machine(&MAIN_VM, read_set, write_set);

    // 设置调用参数相关的数据
    memcpy(MAIN_VM.message.data, message_data, message_len);
    MAIN_VM.message.datasize = message_len;

    // 设置代码段相关的数据
    MAIN_VM.message.codesize = deployed_contract_size;

    // 设置参数
    UPPER(UPPER(MAIN_VM.message.call_value)) = 0x00;
    UPPER(LOWER(MAIN_VM.message.call_value)) = 0x00;
    LOWER(UPPER(MAIN_VM.message.call_value)) = 0x00;
    LOWER(LOWER(MAIN_VM.message.call_value)) = 0x00;

    // 使用 EVM 执行合约函数
    execute_contract(&MAIN_VM, contract_name, deployed_contract_data, deployed_contract_size);
    // shutdown_machine(&MAIN_VM);

    return 0;
}

uint64_t read_contract(char *filename, uint8_t *buf, uint8_t *contract_data) {
    FILE *fd = fopen(filename, "r");
    if(fd == NULL) {
        printf("open contract file '%s' error.", filename);
        return -1;
    }

    int num;
    if((num = fread(buf, sizeof(char), CONTRACT_MAX_LEN*2, fd)) <= 0) {
        printf("read contract file '%s' error.", filename);
        return -1;
    }

    if(decode_hex(buf, num, contract_data) != 0) {
        printf("convert contract file to byte '%s' error.", filename);
        return -1;
    }

    fclose(fd);
    return num/2;
}



void test_evm_deploy_and_call(const char* contract_filename) {

    // 加载代码段
    uint8_t *contract_data = (uint8_t *)malloc(CONTRACT_MAX_LEN);
    uint8_t *buf = (uint8_t *)malloc(CONTRACT_MAX_LEN * 2);
    uint64_t contract_size;
    if((contract_size = read_contract(contract_filename,
                                      buf, contract_data)) <= 0) {
        printf("read contract file error: %s \n", contract_filename);
        return;
    }

    // 准备部署代码
    char *deploy_message = "";
    int deploy_message_len = strlen(deploy_message);
    uint8_t deploy_message_data[deploy_message_len/2];
    if(decode_hex(deploy_message, deploy_message_len, deploy_message_data) != 0) {
        printf("read message hex data error \n");
        return;
    }

    // 准备读写集
    struct hashmap_s read_set;
    struct hashmap_s write_set;
    hashmap_create(64, &read_set);
    hashmap_create(64, &write_set);

    // 部署
    uint8_t deployed_contract_data[CONTRACT_MAX_LEN];
    uint32_t deployed_contract_size;
    contract_deploy("contract_name",
                    contract_data, contract_size,
                    &read_set, &write_set,
                    deployed_contract_data, &deployed_contract_size);

    printf("read_set => \n");
    hashmap_iterate_pairs(&read_set, print_elem, NULL);
    printf("write_set => \n");
    hashmap_iterate_pairs(&write_set, print_elem, NULL);

    // hashmap_destroy(&read_set);
    // hashmap_destroy(&write_set);
    printf("\n===============================================================\n");
    printf("||                        合约部署完成                          ||\n");
    printf("===============================================================\n\n");


    // 准备调用参数
    char *call_message = "771602f700000000000000000000000000000000000000000000000000000000000000050000000000000000000000000000000000000000000000000000000000000006"; // test.hex
    // char *call_message = "0121b93f0000000000000000000000000000000000000000000000000000000000000002"; // ballot.hex
    // test_mapping.hex
    // char *call_message = "1313be1c00000000000000000000000000000000000000000000000000000000000000030000000000000000000000000000000000000000000000000000000000000005";
    int call_message_len = strlen(call_message);
    uint8_t call_message_data[call_message_len/2];
    if (decode_hex(call_message, call_message_len, call_message_data) != 0) {
        printf("read message hex data error: %s \n");
        return;
    }

    // hashmap_create(64, &read_set);
    // hashmap_create(64, &write_set);
    contract_call("contract_name",
                  deployed_contract_data, deployed_contract_size,
                  &read_set, &write_set,
                  call_message_data, call_message_len/2);

    printf("read_set => \n");
    hashmap_iterate_pairs(&read_set, print_elem, NULL);
    printf("write_set => \n");
    hashmap_iterate_pairs(&write_set, print_elem, NULL);

    free(contract_data);
    free(buf);
    hashmap_destroy(&read_set);
    hashmap_destroy(&write_set);
}