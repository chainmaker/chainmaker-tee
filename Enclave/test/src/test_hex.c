/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdio.h>
#include <stdlib.h>
#include "hex.h"

void test_hex_to_bytes() {
    const char hex_data_lower[] = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    const char hex_data_upper[] = {'0','1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    uint8_t contract_data[8] = {0};
    if(decode_hex(hex_data_lower, 16, contract_data) != 0) {
        enclave_printf("convert hex char to bytes error\n");
        return;
    }

    for(int i = 0; i < 8; i++) {
        enclave_printf("%02X", contract_data[i]);
    }
    enclave_printf("\n\n");

    if(decode_hex(hex_data_upper, 16, contract_data) != 0) {
        enclave_printf("convert hex char to bytes error\n");
        return;
    }

    for(int i = 0; i < 8; i++) {
        enclave_printf("%02X", contract_data[i]);
    }
    enclave_printf("\n\n");
}
