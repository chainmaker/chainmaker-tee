//
// Created by caizhihong on 2021/6/25.
//

#include <stdint.h>
#include "little_endian.h"
#include "test_endian.h"

void test_uint16() {
    unsigned char data[2];
    uint16_t x = 0x1234;
    uint16_to_little_endian(x, data, 0);
    for(int i = 0; i < 2; i++) {
        printf("%02x", data[i]);
    }
}

void test_uint32() {
    unsigned char data[4];
    uint32_t x = 0x12345678;
    uint32_to_little_endian(x, data, 0);
    for(int i = 0; i < 4; i++) {
        printf("%02x", data[i]);
    }
}

void test_uint64() {
    unsigned char data[8];
    uint64_t x = 0x0123456789abcdef;
    uint64_to_little_endian(x, data, 0);
    for(int i = 0; i < 8; i++) {
        printf("%02x", data[i]);
    }
}