/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdio.h>
#include "hashmap.h"

int print_elem(void *const context, struct hashmap_element_s *const p_elem) {
    enclave_printf("%s => 0x", p_elem->key);
    for(int i = 0; i < 32; i++) {
        enclave_printf("%02X", ((u_int8_t *)(p_elem->data))[i]);
    }
    enclave_printf("\n");
    return 0;
}

void test_hashmap() {
    struct hashmap_s hashmap;
    int ret;

    ret = hashmap_create(64, &hashmap);
    if(ret != 0) {
        enclave_printf("create hashmap failed.");
        return;
    }

    hashmap_put(&hashmap, "Cat", 3, (void *){"Tom"}, 3);
    hashmap_put(&hashmap, "Mouse", 5, (void *){"Jerry"}, 5);
    hashmap_put(&hashmap, "Dog", 3, (void *){"Ruppy"}, 5);

    hashmap_iterate_pairs(&hashmap, print_elem, NULL);

    hashmap_destroy(&hashmap);
}