//
// Created by caizhihong on 2021/6/25.
//

#ifndef TEEEVM_LITTLE_ENDIAN_H
#define TEEEVM_LITTLE_ENDIAN_H

#include <stdint.h>


//16 bit
#define BSWAP_16(x) \
    (uint16_t)((((uint16_t)(x) & 0x00ff) << 8) | \
              (((uint16_t)(x) & 0xff00) >> 8) \
             )

//32 bit
#define BSWAP_32(x) \
    (uint32_t)((((uint32_t)(x) & 0xff000000) >> 24) | \
              (((uint32_t)(x) & 0x00ff0000) >> 8) | \
              (((uint32_t)(x) & 0x0000ff00) << 8) | \
              (((uint32_t)(x) & 0x000000ff) << 24) \
             )

//64 bit
#define BSWAP_64(x) \
    (uint64_t)((((uint64_t)(x) & 0xff00000000000000) >> 56) | \
              (((uint64_t)(x) & 0x00ff000000000000) >> 40) | \
              (((uint64_t)(x) & 0x0000ff0000000000) << 24) | \
              (((uint64_t)(x) & 0x000000ff00000000) << 8) | \
              (((uint64_t)(x) & 0x00000000ff000000) >> 8) | \
              (((uint64_t)(x) & 0x0000000000ff0000) >> 24) | \
              (((uint64_t)(x) & 0x000000000000ff00) << 40) | \
              (((uint64_t)(x) & 0x00000000000000ff) << 56) \
             )


int check_cpu();
void uint16_to_little_endian(uint16_t x, unsigned char* data, uint32_t pos);
void uint32_to_little_endian(uint32_t x, unsigned char* data, uint32_t pos);
void uint64_to_little_endian(uint64_t x, unsigned char* data, uint32_t pos);

#endif //TEEEVM_LITTLE_ENDIAN_H
