//
// Created by caizhihong on 2021/6/25.
//

#include "little_endian.h"

int check_cpu() {
    union w {
        int a;
        char b;
    } c;
    c.a = 1;
    return (c.b == 1); // 小端返回TRUE,大端返回FALSE
}

void uint16_to_little_endian(uint16_t x, unsigned char* data, uint32_t pos) {
    if (!check_cpu()) {
        x = BSWAP_16(x);
    }
    memcpy(data + pos, &x, sizeof(uint16_t));
}

void uint32_to_little_endian(uint32_t x, unsigned char* data, uint32_t pos) {
    if (!check_cpu()) {
        x = BSWAP_32(x);
    }
    memcpy(data + pos, &x, sizeof(uint32_t));
}

void uint64_to_little_endian(uint64_t x, unsigned char* data, uint32_t pos) {
    if(!check_cpu()) {
        x = BSWAP_64(x);
    }
    memcpy(data + pos, &x, sizeof(uint64_t));
}