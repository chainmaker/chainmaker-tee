/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"
#include "errors.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_utils.h>
#include <tSgxSSL_api.h>
#include <tsgxsslio.h>
#include <sgx_tprotected_fs.h>
#include <sgx_trts.h>

uint8_t g_keyWord[SEAL_KEY_LENGTH] = {0};

int pem_passphrase_cb(char *buf, int size, int rwflag, void *u)
{
	int key_len = SEAL_KEY_LENGTH;
	if (u == NULL || buf == NULL)
	{
		return -1;
	}
	if (SEAL_KEY_LENGTH > size)
	{
		key_len = size;
	}
	memcpy(buf, u, key_len);
	return key_len;
}

int getKeyWord(unsigned char *keyWord)
{
	int i;
	int retValue = 0;
	SGX_FILE *rtfile;
	SGX_FILE *krfile;
	sgx_report_t report;
	unsigned char randnum[SGX_KEYID_SIZE];
	//__sgx_mem_aligned sgx_key_request_t key_request;
	sgx_key_request_t key_request;

	enclave_printf("getKeyWord g_keyWord = ");
	enclave_print_hex_bytes(g_keyWord, 16);

	if (g_keyWord[0] != '\0')
	{
		memcpy(keyWord, g_keyWord, SEAL_KEY_LENGTH);
		enclave_printf("return keyWord from memory");
		return 0;
	}

	rtfile = sgx_fopen(TEE_REPORT_FILE_PATH, "rb", NULL);
	if (rtfile == NULL)
	{
		enclave_printf("getKeyWord open report error");
		retValue = 1;
		goto freeParameter;
	}
	if (1 != sgx_fread(&report, sizeof(report), 1, rtfile))
	{
		enclave_printf("getKeyWord report read error");
		retValue = 2;
		goto freeParameter;
	}
	krfile = sgx_fopen(SGX_KR_FILE_PATH, "rb", NULL);
	if (krfile != NULL)
	{
		enclave_printf("getKeyWord using existing request---");
		if (1 != sgx_fread(&key_request, sizeof(key_request), 1, krfile))
		{
			enclave_printf("getKeyWord keyrequest read error");
			retValue = 3;
			goto freeParameter;
		}
	}
	else
	{
		enclave_printf("getKeyWord using new request---");
		memset(&key_request, 0, sizeof(key_request));
		key_request.key_name = SEAL_KEY;
		//key_request.key_policy = SGX_KEYPOLICY_MRSIGNER | SGX_KEYPOLICY_MRENCLAVE;
		key_request.key_policy = SGX_KEYPOLICY_MRSIGNER;
		if (report.body.attributes.flags & SGX_FLAGS_KSS)
		{
			key_request.key_policy |= KEY_POLICY_KSS;
		}
		key_request.isv_svn = report.body.isv_svn;
		key_request.cpu_svn = report.body.cpu_svn;
		key_request.config_svn = report.body.config_svn;
		sgx_read_rand(randnum, SGX_KEYID_SIZE);
		for (i = 0; i < SGX_KEYID_SIZE; ++i)
		{
			key_request.key_id.id[i] = (uint8_t)randnum[i];
		}
		key_request.attribute_mask.flags = TSEAL_DEFAULT_FLAGSMASK;
		key_request.attribute_mask.xfrm = 0;
		key_request.misc_mask = TSEAL_DEFAULT_MISCMASK;
		krfile = sgx_fopen(SGX_KR_FILE_PATH, "wb", NULL);
		if (!krfile)
		{
			enclave_printf("getKeyWord keyrequest open error");
			retValue = 4;
			goto freeParameter;
		}
		if (1 != sgx_fwrite(&key_request, sizeof(key_request), 1, krfile))
		{
			enclave_printf("getKeyWord keyrequest write error");
			retValue = 5;
			goto freeParameter;
		}
	}
	enclave_printf("getKeyWord using report's key_id");
	enclave_print_hex_bytes(report.key_id.id, 32);

	enclave_printf("keyrequest's key_name = %d ", key_request.key_name);
	enclave_printf("keyrequest's key_policy = %d ", key_request.key_policy);
	enclave_printf("keyrequest's flags = %d ", key_request.attribute_mask.flags);
	enclave_printf("keyrequest's mask = %d ", key_request.misc_mask);
	enclave_printf("keyrequest's cpu_svn = ");
	enclave_print_hex_bytes(key_request.cpu_svn.svn, 16);

	enclave_printf("keyrequest's isv_svn = ");
	enclave_printf("%x ", key_request.isv_svn);
	enclave_printf("\n");
	enclave_printf("keyrequest's config_svn = ");
	enclave_printf("%x ", key_request.config_svn);
	enclave_printf("\n");

	memcpy(&key_request.key_id, &report.key_id, sizeof(key_request.key_id));
	sgx_get_key(&key_request, (sgx_key_128bit_t *)keyWord);
	memcpy(g_keyWord, keyWord, SEAL_KEY_LENGTH);
	enclave_printf("getKeyWord keyWord = ");
	enclave_print_hex_bytes(keyWord, 16);

freeParameter:
	if (rtfile != NULL)
	{
		sgx_fclose(rtfile);
	}
	if (krfile != NULL)
	{
		sgx_fclose(krfile);
	}
	return retValue;
}

//***************************************************************************************************
//function name:generateKey()
//in:       const char *publickey_path
//          const char *secretkey_path
//fwrite    publickey & secretkey
//return:   success:                        0
//          generate rsa_pair               1
//          write publickey                 2
//          getKeyWord error                3
//          write secretkey                 4
//			BN_set_word error				5
//***************************************************************************************************
int generateKey(const char *publickey_path, const char *secretkey_path, int app)
{
	int retValue = 0;
	int ret = 0;
	RSA *rsa_pair = NULL;
	EVP_PKEY *evp_rsa_key = NULL;
	BIGNUM *bne = NULL;
	BIO *pubkey_bio = NULL;
	BIO *prikey_bio = NULL;
	SGX_FILE *pubkey_fp = NULL;
	SGX_FILE *prikey_fp = NULL;
	int pubkey_wlen = 0;
	int prikey_wlen = 0;
	int keyAssigned = 0;
	unsigned char *pubkeyBuff = NULL;
	unsigned char *prikeyBuff = NULL;
	//__sgx_mem_aligned uint8_t keyWord[SEAL_KEY_LENGTH];
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};

	ret = getKeyWord(keyWord);
	if (ret != 0)
	{
		enclave_printf("generateKey getKeyWord error %d", ret);
		retValue = 1;
		goto freeParameter;
	}

	//generate rsa_pair
	rsa_pair = RSA_new();
	evp_rsa_key = EVP_PKEY_new();
	bne = BN_new();
	if (BN_set_word(bne, 65537) != 1)
	{
		enclave_printf("generateKey BN_set_word error");
		retValue = 2;
		goto freeParameter;
	}
	if (RSA_generate_key_ex(rsa_pair, CIPHERSIZE, bne, NULL) != 1)
	{
		enclave_printf("generateKey RSA_generate_key_ex error");
		retValue = 3;
		goto freeParameter;
	}
	if (EVP_PKEY_assign_RSA(evp_rsa_key, rsa_pair) != 1)
	{
		enclave_printf("EVP_PKEY_assign_RSA sk error");
		retValue = 3;
		goto freeParameter;
	}
	keyAssigned = 1;

	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL)
	{
		enclave_printf("pubkey_bio is NULL");
		retValue = 4;
		goto freeParameter;
	}
	if (PEM_write_bio_PUBKEY(pubkey_bio, evp_rsa_key) != 1)
	{
		enclave_printf("generateKey get PEM for RSA key error");
		retValue = 5;
		goto freeParameter;
	}
	pubkeyBuff = (unsigned char *)malloc(BIO_number_written(pubkey_bio)+1);
	if (pubkeyBuff == NULL)
	{
		enclave_printf("pubkeyBuff malloc error");
		retValue = 6;
		goto freeParameter;
	}
	memset(pubkeyBuff, 0, BIO_number_written(pubkey_bio)+1);
	pubkey_wlen = BIO_read(pubkey_bio, pubkeyBuff, BIO_number_written(pubkey_bio));
	enclave_printf("pubkey_wlen =  %d", pubkey_wlen);
	enclave_printf("pubkey_buff =  %s", pubkeyBuff);
	if (pubkey_wlen <= 0)
	{
		enclave_printf("bio read error");
		retValue = 7;
		goto freeParameter;
	}
	pubkey_fp = sgx_fopen(publickey_path, "wb", NULL);
	if (pubkey_fp == NULL)
	{
		enclave_printf("sgx_fopen error");
		retValue = 8;
		goto freeParameter;
	}
	sgx_fwrite(pubkeyBuff, 1, pubkey_wlen, pubkey_fp);

	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL)
	{
		enclave_printf("prikey_bio is NULL");
		retValue = 9;
		goto freeParameter;
	}
	if (PEM_write_bio_PrivateKey(prikey_bio, evp_rsa_key, EVP_aes_128_cbc(), NULL, 0, pem_passphrase_cb, keyWord) != 1)
	{
		enclave_printf("generateKey get encrypted PEM for RSA key error");
		retValue = 10;
		goto freeParameter;
	}
	prikeyBuff = (unsigned char *)malloc(BIO_number_written(prikey_bio));
	if (prikeyBuff == NULL)
	{
		enclave_printf("prikeyBuff malloc error");
		retValue = 11;
		goto freeParameter;
	}
	prikey_wlen = BIO_read(prikey_bio, prikeyBuff, BIO_number_written(prikey_bio));
	enclave_printf("prikey_wlen =  %d", prikey_wlen);
	enclave_printf("prikey_buff =  %s", prikeyBuff);
	if (prikey_wlen <= 0)
	{
		enclave_printf("bio read error");
		retValue = 12;
		goto freeParameter;
	}
	prikey_fp = sgx_fopen(secretkey_path, "wb", NULL);
	if (prikey_fp == NULL)
	{
		enclave_printf("sgx_fopen for prikey error");
		retValue = 13;
		goto freeParameter;
	}
	sgx_fwrite(prikeyBuff, 1, prikey_wlen, prikey_fp);

	if (app)
	{
		ocall_export_pubkey((char *)pubkeyBuff);
	}

freeParameter:
	if (pubkeyBuff != NULL)
	{
		free(pubkeyBuff);
	}
	if (prikeyBuff != NULL)
	{
		free(prikeyBuff);
	}

	if (pubkey_bio != NULL)
	{
		BIO_free(pubkey_bio);
	}
	if (prikey_bio != NULL)
	{
		BIO_free(prikey_bio);
	}

	if (pubkey_fp != NULL)
	{
		sgx_fclose(pubkey_fp);
	}
	if (prikey_fp != NULL)
	{
		sgx_fclose(prikey_fp);
	}

	if (evp_rsa_key != NULL)
	{
		EVP_PKEY_free(evp_rsa_key);
	} 

	if (keyAssigned == 0) {
		if (rsa_pair != NULL)
		{
			RSA_free(rsa_pair);
		}
	}

	if (bne != NULL)
	{
		BN_free(bne);
	}
	return retValue;
}

int generateECCKey(const char *publickey_path, const char *secretkey_path)
{
	int retValue = 0;
	int ret = 0;
	int eccgrp = 0;
	EC_KEY *ecc = NULL;
	EVP_PKEY *pkey = NULL;
	BIO *pubkey_bio = NULL;
	BIO *prikey_bio = NULL;
	SGX_FILE *pubkey_fp = NULL;
	SGX_FILE *prikey_fp = NULL;
	int pubkey_wlen = 0;
	int prikey_wlen = 0;
	int keyAssigned = 0;
	unsigned char *pubkeyBuff = NULL;
	unsigned char *prikeyBuff = NULL;
	//__sgx_mem_aligned uint8_t keyWord[SEAL_KEY_LENGTH];
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};

	ret = getKeyWord(keyWord);
	if (ret != 0)
	{
		enclave_printf("generateKey getKeyWord error %d", ret);
		retValue = 1;
		goto freeParameter;
	}

	eccgrp = OBJ_txt2nid("prime256v1");
	enclave_printf("eccgrp: %d", eccgrp);

	ecc = EC_KEY_new_by_curve_name(eccgrp);
	if (ecc == NULL)
	{
		enclave_printf("EC_KEY_new_by_curve_name error");
		retValue = 2;
		goto freeParameter;
	}
	if (!(EC_KEY_generate_key(ecc)))
	{
		enclave_printf("EC_KEY_generate_key error");
		retValue = 3;
		goto freeParameter;
	}
	enclave_printf("size of eckey = %d ", ECDSA_size(ecc));
	pkey = EVP_PKEY_new();
	if (pkey == NULL)
	{
		enclave_printf("EVP_PKEY_new error");
		retValue = 4;
		goto freeParameter;
	}
	if (!EVP_PKEY_assign_EC_KEY(pkey, ecc))
	{
		enclave_printf("EVP_PKEY_assign_EC_KEY error");
		retValue = 5;
		goto freeParameter;
	}
	keyAssigned = 1;

	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL)
	{
		enclave_printf("pubkey_bio is NULL");
		retValue = 6;
		goto freeParameter;
	}
	if (PEM_write_bio_PUBKEY(pubkey_bio, pkey) != 1)
	{
		enclave_printf("PEM_write_bio_PUBKEY error");
		retValue = 7;
		goto freeParameter;
	}
	pubkeyBuff = (unsigned char *)malloc(BIO_number_written(pubkey_bio));
	if (pubkeyBuff == NULL)
	{
		enclave_printf("pubkeyBuff malloc error");
		retValue = 8;
		goto freeParameter;
	}
	pubkey_wlen = BIO_read(pubkey_bio, pubkeyBuff, BIO_number_written(pubkey_bio));
	enclave_printf("pubkey_wlen =  %d", pubkey_wlen);
	enclave_printf("pubkey_buff =  %s\n", pubkeyBuff);
	if (pubkey_wlen <= 0)
	{
		enclave_printf("bio read error\n");
		retValue = 9;
		goto freeParameter;
	}
	pubkey_fp = sgx_fopen(publickey_path, "wb", NULL);
	if (pubkey_fp == NULL)
	{
		enclave_printf("sgx_fopen error\n");
		retValue = 10;
		goto freeParameter;
	}
	sgx_fwrite(pubkeyBuff, 1, pubkey_wlen, pubkey_fp);

	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL)
	{
		enclave_printf("prikey_bio is NULL\n");
		retValue = 11;
		goto freeParameter;
	}
	if (PEM_write_bio_PrivateKey(prikey_bio, pkey, EVP_aes_128_cbc(), NULL, 0, pem_passphrase_cb, keyWord) != 1)
	{
		enclave_printf("PEM_write_bio_PrivateKey error\n");
		retValue = 12;
		goto freeParameter;
	}
	prikeyBuff = (unsigned char *)malloc(BIO_number_written(prikey_bio));
	if (prikeyBuff == NULL)
	{
		enclave_printf("prikeyBuff malloc error\n");
		retValue = 13;
		goto freeParameter;
	}
	prikey_wlen = BIO_read(prikey_bio, prikeyBuff, BIO_number_written(prikey_bio));
	enclave_printf("prikey_wlen =  %d\n", prikey_wlen);
	enclave_printf("prikey_buff =  %s\n", prikeyBuff);
	if (prikey_wlen <= 0)
	{
		enclave_printf("bio read error\n");
		retValue = 14;
		goto freeParameter;
	}
	prikey_fp = sgx_fopen(secretkey_path, "wb", NULL);
	if (prikey_fp == NULL)
	{
		enclave_printf("sgx_fopen for prikey error\n");
		retValue = 15;
		goto freeParameter;
	}
	sgx_fwrite(prikeyBuff, 1, prikey_wlen, prikey_fp);

freeParameter:
	if (pubkey_fp != NULL)
	{
		sgx_fclose(pubkey_fp);
	}
	if (prikey_fp != NULL)
	{
		sgx_fclose(prikey_fp);
	}

	if (pubkeyBuff != NULL)
	{
		free(pubkeyBuff);
	}
	if (prikeyBuff != NULL)
	{
		free(prikeyBuff);
	}
	if (pubkey_bio != NULL)
	{
		BIO_free(pubkey_bio);
	}
	if (prikey_bio != NULL)
	{
		BIO_free(prikey_bio);
	}

	if (pkey != NULL)
	{
		EVP_PKEY_free(pkey);
	}
	if (keyAssigned == 0) 
	{
		if (ecc != NULL) 
		{
			EC_KEY_free(ecc);
		}
	}

	return retValue;
}

//***************************************************************************************************
//function name:initKey()
//in:       char* signAlgorithm
//return:   success:					0
//          unknowen signAlgorithm		1
//          initKey generateKey			2
//          loadKey generateKey Ext		3
//***************************************************************************************************
int initKey(char *signAlgorithm)
{
	int ret = 0;
	int rsa_flag = 0;
	int ecc_flag = 0;
	if (strcmp(signAlgorithm, ALG_RSA) == 0)
	{
		rsa_flag = 1;
	}
	else if (strcmp(signAlgorithm, ALG_ECC) == 0)
	{
		ecc_flag = 1;
	}
	else
	{
		enclave_printf("initKey unknown signAlgorithm: %s\n", signAlgorithm);
		return 1;
	}
	if (rsa_flag == 1)
	{
		ret = generateKey(PUBLICKEY_FILE_PATH, SECRETKEY_FILE_PATH, 0);
		if (ret != 0)
		{
			enclave_printf("initKey generateKey %d\n", ret);
			return 2;
		}
	}
	else if (ecc_flag == 1)
	{
		ret = generateECCKey(PUBLICKEY_FILE_PATH, SECRETKEY_FILE_PATH);
		if (ret != 0)
		{
			enclave_printf("initKey generateECCKey %d\n", ret);
			return 3;
		}
	}
	ret = generateKey(PUBLICKEYEXT_FILE_PATH, SECRETKEYEXT_FILE_PATH, 1);
	if (ret != 0)
	{
		enclave_printf("initKey generateKey Ext %d\n", ret);
		return 4;
	}
	return 0;
}

int getPubKey(EVP_PKEY **evp_pubkey, const char *pubkey_path)
{
	int ret = 0;
	size_t keylen = 0;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio;
	unsigned char *pubkey_buff;

	enclave_printf("getPubKey in for %s\n", pubkey_path);
	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL)
	{
		enclave_printf("new bio error\n");
		return -1;
	}
	fp = sgx_fopen(pubkey_path, "rb", NULL);
	if (fp == NULL)
	{
		enclave_printf("sgx_fopen error\n");
		BIO_free(pubkey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -3;
	}
	keylen = sgx_ftell(fp);
	if (0 != sgx_fseek(fp, 0, SEEK_SET))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -4;
	}
	pubkey_buff = (unsigned char *)malloc(keylen);
	if (pubkey_buff == NULL)
	{
		enclave_printf("malloc error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -5;
	}
	memset(pubkey_buff, 0, keylen);
	sgx_fread(pubkey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("pubkey\n");
	//enclave_printf("%s\n", pubkey_buff);
	BIO_puts(pubkey_bio, (const char *)pubkey_buff);

	*evp_pubkey = PEM_read_bio_PUBKEY(pubkey_bio, NULL, NULL, NULL);
	BIO_free(pubkey_bio);
	if (pubkey_buff != NULL)
	{
		free(pubkey_buff);
	}
	if (*evp_pubkey == NULL)
	{
		enclave_printf("new evp error\n");
		return -6;
	}
	enclave_printf("getPubKey out\n");
	return 0;
}

int getPriKey(EVP_PKEY **evp_prikey, const char *prikey_path)
{
	int ret = 0;
	size_t keylen = 0;
	SGX_FILE *fp = NULL;
	BIO *prikey_bio;
	unsigned char *prikey_buff;
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};

	enclave_printf("getPriKey in for %s\n", prikey_path);
	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL)
	{
		enclave_printf("new bio error\n");
		return -1;
	}
	fp = sgx_fopen(prikey_path, "rb", NULL);
	if (fp == NULL)
	{
		enclave_printf("sgx_fopen error\n");
		BIO_free(prikey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -3;
	}
	keylen = sgx_ftell(fp);
	if (0 != sgx_fseek(fp, 0, SEEK_SET))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -4;
	}
	prikey_buff = (unsigned char *)malloc(keylen);
	if (prikey_buff == NULL)
	{
		enclave_printf("malloc error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -5;
	}
	memset(prikey_buff, 0, keylen);
	sgx_fread(prikey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("prikey in getPrikey\n");
	//enclave_printf("%s\n", prikey_buff);
	BIO_puts(prikey_bio, (const char *)prikey_buff);

	ret = getKeyWord(keyWord);
	if (ret != 0)
	{
		enclave_printf("getPriKey getKeyWord error %d\n", ret);
		BIO_free(prikey_bio);
		if (prikey_buff != NULL)
		{
			free(prikey_buff);
		}
		return -6;
	}

	*evp_prikey = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	BIO_free(prikey_bio);
	if (prikey_buff != NULL)
	{
		free(prikey_buff);
	}
	if (*evp_prikey == NULL)
	{
		enclave_printf("new evp error\n");
		return -7;
	}
	enclave_printf("getPriKey out\n");
	return 0;
}

int loadKey(EVP_PKEY **evp_secretkey, EVP_PKEY **evp_publickey, EVP_PKEY **evp_secretkeyExt, EVP_PKEY **evp_publickeyExt)
{
	int ret = 0;

	enclave_printf("loadKey in\n");
	ret = getPriKey(evp_secretkey, SECRETKEY_FILE_PATH);
	if (ret != 0)
	{
		enclave_printf("loadKey getPriKey error: %d\n", ret);
		return 2;
	}
	ret = getPriKey(evp_secretkeyExt, SECRETKEYEXT_FILE_PATH);
	if (ret != 0)
	{
		enclave_printf("loadKey getPriKey for ext error: %d\n", ret);
		return 3;
	}
	ret = getPubKey(evp_publickey, PUBLICKEY_FILE_PATH);
	if (ret != 0)
	{
		enclave_printf("loadKey getPubKey error: %d\n", ret);
		return 4;
	}
	ret = getPubKey(evp_publickeyExt, PUBLICKEYEXT_FILE_PATH);
	if (ret != 0)
	{
		enclave_printf("loadKey getPriKey for ext error: %d\n", ret);
		return 5;
	}
	enclave_printf("loadKey out\n");
	return 0;
}

int initReport()
{
	int i = 0;
	SGX_FILE *tfp = NULL;
	SGX_FILE *rfp = NULL;
	sgx_target_info_t target_info;
	sgx_target_info_t orig_target_info;
	sgx_report_t report;
	unsigned char *reportB64 = NULL;
   	size_t reportB64Size = 0;

	if ((tfp = sgx_fopen(TEE_TARGET_FILE_PATH, "rb", NULL)) == NULL)
	{
		if (SGX_SUCCESS != sgx_self_target(&target_info))
		{
			enclave_printf("self_target error\n");
			return 1;
		}
		if ((tfp = sgx_fopen(TEE_TARGET_FILE_PATH, "wb", NULL)) == NULL)
		{
			enclave_printf("fopen for target file error\n");
			return 2;
		}
		sgx_fwrite(&target_info, sizeof(target_info), 1, tfp);
		sgx_fclose(tfp);
		enclave_printf("write mytarget ok\n");
	}
	else
	{
		sgx_fread(&orig_target_info, sizeof(orig_target_info), 1, tfp);
		sgx_fclose(tfp);
		enclave_printf("mytarget exists\n");
		enclave_printf("original mr_enclave = \n");
		enclave_print_hex_bytes(orig_target_info.mr_enclave.m, SGX_HASH_SIZE);
		if (SGX_SUCCESS != sgx_self_target(&target_info))
		{
			enclave_printf("self_target error\n");
			return 1;
		}
		enclave_printf("current mr_enclave = \n");
		enclave_print_hex_bytes(target_info.mr_enclave.m, SGX_HASH_SIZE);
		if (0 == memcmp(orig_target_info.mr_enclave.m, target_info.mr_enclave.m, SGX_HASH_SIZE))
		{
			enclave_printf("mr_enclave does not change\n");
			return 0;
		}
		if ((tfp = sgx_fopen(TEE_TARGET_FILE_PATH, "wb", NULL)) == NULL)
		{
			enclave_printf("fopen for target file error\n");
			return 2;
		}
		sgx_fwrite(&target_info, sizeof(target_info), 1, tfp);
		sgx_fclose(tfp);
		enclave_printf("write new mytarget ok\n");
	}

	if ((rfp = sgx_fopen(TEE_REPORT_FILE_PATH, "wb", NULL)) != NULL)
	{
		if (SGX_SUCCESS != sgx_create_report(&target_info, NULL, &report))
		{
			enclave_printf("create report error\n");
			return 3;
		}
		enclave_printf("mac of report = \n");
		enclave_print_hex_bytes(report.mac, 16);
		enclave_printf("key_id of report = \n");
		enclave_print_hex_bytes(report.key_id.id, 32);
		enclave_printf("report's cpu_svn = \n");
		enclave_print_hex_bytes(report.body.cpu_svn.svn, 16);
		enclave_printf("report's mr_enclave = \n");
		enclave_print_hex_bytes(report.body.mr_enclave.m, SGX_HASH_SIZE);
		enclave_printf("report's mr_signer = \n");
		enclave_print_hex_bytes(report.body.mr_signer.m, 32);
		sgx_fwrite(&report, sizeof(report), 1, rfp);
		sgx_fclose(rfp);
		enclave_printf("write myreport ok\n");

		//get report and export
		if (getReportB64(&reportB64, &reportB64Size) != 0)
		{
			enclave_printf("getReportB64 error in initReport\n");
			return 5;
		}
		enclave_printf("exporting report\n");
		ocall_export_report((char *) reportB64);
		if (reportB64 != NULL)
		{
			free(reportB64);
		}
	}

	return 0;
}

int tee_crypt_init(char *signAlgorithm)
{
	SGX_FILE *fp = NULL;
	SGX_FILE *fp_ext = NULL;
	int ret;
	int retValue = ENCLAVE_OK;
	enclave_printf("tee_crypt_init in\n");
	OPENSSL_init_crypto(0, NULL);
	OpenSSL_add_all_algorithms();

	initReport();
	enclave_printf("after initReport\n");

	if ((strcmp(signAlgorithm, ALG_RSA) != 0) && (strcmp(signAlgorithm, ALG_ECC) != 0))
	{
		enclave_printf("unknown signAlgorithm in tee_crypt_init: %s\n", signAlgorithm);
		retValue = ENCLAVE_ERROR;
		goto end_procedure;
	}

	if (((fp = sgx_fopen(SECRETKEY_FILE_PATH, "rb", NULL)) == NULL) || ((fp_ext = sgx_fopen(SECRETKEYEXT_FILE_PATH, "rb", NULL)) == NULL))
	{
		enclave_printf("initKey start\n");
		if ((ret = initKey(signAlgorithm)) != 0)
		{
			enclave_printf("error when initKey: %d\n", ret);
			retValue = ENCLAVE_ERROR;
			goto end_procedure;
		}
		enclave_printf("initKey ok\n");
	}

	if ((retValue = CheckCSR()) != ENCLAVE_OK)
	{
		goto end_procedure;
	}
	enclave_printf("after CheckCSR\n");

	if ((retValue = LoadTeeCert()) != ENCLAVE_OK)
	{
		goto end_procedure;
	}
	enclave_printf("after LoadTeeCert\n");

	sgxssl_test();
	enclave_printf("after sgxssl_test\n");

end_procedure:
	if (fp != NULL)
	{
		sgx_fclose(fp);
	}
	if (fp_ext != NULL)
	{
		sgx_fclose(fp_ext);
	}
	enclave_printf("tee_crypt_init out\n");
	return retValue;
}
