/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>

int aes_gcm_encrypt(char* key, int key_length, char* iv, int iv_length, char* plaintext, int plain_length, char* ciphertext, int* cipher_length, char* tag) {
	EVP_CIPHER_CTX *ctx;
	int outlen, tmplen;
	char* outbuf = (char*)malloc(sizeof(char) * (plain_length + AES_GCM_CIPHER_BLOCK_LENGTH));
	if (outbuf == NULL) {
		enclave_printf("new outbuf error\n");
		return -1;
	}
	if ((key_length != 16) && (key_length != 32)) {
		enclave_printf("key_length error\n");
		return -2;
	}
	ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL) {
		enclave_printf("new ctx error\n");
		return -3;
	}
	/* Set cipher type and mode */
	if (key_length == 16) {
		EVP_EncryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, NULL, NULL);
	} else {
		EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	}
	/* Set IV length if default 96 bits is not appropriate */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_length, NULL);
	/* Initialise key and IV */
	EVP_EncryptInit_ex(ctx, NULL, NULL, (const unsigned char *)key, (const unsigned char *)iv);
	/* Encrypt plaintext */
	EVP_EncryptUpdate(ctx, (unsigned char *)outbuf, &outlen, (const unsigned char *)plaintext, plain_length);
	memcpy(ciphertext, outbuf, outlen);
	*cipher_length = outlen;
	/* Finalise: note get no output for GCM */
	EVP_EncryptFinal_ex(ctx, (unsigned char *)outbuf, &outlen);
	/* Get tag */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, AES_GCM_TAG_LENGTH, outbuf);
	/* Output tag */
	memcpy(tag, outbuf, AES_GCM_TAG_LENGTH);
	if (ctx != NULL) {
		EVP_CIPHER_CTX_free(ctx);
	}
	free(outbuf);
	return 0;
}

int aes_gcm_decrypt(char* key, int key_length, char* iv, int iv_length, char* ciphertext, int cipher_length, char* tag, char* plaintext, int* plain_length)
{
	EVP_CIPHER_CTX *ctx;
	int outlen, tmplen, rv;
	char* outbuf = (char*)malloc(sizeof(char) * cipher_length);
	if (outbuf == NULL) {
		enclave_printf("new outbuf error\n");
		return -1;
	}
	if ((key_length != 16) && (key_length != 32)) {
		enclave_printf("key_length error\n");
		return -2;
	}
	ctx = EVP_CIPHER_CTX_new();
	if (ctx == NULL) {
		enclave_printf("new ctx error\n");
		return -3;
	}
	/* Select cipher */
	if (key_length == 16) {
		EVP_DecryptInit_ex(ctx, EVP_aes_128_gcm(), NULL, NULL, NULL);
	} else {
		EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
	}
	/* Set IV length, omit for 96 bits */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_length, NULL);
	/* Set expected tag value. */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, AES_GCM_TAG_LENGTH, tag);
	/* Specify key and IV */
	EVP_DecryptInit_ex(ctx, NULL, NULL, (const unsigned char *)key, (const unsigned char *)iv);
	/* Decrypt plaintext */
	EVP_DecryptUpdate(ctx, (unsigned char *)outbuf, &outlen, (const unsigned char *)ciphertext, cipher_length);
	/* Output decrypted block */
	memcpy(plaintext, outbuf, outlen);
	*plain_length = outlen;
	/* Finalise: note get no output for GCM */
	rv = EVP_DecryptFinal_ex(ctx, (unsigned char *)outbuf, &outlen);
	if (ctx != NULL) {
		EVP_CIPHER_CTX_free(ctx);
	}
	free(outbuf);
	if (rv == 1) {
		return 0;
	} else {
		enclave_printf("aes decrypt error: %d \n", rv);
		return -4;
	}
}

