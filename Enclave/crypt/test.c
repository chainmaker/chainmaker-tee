/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */

#include "crypt_api.h"
#include "crypt_internal.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_utils.h>
#include <tSgxSSL_api.h>
#include <tsgxsslio.h>
#include <sgx_tprotected_fs.h>

static unsigned char fs_key[] = { // 32 bytes, Key
        0x1e, 0xdc, 0x1f, 0x57, 0x48, 0x7f, 0x51, 0x92,
        0x1c, 0x04, 0x65, 0x66, 0x5f, 0x8a, 0x46, 0x31
};

static unsigned char gcm_key[] = { // 32 bytes, Key
        0x1e, 0xdc, 0x1f, 0x57, 0x48, 0x7f, 0x51, 0x92,
        0x1c, 0x04, 0x65, 0x66, 0x5f, 0x8a, 0x46, 0x31,
        0x1e, 0xdc, 0x1f, 0x57, 0x48, 0x7f, 0x51, 0x92,
        0x1c, 0x04, 0x65, 0x66, 0x5f, 0x8a, 0x46, 0x31
};

static unsigned char gcm_iv[] = { // 12 bytes, IV(Initialisation Vector)
        0x99, 0xba, 0x3e, 0x68, 0x2d, 0x81, 0x73, 0x30, 0x4e, 0x50, 0x66, 0x84
};

void rsa_test() {
	int ret = 0;
	EVP_PKEY *rsa = NULL;
	EVP_PKEY *rsa_pri = NULL;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio = NULL;
	BIO *prikey_bio = NULL;
	unsigned char pubkey_buff[2048];
	unsigned char prikey_buff[4096];
	char secret_share[SHARE_LENGTH]="";
	char out[SHARE_LENGTH]="";
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
	memcpy(secret_share, "hello world", 12);

	char cipher[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	char decrypted[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	size_t plain_len = SHARE_LENGTH;
	size_t cipher_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;
	size_t decrypted_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;

	enclave_printf("rsa_test in");

	ret = getKeyWord(keyWord);
	if(ret != 0){
		enclave_printf("rsa_test getKeyWord error %d",ret);
		goto endProcedure;
	}

	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL) { 
		goto endProcedure;
	}
	fp = sgx_fopen(PUBLICKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		goto endProcedure;
	}
	sgx_fread(pubkey_buff, 1, 2048, fp);
	sgx_fclose(fp);
	//enclave_printf("pubkey");
	//enclave_printf("%s", pubkey_buff);
	BIO_puts(pubkey_bio, (const char*)pubkey_buff);
	rsa = PEM_read_bio_PUBKEY(pubkey_bio,NULL,NULL,NULL);
	if (rsa == NULL) {
		goto endProcedure;
	}
	if (RSA_OAEP_encrypt(rsa, (unsigned char*)secret_share, plain_len, (unsigned char*)cipher, &cipher_len) != 0) {
		goto endProcedure;
	}
	enclave_printf("cipher length: %d", cipher_len);
	enclave_printf("cipher : %x", cipher);

	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL) {
		goto endProcedure;
	}
	fp = sgx_fopen(SECRETKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		goto endProcedure;
	}
	sgx_fread(prikey_buff, 1, 4096, fp);
	sgx_fclose(fp);
	//enclave_printf("prikey");
	//enclave_printf("%s", prikey_buff);
	BIO_puts(prikey_bio, (const char*)prikey_buff);
	rsa_pri = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (rsa_pri == NULL) {
		enclave_printf("read bio rsaprivtekey error");
		goto endProcedure;
	}
	if ((ret = RSA_OAEP_decrypt(rsa_pri, (unsigned char*)cipher, cipher_len, (unsigned char*)out, &decrypted_len)) != 0) {
		enclave_printf("decrypt error: %d", ret);
		goto endProcedure;
	}
	enclave_printf("decrypt length: %d", decrypted_len);
	enclave_printf("out : %s", out);
	enclave_printf("rsa_test ok");

endProcedure:
	if (rsa != NULL)
	{
		EVP_PKEY_free(rsa);
	}
	if (rsa_pri != NULL)
	{
		EVP_PKEY_free(rsa_pri);
	}
	if (pubkey_bio != NULL)
	{
		BIO_free(pubkey_bio);
	}
	if (prikey_bio != NULL)
	{
		BIO_free(prikey_bio);
	}

	return;	
}

void tee_rsa_decrypt_test() {
	int ret = 0;
	EVP_PKEY *rsa = NULL;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio = NULL;
	unsigned char pubkey_buff[2048];
	char secret_share[SHARE_LENGTH]="";
	char out[SHARE_LENGTH]="";
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
	memcpy(secret_share, "hello world", 12);

	char cipher[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	char decrypted[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	size_t plain_len = SHARE_LENGTH;
	size_t cipher_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;
	size_t decrypted_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;

	enclave_printf("tee_rsa_decrypt_test in");

	ret = getKeyWord(keyWord);
	if(ret != 0){
		enclave_printf("tee_rsa_decryp_test getKeyWord error %d",ret);
		goto endProcedure;
	}

	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL) { 
		goto endProcedure;
	}
	fp = sgx_fopen(PUBLICKEYEXT_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		enclave_printf("tee_rsa_decryp_test read pubkey error");
		goto endProcedure;
	}
	sgx_fread(pubkey_buff, 1, 2048, fp);
	sgx_fclose(fp);
	//enclave_printf("pubkey");
	//enclave_printf("%s", pubkey_buff);
	BIO_puts(pubkey_bio, (const char*)pubkey_buff);
	rsa = PEM_read_bio_PUBKEY(pubkey_bio,NULL,NULL,NULL);
	if (rsa == NULL) {
		enclave_printf("tee_rsa_decryp_test bio read pubkey error");
		goto endProcedure;
	}
	if (RSA_OAEP_encrypt(rsa, (unsigned char*)secret_share, plain_len, (unsigned char*)cipher, &cipher_len) != 0) {
		enclave_printf("tee_rsa_decryp_test encrypt error");
		goto endProcedure;
	}
	enclave_printf("cipher length: %d", cipher_len);
	enclave_print_hex_bytes((unsigned char *)cipher, cipher_len);
	if (tee_rsa_decrypt((unsigned char*)cipher, cipher_len, (unsigned char*)out, &decrypted_len, 0) != 0) {
		enclave_printf("tee_rsa_decryp_test decrypt error");
		goto endProcedure;
	}
	enclave_printf("decrypt length: %d", decrypted_len);
	enclave_printf("out : %s", out);
	enclave_printf("tee_rsa_decrypt_test ok");

endProcedure:
	if (rsa != NULL)
	{
		EVP_PKEY_free(rsa);
	}
	if (pubkey_bio != NULL)
	{
		BIO_free(pubkey_bio);
	}
	return;	
}

void rsa_sign_test() {
	int ret = 0;
	EVP_PKEY *rsa = NULL;
	EVP_PKEY *rsa_pri = NULL;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio = NULL;
	BIO *prikey_bio = NULL;
	unsigned char pubkey_buff[2048];
	unsigned char prikey_buff[4096];
	char secret_share[SHARE_LENGTH]="";
	char out[SHARE_LENGTH]="";
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
	memcpy(secret_share, "hello world", 12);

	char cipher[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	char decrypted[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	unsigned int plain_len = SHARE_LENGTH;
	unsigned int cipher_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;

	enclave_printf("rsa_sign_test in");
	ret = getKeyWord(keyWord);
	if(ret != 0){
		enclave_printf("rsa_sign_test getKeyWord error %d",ret);
		goto endProcedure;
	}

	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL) {
		goto endProcedure;
	}
	fp = sgx_fopen(SECRETKEYEXT_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		goto endProcedure;
	}
	sgx_fread(prikey_buff, 1, 4096, fp);
	sgx_fclose(fp);
	//enclave_printf("prikey");
	//enclave_printf("%s", prikey_buff);
	BIO_puts(prikey_bio, (const char*)prikey_buff);
	rsa_pri = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (rsa_pri == NULL) {
		enclave_printf("read bio rsaprivtekey error");
		goto endProcedure;
	}
	if ((ret = RSA_PSS_sign(rsa_pri, (unsigned char*)secret_share, SHARE_LENGTH, (unsigned char*)cipher, (size_t*)&cipher_len)) != 1) {
		enclave_printf("sign error: %d", ret);
		goto endProcedure;
	}
	enclave_printf("cipher length: %d", cipher_len);
	enclave_printf("cipher : %x", cipher);

	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL) {
		goto endProcedure;
	}
	fp = sgx_fopen(PUBLICKEYEXT_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		goto endProcedure;
	}
	sgx_fread(pubkey_buff, 1, 2048, fp);
	sgx_fclose(fp);
	//enclave_printf("pubkey");
	//enclave_printf("%s", pubkey_buff);
	BIO_puts(pubkey_bio, (const char*)pubkey_buff);
	rsa = PEM_read_bio_PUBKEY(pubkey_bio,NULL,NULL,NULL);
	if (rsa == NULL) {
		goto endProcedure;
	}
	if ((ret = RSA_PSS_verify(rsa, (unsigned char*)secret_share, SHARE_LENGTH, (unsigned char*)cipher, cipher_len)) != 1) {
		enclave_printf("verify error: %d", ret);
		goto endProcedure;
	}
	enclave_printf("verify ok ---------");
	enclave_printf("rsa_sign_test out");
	
endProcedure:
	if (rsa != NULL)
	{
		EVP_PKEY_free(rsa);
	}
	if (rsa_pri != NULL)
	{
		EVP_PKEY_free(rsa_pri);
	}
	if (pubkey_bio != NULL)
	{
		BIO_free(pubkey_bio);
	}
	if (prikey_bio != NULL)
	{
		BIO_free(prikey_bio);
	}
	return;	
}

void aes_test() {
	char src[16];
	char dst[16];
	char dec_dst[16];
	char tag[16];
	int dst_len = 0;
	int dec_dst_len = 0;
	int ret = 0;
	memset(src, 0, 16);
	memset(dst, 0, 16);
	memset(dec_dst, 0, 16);
	memset(tag, 0, 16);
	memcpy(src, "1234567890ABCDE", 16);
	enclave_printf("aes_test in");
	ret = aes_gcm_encrypt((char *)gcm_key, 32, (char *)gcm_iv, 12, src, 16, dst, &dst_len, tag);
	if (ret != 0 ) {
		return;
	}
	enclave_printf("dst: %d ", dst_len);
	enclave_print_hex_bytes((unsigned char *)dst, 16);
	enclave_printf("tag: ");
	enclave_print_hex_bytes((unsigned char *)tag, 16);
	enclave_printf("before decrypt");
	//memset(tag, 0, 16);
	ret = aes_gcm_decrypt((char *)gcm_key, 32, (char *)gcm_iv, 12, dst, 16, tag, dec_dst, &dec_dst_len);
	enclave_printf("ret: %d ", ret);
	if (ret != 0 ) {
		return;
	}
	enclave_printf("dec_dst_len: %d ", dec_dst_len);
	enclave_printf("dec_dst: %s ", dec_dst);
	enclave_printf("aes_test ok");
	return;
}

void sgxssl_test() {
	unsigned char src[16];
	unsigned char cipher[RSA3072_LENGTH]; // pre-set a value that is larger than 384;
	size_t cipher_len = RSA3072_LENGTH; // pre-set a value that is larger than 384;
	int ret = 0;
	unsigned char hash[32];
	unsigned int hash_len;
	unsigned char *report = NULL;
	uint32_t report_len = 0;

	memset(src, 0, 16);
	memcpy(src, "hello world", 11);
	memset(cipher, 0, RSA3072_LENGTH);
	
	ret = getReportHash(hash, &hash_len);
    enclave_printf("ret for getReportHash = %d", ret);
    enclave_printf("hash_len for getReportHash = %d", hash_len);
    enclave_printf("hash for getReportHash = ");
    enclave_print_hex_bytes(hash, hash_len);
    if (0 != tee_get_report_size(&report_len))
    {
		enclave_printf("tee_get_report_size error");
    } else {
   		enclave_printf("report_len from tee_get_report_size = %d", report_len);
		report = (unsigned char *)malloc(report_len);
		if (report != NULL)
		{
			memset(report, 0, report_len);
			if (0 != tee_get_report(report_len, report))
			{
				enclave_printf("tee_get_report error");
			} else {
				enclave_printf("report from tee_get_report: ");
				enclave_print_hex_bytes(report, report_len);
				enclave_printf("report end");
			}
			free(report);
		}
	}

	aes_test();

	rsa_test();
	
	rsa_sign_test();
	
	tee_rsa_decrypt_test();

    if (tee_cert_sign(src, 16, cipher, &cipher_len) != 0) {
    	enclave_printf("tee_cert_sign fail");
		return;
	}
    enclave_printf("tee_cert_sign ok");
    if (tee_cert_verify(src, 16, cipher, cipher_len) != 0) {
    	enclave_printf("tee_cert_verify fail");
    } else {
    	enclave_printf("tee_cert_verify ok");
    }

}
