/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_tprotected_fs.h>

#include "errors.h"

int ECDSA_SHA256_sign(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t *siglen)
{
	EVP_MD_CTX *mdctx = NULL;
	EVP_PKEY_CTX *pctx;
	int ret = CRYPTO_ERROR_OPENSSL;

	if (!(mdctx = EVP_MD_CTX_create()))
	{
		enclave_printf("fail to init mdctx\n");
		goto err;
	}

	if (1 != EVP_DigestSignInit(mdctx, &pctx, EVP_sha256(), NULL, pkey))
	{
		enclave_printf("fail to init sign ctx\n");
		goto err;
	}

	if (1 != EVP_DigestSignUpdate(mdctx, msg, msglen))
	{
		enclave_printf("fail to init sign\n");
		goto err;
	}

	if (1 != EVP_DigestSignFinal(mdctx, NULL, siglen))
	{
		enclave_printf("fail to sign\n");
		goto err;
	}

	if (1 != EVP_DigestSignFinal(mdctx, sig, siglen))
	{
		enclave_printf("fail to sign\n");
		goto err;
	}

	ret = CRYPTO_OK;

err:
	if (mdctx)
		EVP_MD_CTX_destroy(mdctx);

	return ret;
}

int ECDSA_SHA256_verify(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t siglen)
{
	EVP_MD_CTX *mdctx = NULL;
	EVP_PKEY_CTX *pctx;
	int ret = CRYPTO_ERROR_OPENSSL;

	if (!(mdctx = EVP_MD_CTX_create()))
	{
		enclave_printf("fail to init mdctx\n");
		goto err;
	}

	if (1 != EVP_DigestVerifyInit(mdctx, &pctx, EVP_sha256(), NULL, pkey))
	{
		enclave_printf("fail to init verify ctx\n");
		goto err;
	}

	if (1 != EVP_DigestVerifyUpdate(mdctx, msg, msglen))
	{
		enclave_printf("fail to init verify\n");
		goto err;
	}

	if (1 == EVP_DigestVerifyFinal(mdctx, sig, siglen))
	{
		ret = CRYPTO_OK;
	}

err:
	if (mdctx)
		EVP_MD_CTX_destroy(mdctx);

	return ret;
}

int tee_ecc_sign(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen) {
	int ret = 0;
	unsigned int hash_len = 0;
	size_t keylen = 0;
	EC_KEY *eckey;
	EVP_PKEY *pkey = NULL;
	SGX_FILE *fp = NULL;
	BIO *prikey_bio;
	unsigned char *prikey_buff;
	unsigned char hash_in[32];
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
        
	enclave_printf("tee_ecc_sign in\n");
	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL) { 
		enclave_printf("new bio error\n");
	    return -1;
	}
	fp = sgx_fopen(ECC_SECRETKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		enclave_printf("sgx_fopen error\n");
		BIO_free(prikey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		return -3;
	}
	keylen = sgx_ftell(fp);
	if (0 != sgx_fseek(fp, 0, SEEK_SET)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		return -4;
	}
	prikey_buff = (unsigned char*)malloc(keylen);
	if (prikey_buff == NULL) {
		enclave_printf("malloc error\n");
		BIO_free(prikey_bio);
		return -5;
	}
	memset(prikey_buff, 0, keylen);
	sgx_fread(prikey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("ecc prikey\n");
	//enclave_printf("%s\n", prikey_buff);
	BIO_puts(prikey_bio, (const char*)prikey_buff);
	ret = getKeyWord(keyWord);
	if(ret != 0){
		enclave_printf("tee_ecc_sign getKeyWord error %d\n",ret);
		BIO_free(prikey_bio);
		return -6;
	}

	pkey = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (pkey == NULL) {
		enclave_printf("read bio for eccprivtekey error\n");
		BIO_free(prikey_bio);
		return -7;
	}
	BIO_free(prikey_bio);
	//hash
	if (!EVP_Digest(in, inlen, hash_in, &hash_len, EVP_sha256(), NULL)) {
		enclave_printf("EVP_Digest for eccprivtekey error\n");
		EVP_PKEY_free(pkey);
		return -8;
	}
	enclave_printf("hash length: %d\n", hash_len);
	enclave_printf("hash in: \n");
	for (int i=0; i<hash_len; i++) {
		enclave_printf("%02x ", hash_in[i]);
	}
	enclave_printf("\n");

	eckey = EVP_PKEY_get1_EC_KEY(pkey);
	if (eckey == NULL) {
		enclave_printf("get1_EC_KEY error\n");
		EVP_PKEY_free(pkey);
		return -9;
	}
	if (EC_KEY_check_key(eckey) != 1) {
		enclave_printf("check key err.\n");
		EVP_PKEY_free(pkey);
		return -10;
	}
	enclave_printf("size of eckey = %d \n",ECDSA_size(eckey));
	EVP_PKEY_free(pkey);

	if (!ECDSA_sign(0, hash_in, hash_len, out, (unsigned int *)outlen, eckey)) {
		enclave_printf("sign error\n");
		EC_KEY_free(eckey);
		return -11;
	}
	EC_KEY_free(eckey);
	enclave_printf("out length of ecc sign: %d\n", *outlen);
	enclave_printf("out : \n");
	for (int j=0; j<*outlen; j++) {
		enclave_printf("%02x ", out[j]);
	}
	enclave_printf("\n");
	enclave_printf("tee_ecc_sign ok\n");
	return 0;	
}

int tee_ecc_verify(unsigned char *data, size_t datalen, unsigned char *sig, size_t siglen) {
	int ret = 0;
	unsigned int hash_len = 0;
	size_t keylen = 0;
	EC_KEY *eckey;
	EVP_PKEY *pkey = NULL;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio;
	unsigned char *pubkey_buff;
	unsigned char hash_in[32];

	enclave_printf("tee_ecc_verify in\n");
	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL) { 
		enclave_printf("new bio error\n");
		return -1;
	}
	fp = sgx_fopen(ECC_PUBLICKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		enclave_printf("sgx_fopen error\n");
		BIO_free(pubkey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		return -3;
	}
	keylen = sgx_ftell(fp);
	enclave_printf("keylen from ftell = %d\n", keylen);
	if (0 != sgx_fseek(fp, 0, SEEK_SET)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		return -4;
	}
	pubkey_buff = (unsigned char*)malloc(keylen);
	if (pubkey_buff == NULL) {
		enclave_printf("malloc error\n");
		BIO_free(pubkey_bio);
		return -5;
	}
	memset(pubkey_buff, 0, keylen);
	sgx_fread(pubkey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("ecc pubkey\n");
	//enclave_printf("%s\n", pubkey_buff);
	BIO_puts(pubkey_bio, (const char*)pubkey_buff);
	pkey = PEM_read_bio_PUBKEY(pubkey_bio,NULL,NULL,NULL);
	if (pkey == NULL) {
		enclave_printf("read bio for eccpublickey error\n");
		BIO_free(pubkey_bio);
		return -6;
	}
	BIO_free(pubkey_bio);
	//hash
	if (!EVP_Digest(data, datalen, hash_in, &hash_len, EVP_sha256(), NULL)) {
		enclave_printf("EVP_Digest for eccpublickey error\n");
		EVP_PKEY_free(pkey);
		return -7;
	}
	enclave_printf("hash length: %d\n", hash_len);
	enclave_printf("hash in: \n");
	for (int i=0; i<hash_len; i++) {
		enclave_printf("%02x ", hash_in[i]);
	}
	enclave_printf("\n");

	eckey = EVP_PKEY_get1_EC_KEY(pkey);
	if (eckey == NULL) {
		enclave_printf("get1_EC_KEY error\n");
		EVP_PKEY_free(pkey);
		return -8;
	}
	if (EC_KEY_check_key(eckey) != 1) {
		enclave_printf("check key err.\n");
		EVP_PKEY_free(pkey);
		return -9;
	}
	enclave_printf("size of eckey = %d \n",ECDSA_size(eckey));
	EVP_PKEY_free(pkey);

	ret = ECDSA_verify(0, hash_in, hash_len, sig, siglen, eckey);
	enclave_printf("verify ret = %d\n", ret);
	if (ret != 1) {
		enclave_printf("verify error\n");
		EC_KEY_free(eckey);
		return -10;
	}
	EC_KEY_free(eckey);
	enclave_printf("tee_ecc_sign ok\n");
	return 0;	
}

