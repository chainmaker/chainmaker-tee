/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"
#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_utils.h>
#include <tSgxSSL_api.h>
#include <tsgxsslio.h>
#include <sgx_tprotected_fs.h>
#include "errors.h"

int RSA_OAEP_encrypt(EVP_PKEY *pkey, unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen)
{
	EVP_PKEY_CTX *ctx;

	ctx = EVP_PKEY_CTX_new(pkey, NULL);
	if (!ctx)
	{
		return -1;
	}

	if (EVP_PKEY_encrypt_init(ctx) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -2;
	}
	if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -3;
	}
	EVP_PKEY_CTX_set_rsa_oaep_md(ctx, EVP_sha256());
	EVP_PKEY_CTX_set_rsa_mgf1_md(ctx, EVP_sha256());
	if (EVP_PKEY_encrypt(ctx, out, outlen, in, inlen) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -4;
	}
	EVP_PKEY_CTX_free(ctx);

	return CRYPTO_OK;
}

int RSA_OAEP_decrypt(EVP_PKEY *pkey, unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen)
{
	EVP_PKEY_CTX *ctx;

	ctx = EVP_PKEY_CTX_new(pkey, NULL);
	if (!ctx)
	{
		return -1;
	}
	if (EVP_PKEY_decrypt_init(ctx) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -2;
	}
	if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -3;
	}
	EVP_PKEY_CTX_set_rsa_oaep_md(ctx, EVP_sha256());
	EVP_PKEY_CTX_set_rsa_mgf1_md(ctx, EVP_sha256());
	if (EVP_PKEY_decrypt(ctx, NULL, outlen, in, inlen) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -4;
	}
	if (EVP_PKEY_decrypt(ctx, out, outlen, in, inlen) <= 0)
	{
		EVP_PKEY_CTX_free(ctx);
		return -4;
	}
	EVP_PKEY_CTX_free(ctx);

	return CRYPTO_OK;
}

int RSA_PSS_sign(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t *siglen)
{
	EVP_MD_CTX *mdctx = NULL;
	EVP_PKEY_CTX *pctx;
	int ret = CRYPTO_ERROR_OPENSSL;

	if (!(mdctx = EVP_MD_CTX_create()))
	{
		enclave_printf("fail to init mdctx\n");
		goto err;
	}

	if (1 != EVP_DigestSignInit(mdctx, &pctx, EVP_sha256(), NULL, pkey))
	{
		enclave_printf("fail to init sign ctx\n");
		goto err;
	}

	if (EVP_PKEY_CTX_set_rsa_padding(pctx, RSA_PKCS1_PSS_PADDING) <= 0)
	{
		enclave_printf("Fail to set RSA padding.\n");
		goto err;
	}
	EVP_PKEY_CTX_set_rsa_mgf1_md(pctx, EVP_sha256());

	if (1 != EVP_DigestSignUpdate(mdctx, msg, msglen))
	{
		enclave_printf("fail to init sign\n");
		goto err;
	}

	if (1 != EVP_DigestSignFinal(mdctx, sig, siglen))
	{
		enclave_printf("fail to sign\n");
		goto err;
	}

	ret = CRYPTO_OK;

err:
	if (mdctx)
		EVP_MD_CTX_destroy(mdctx);

	return ret;
}

int RSA_PSS_verify(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t siglen)
{
	EVP_MD_CTX *mdctx = NULL;
	EVP_PKEY_CTX *pctx;
	int ret = CRYPTO_ERROR_OPENSSL;

	if (!(mdctx = EVP_MD_CTX_create()))
	{
		enclave_printf("fail to init mdctx\n");
		goto err;
	}

	if (1 != EVP_DigestVerifyInit(mdctx, &pctx, EVP_sha256(), NULL, pkey))
	{
		enclave_printf("fail to init verify ctx\n");
		goto err;
	}

	if (EVP_PKEY_CTX_set_rsa_padding(pctx, RSA_PKCS1_PSS_PADDING) <= 0)
	{
		enclave_printf("Fail to set RSA padding.\n");
		goto err;
	}
	EVP_PKEY_CTX_set_rsa_mgf1_md(pctx, EVP_sha256());

	if (1 != EVP_DigestVerifyUpdate(mdctx, msg, msglen))
	{
		enclave_printf("fail to init verify\n");
		goto err;
	}

	if (1 == EVP_DigestVerifyFinal(mdctx, sig, siglen))
	{
		ret = CRYPTO_OK;
	}

err:
	if (mdctx)
		EVP_MD_CTX_destroy(mdctx);

	return ret;
}

int tee_rsa_decrypt(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen, int pad_type)
{
	int ret = 0;
	size_t keylen = 0;
	EVP_PKEY *rsa_pri = NULL;
	SGX_FILE *fp = NULL;
	BIO *prikey_bio;
	unsigned char *prikey_buff;
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
	if (pad_type != 0)
	{
		//only support oaep pad(0) for now
		enclave_printf("unsupported pad type : %d\n", pad_type);
		return -1;
	}
	if (*outlen < CIPHERSIZE/8)
	{
		enclave_printf("outlen is too small : %d\n", *outlen);
		return -2;
	}
	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL)
	{
		enclave_printf("new bio error\n");
		return -3;
	}
	fp = sgx_fopen(SECRETKEYEXT_FILE_PATH, "rb", NULL);
	if (fp == NULL)
	{
		enclave_printf("sgx_fopen error\n");
		BIO_free(prikey_bio);
		return -4;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -5;
	}
	keylen = sgx_ftell(fp);
	enclave_printf("prikey keylen = %d\n", keylen);
	if (0 != sgx_fseek(fp, 0, SEEK_SET))
	{
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -6;
	}
	prikey_buff = (unsigned char *)malloc(keylen);
	if (prikey_buff == NULL)
	{
		enclave_printf("malloc error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -7;
	}
	memset(prikey_buff, 0, keylen);
	sgx_fread(prikey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("prikey\n");
	//enclave_printf("%s\n", prikey_buff);
	BIO_puts(prikey_bio, (const char *)prikey_buff);

	ret = getKeyWord(keyWord);
	if (ret != 0)
	{
		enclave_printf("tee_rsa_decrypt getKeyWord error %d\n", ret);
		BIO_free(prikey_bio);
		if (prikey_buff != NULL)
		{
			free(prikey_buff);
		}
		return -8;
	}

	rsa_pri = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (rsa_pri == NULL)
	{
		enclave_printf("read bio for rsaprivtekey error\n");
		BIO_free(prikey_bio);
		if (prikey_buff != NULL)
		{
			free(prikey_buff);
		}
		return -9;
	}
	BIO_free(prikey_bio);
	if (prikey_buff != NULL)
	{
		free(prikey_buff);
	}
	if ((ret = RSA_OAEP_decrypt(rsa_pri, in, inlen, out, outlen)) != 0)
	{
		enclave_printf("rsa decrypt error: %d\n", ret);
		EVP_PKEY_free(rsa_pri);
		return -10;
	}
	EVP_PKEY_free(rsa_pri);
	return 0;
}

int tee_rsa_sign(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen)
{
	int ret = CRYPTO_ERROR_OPENSSL;
	size_t keylen = 0;
	unsigned int hash_len = 0;
	EVP_PKEY *rsa_pri = NULL;
	SGX_FILE *fp = NULL;
	BIO *prikey_bio = NULL;
	unsigned char *prikey_buff = NULL;
	unsigned char hash_in[32];
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};

	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL)
	{
		enclave_printf("new bio error\n");
		ret = -1;
		goto END_PROCESS;
	}
	fp = sgx_fopen(SECRETKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL)
	{
		enclave_printf("sgx_fopen error\n");
		ret = -2;
		goto END_PROCESS;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END))
	{
		enclave_printf("sgx_fseek error\n");
		ret = -3;
		goto END_PROCESS;
	}
	keylen = sgx_ftell(fp);
	enclave_printf("prikey keylen = %d\n", keylen);
	if (0 != sgx_fseek(fp, 0, SEEK_SET))
	{
		enclave_printf("sgx_fseek error\n");
		ret = -4;
		goto END_PROCESS;
	}
	prikey_buff = (unsigned char *)malloc(keylen + 1);
	if (prikey_buff == NULL)
	{
		enclave_printf("malloc error\n");
		ret = -5;
		goto END_PROCESS;
	}
	memset(prikey_buff, 0, keylen + 1);
	sgx_fread(prikey_buff, 1, keylen, fp);
	BIO_puts(prikey_bio, (const char *)prikey_buff);

	if (getKeyWord(keyWord) != 0)
	{
		enclave_printf("tee_rsa_sign getKeyWord error %d\n", ret);
		ret = -6;
		goto END_PROCESS;
	}

	rsa_pri = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (rsa_pri == NULL)
	{
		enclave_printf("read bio for rsaprivtekey error\n");
		ret = -7;
		goto END_PROCESS;
	}

	if ((ret = RSA_PSS_sign(rsa_pri, in, inlen, out, outlen)) != CRYPTO_OK)
	{
		enclave_printf("fail to create signature\n");
		goto END_PROCESS;
	}

	ret = CRYPTO_OK;

END_PROCESS:
	if (prikey_bio != NULL)
	{
		BIO_free(prikey_bio);
	}
	if (rsa_pri != NULL)
	{
		EVP_PKEY_free(rsa_pri);
	}
	if (fp != NULL)
	{
		sgx_fclose(fp);
	}
	if (prikey_buff != NULL)
	{
		free(prikey_buff);
	}
	return ret;
}
