/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"
#include "errors.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_utils.h>
#include <tSgxSSL_api.h>
#include <tsgxsslio.h>
#include <sgx_tprotected_fs.h>

int uint32_serialization(unsigned char *out, uint32_t in)
{
    if (out == NULL)
    {
        enclave_printf("output is NULL\n");
        return ENCLAVE_ERROR;
    }
    out[0] = in / (1 << 24);
    out[1] = (in % (1 << 24)) / (1 << 16);
    out[2] = (in % (1 << 16)) / (1 << 8);
    out[3] = in % (1 << 8);

    return ENCLAVE_OK;
}

int uint32_deserialization(uint32_t *out, unsigned char *in)
{
    if (out == NULL || in == NULL)
    {
        enclave_printf("input pointer is NULL\n");
        return ENCLAVE_ERROR;
    }

    *out = (uint32_t)(in[0]) * (1 << 24) + (uint32_t)(in[1]) * (1 << 16) + (uint32_t)(in[2]) * (1 << 8) + (uint32_t)(in[3]);
    return ENCLAVE_OK;
}

int read_file(unsigned char **content, size_t *content_len, char *file_name)
{
    int ret = ENCLAVE_OK;
    SGX_FILE *fp = NULL;
    if (file_name == NULL)
    {
        enclave_printf("file name is NULL\n");
        ret = ENCLAVE_ERROR;
        goto end_procedure;
    }
    if ((fp = sgx_fopen(file_name, "rb", NULL)) == NULL)
    {
        enclave_printf("file %s does not exist\n", file_name);
        ret = ENCLAVE_ERROR;
        goto end_procedure;
    }
    if (0 != sgx_fseek(fp, 0, SEEK_END))
    {
        enclave_printf("fail to traverse file\n");
        ret = ENCLAVE_ERROR;
        goto end_procedure;
    }
    *content_len = sgx_ftell(fp);
    enclave_printf("file length from ftell = %d\n", *content_len);
    if (0 != sgx_fseek(fp, 0, SEEK_SET))
    {
        enclave_printf("fail to reset file pointer\n");
        ret = ENCLAVE_ERROR;
        goto end_procedure;
    }
    *content = (unsigned char *)malloc(*content_len + 1);
    if (*content == NULL)
    {
        enclave_printf("malloc error\n");
        ret = ENCLAVE_ERROR;
        goto end_procedure;
    }
    memset(*content, 0, *content_len + 1);
    sgx_fread(*content, 1, *content_len, fp);

end_procedure:
    if (fp != NULL)
    {
        sgx_fclose(fp);
    }

    return ret;
}
