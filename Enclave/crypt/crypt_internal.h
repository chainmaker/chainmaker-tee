/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef _CRYPT_INTERNAL_H_
#define _CRYPT_INTERNAL_H_

#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/ec.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#define SECRETKEY_FILE_PATH "./secretkey.pem"
#define PUBLICKEY_FILE_PATH "./publickey.pem"
#define SECRETKEYEXT_FILE_PATH "./secretkeyExt.pem"
#define PUBLICKEYEXT_FILE_PATH "./publickeyExt.pem"

#define ECC_SECRETKEY_FILE_PATH "./ecc_secretkey.pem"
#define ECC_PUBLICKEY_FILE_PATH "./ecc_publickey.pem"

#define TEE_TARGET_FILE_PATH "./tee_target.dat"
#define TEE_REPORT_FILE_PATH "./tee_report.dat"
#define TEE_CSR_FILE_PATH "./tee_csr.dat"
#define TEE_CERT_FILE_PATH "./tee_cert.pem"
#define SGX_KR_FILE_PATH "./sgxkeyrequest.dat"

#define ALG_RSA "RSA"
#define ALG_ECC "ECC"

#define LAUNCH_KEY         0
#define PROVISION_KEY      1
#define PROVISION_SEAL_KEY 2
#define REPORT_KEY         3
#define SEAL_KEY           4

#define SGX_KEYPOLICY_MRENCLAVE        0x0001      /* Derive key using the enclave's ENCLAVE measurement register */
#define SGX_KEYPOLICY_MRSIGNER         0x0002      /* Derive key using the enclave's SINGER measurement register */
#define SGX_KEYPOLICY_NOISVPRODID      0x0004      /* Derive key without the enclave's ISVPRODID */
#define SGX_KEYPOLICY_CONFIGID         0x0008      /* Derive key with the enclave's CONFIGID */
#define SGX_KEYPOLICY_ISVFAMILYID      0x0010      /* Derive key with the enclave's ISVFAMILYID */
#define SGX_KEYPOLICY_ISVEXTPRODID     0x0020      /* Derive key with the enclave's ISVEXTPRODID */
#define KEY_POLICY_KSS  (SGX_KEYPOLICY_CONFIGID | SGX_KEYPOLICY_ISVFAMILYID | SGX_KEYPOLICY_ISVEXTPRODID)

#define SGX_FLAGS_LICENSE_KEY   0x20ULL

#define FLAGS_NON_SECURITY_BITS     (0xFFFFFFFFFFFFC0ULL | SGX_FLAGS_MODE64BIT | SGX_FLAGS_PROVISION_KEY| SGX_FLAGS_LICENSE_KEY)
#define TSEAL_DEFAULT_FLAGSMASK     (~FLAGS_NON_SECURITY_BITS)
#define TSEAL_DEFAULT_MISCMASK      (~MISC_NON_SECURITY_BITS)

#define MISC_NON_SECURITY_BITS      0x0FFFFFFF  /* bit[27:0]: have no security implications */
#define TSEAL_DEFAULT_MISCMASK      (~MISC_NON_SECURITY_BITS)

#define SGX_REPORT_SIGNED_SIZE 384
#define SGX_REPORT_ACTUAL_SIZE 432

//#if defined(__cplusplus)
//extern "C" {
//#endif

int tee_ecc_verify(unsigned char *data, size_t datalen, unsigned char *sig, size_t siglen);

int pem_passphrase_cb(char *buf, int size, int rwflag, void *u);

int getKeyWord(unsigned char *keyWord);

int loadKey(EVP_PKEY **evp_secretkey, EVP_PKEY **evp_publickey, EVP_PKEY **evp_secretkeyExt, EVP_PKEY **evp_publickeyExt);

int getPriKey(EVP_PKEY **evp_prikey, const char *prikey_path);
int getReportB64(unsigned char **reportB64, size_t *reportB64Size);

int CheckCSR();
int LoadTeeCert();

int read_file(unsigned char **content, size_t *content_len, char* file_name);
int uint32_serialization(unsigned char *out, uint32_t in);

//#if defined(__cplusplus)
//}
//#endif

#endif
