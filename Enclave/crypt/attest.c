/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"
#include "errors.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_utils.h>
#include <tSgxSSL_api.h>
#include <tsgxsslio.h>
#include <sgx_tprotected_fs.h>

typedef struct entry
{
	char *field;
	unsigned int fieldLength;
	char *bytes;
	unsigned int bytesLength;
} Entry;

typedef struct extension
{
	unsigned int NID;
	int critical;
	char *data;
	unsigned int dataLength;
} Extension;

//***************************************************************************************************
//function name:getentry()
//out:	Entry **entry
//      int *entryCount
//Note: remember to free entry
//***************************************************************************************************
void getentry(Entry **entry, int *entryCount)
{
	*entryCount = 3;
	*entry = (Entry *)malloc(sizeof(Entry) * *entryCount);
	{
		(*entry)[0].fieldLength = 2;
		(*entry)[0].field = (char *)malloc(sizeof(char) * (*entry)[0].fieldLength);
		strncpy((*entry)[0].field, "C", 2);
		(*entry)[0].bytesLength = 2;
		(*entry)[0].bytes = (char *)malloc(sizeof(char) * (*entry)[0].bytesLength);
		strncpy((*entry)[0].bytes, "CN", 2);
	}
	{
		(*entry)[1].fieldLength = 2;
		(*entry)[1].field = (char *)malloc(sizeof(char) * (*entry)[1].fieldLength);
		strncpy((*entry)[1].field, "O", 2);
		(*entry)[1].bytesLength = 17;
		(*entry)[1].bytes = (char *)malloc(sizeof(char) * (*entry)[1].bytesLength);
		strncpy((*entry)[1].bytes, "wx.chainmaker.org", 17);
	}

	{
		(*entry)[2].fieldLength = 3;
		(*entry)[2].field = (char *)malloc(sizeof(char) * (*entry)[2].fieldLength);
		strncpy((*entry)[2].field, "CN", 3);
		(*entry)[2].bytesLength = 18;
		(*entry)[2].bytes = (char *)malloc(sizeof(char) * (*entry)[2].bytesLength);
		strncpy((*entry)[2].bytes, "tee.chainmaker.org", 18);
	}
}

//***************************************************************************************************
//function name:getextension()
//out:	Extension **extension
//      int *extensionCount
//Note: remember to free extension
//***************************************************************************************************
void getextension(Extension **extension, int *extensionCount)
{
	*extensionCount = 2;
	*extension = (Extension *)malloc(sizeof(Extension) * *extensionCount);
	{
		(*extension)[0].dataLength = 5;
		(*extension)[0].data = (char *)malloc(sizeof(unsigned char) * (*extension)[0].dataLength);
		memset((*extension)[0].data, 0, sizeof(unsigned char) * (*extension)[0].dataLength);
		strncpy((*extension)[0].data, "quote", 5);
		(*extension)[0].critical = 0;
		(*extension)[0].NID = 156;
	}
	{
		(*extension)[1].dataLength = 9;
		(*extension)[1].data = (char *)malloc(sizeof(unsigned char) * (*extension)[1].dataLength);
		memset((*extension)[1].data, 0, sizeof(unsigned char) * (*extension)[1].dataLength);
		strncpy((*extension)[1].data, "123456789", 9);
		(*extension)[1].critical = 0;
		(*extension)[1].NID = 157;
	}
}

//***************************************************************************************************
//function name:b64_encode()
//in:   const unsigned char *in
//      size_t in_len
//out:	unsigned char **out
//      size_t *out_len
//Note: remember to free out
//***************************************************************************************************
int b64_encode(const unsigned char *in, size_t in_len, unsigned char **out, size_t *out_len)
{
    int ret_code = 0;
	BIO *bmem = NULL;
	BIO *b64 = NULL;
	BUF_MEM *bptr = NULL;
	b64 = BIO_new(BIO_f_base64());
	if (b64 == NULL)
	{
		enclave_printf("BIO_new error for b64\n");
		ret_code = 1;
		goto out;
	}
	bmem = BIO_new(BIO_s_mem());
	if (bmem == NULL)
	{
		enclave_printf("BIO_new error for bmem\n");
		ret_code = 2;
		goto out;
	}
	b64 = BIO_push(b64, bmem);
	if (BIO_write(b64, in, in_len) != in_len)
	{
		enclave_printf("BIO_write error\n");
		ret_code = 3;
		goto out;
	}
	BIO_flush(b64);
	BIO_get_mem_ptr(b64, &bptr);
	*out_len = bptr->length;
	*out = (unsigned char *)calloc(*out_len, sizeof(unsigned char));
	if (*out == NULL)
	{
		enclave_printf("calloc error\n");
		ret_code = 4;
		goto out;
	}
	memcpy(*out, bptr->data, bptr->length);

out:
    if  (b64 != NULL)
    {
    	BIO_free_all(b64);
    }
    return ret_code;
}

//***************************************************************************************************
//function name:getReportB64()
//out:	    unsigned char **reportB64
//          size_t *reportB64Size
//return:   success:                        0
//          get report error	            1
//***************************************************************************************************
int getReportB64(unsigned char **reportB64, size_t *reportB64Size)
{
	int ret = 0;
	SGX_FILE *rtfile;
	RSA *rsa_publickey = NULL;
	unsigned char *public_hash = NULL;
	unsigned char report[SGX_REPORT_ACTUAL_SIZE];
	rtfile = sgx_fopen(TEE_REPORT_FILE_PATH, "rb", NULL);
	if (rtfile == NULL)
	{
		enclave_printf("open report file error\n");
		return 1;
	}
	if (1 != sgx_fread(&report, sizeof(report), 1, rtfile))
	{
		enclave_printf("report read error\n");
		sgx_fclose(rtfile);
		return 2;
	}

	if (0 != b64_encode(report, SGX_REPORT_ACTUAL_SIZE, reportB64, reportB64Size))
	{
		enclave_printf("b64_encode error\n");
		sgx_fclose(rtfile);
		return 3;
	}

	sgx_fclose(rtfile);
	return 0;
}

int getReportHash(unsigned char *reportHash, unsigned int *reportHashSize)
{
	int ret = 0;
	int ret_code = 0;
	unsigned char *reportB64 = NULL;
	size_t reportB64Size;

	ret = getReportB64(&reportB64, &reportB64Size);
	if (ret != 0)
	{
		enclave_printf("getReportB64 error in getReportHash = %d\n", ret);
		ret_code = 1;
		goto out;
	}
	//enclave_printf("reportB64 in getReportHash = %s\n", reportB64);
	//enclave_printf("reportB64Size in getReportHash = %d\n", reportB64Size);

	ret = EVP_Digest(reportB64, reportB64Size, reportHash, reportHashSize, EVP_sha256(), NULL);
	if (ret == 0)
	{
		enclave_printf("EVP_Digest error in getReportHash = %d\n", ret);
		ret_code = 2;
		goto out;
	}

out:
	if (reportB64 != NULL)
	{
		free(reportB64);
	}
	return ret_code;
}

//***************************************************************************************************
//function name:getExtensions()
//in:       Extension *extension
//          int extensionCount
//          unsigned char* reportB64
//          unsigned int reportB64Size
//			EVP_PKEY *evp_publickeyExt
//out:	    X509_EXTENSIONS **extensionStack
//return:   NULL
//Note: remember to free extensionStack
//***************************************************************************************************
void getExtensions(X509_EXTENSIONS **extensionStack, Extension *extension, int extensionCount, unsigned char *reportB64, unsigned int reportB64Size, EVP_PKEY *evp_publickeyExt)
{

	X509_EXTENSION *x509_ext[extensionCount + 1];
	ASN1_STRING *asn1_str[extensionCount + 1];
	ASN1_OBJECT *asn1_obj[extensionCount + 1];
	BIO *evp_publickeyExtBio = NULL;
	RSA *rsa_key = NULL;
	unsigned char *evp_publickeyExtStr;
	*extensionStack = sk_X509_EXTENSION_new_null();
	for (int i = 0; i < extensionCount; i++)
	{
		asn1_str[i] = ASN1_STRING_new();
		ASN1_STRING_set(asn1_str[i], extension[i].data, extension[i].dataLength);
		x509_ext[i] = X509_EXTENSION_new();
		asn1_obj[i] = ASN1_OBJECT_new();
		asn1_obj[i] = OBJ_nid2obj(extension[i].NID);
		X509_EXTENSION_create_by_OBJ(&(x509_ext[i]), asn1_obj[i], extension[i].critical, asn1_str[i]);
		sk_X509_EXTENSION_push(*extensionStack, x509_ext[i]);
	}
	asn1_str[extensionCount] = ASN1_STRING_new();
#if 0
	//******************************************************************************************
	ASN1_STRING_set(asn1_str[extensionCount], reportB64, reportB64Size);
	//******************************************************************************************
	x509_ext[extensionCount] = X509_EXTENSION_new();
	asn1_obj[extensionCount] = ASN1_OBJECT_new();
	asn1_obj[extensionCount] = OBJ_nid2obj(NID_set_msgExt);
	X509_EXTENSION_create_by_OBJ(&(x509_ext[extensionCount]), asn1_obj[extensionCount], 0, asn1_str[extensionCount]);
	sk_X509_EXTENSION_push(*extensionStack, x509_ext[extensionCount]);
#endif
	evp_publickeyExtBio = BIO_new(BIO_s_mem());
	PEM_write_bio_PUBKEY(evp_publickeyExtBio, evp_publickeyExt);
	evp_publickeyExtStr = (unsigned char *)malloc(BIO_number_written(evp_publickeyExtBio));
	BIO_read(evp_publickeyExtBio, evp_publickeyExtStr, BIO_number_written(evp_publickeyExtBio));
	asn1_str[extensionCount] = ASN1_STRING_new();
	ASN1_STRING_set(asn1_str[extensionCount], evp_publickeyExtStr, BIO_number_written(evp_publickeyExtBio));
	x509_ext[extensionCount] = X509_EXTENSION_new();
	asn1_obj[extensionCount] = ASN1_OBJECT_new();
	asn1_obj[extensionCount] = OBJ_nid2obj(NID_keyBag);
	X509_EXTENSION_create_by_OBJ(&(x509_ext[extensionCount]), asn1_obj[extensionCount], 0, asn1_str[extensionCount]);
	sk_X509_EXTENSION_push(*extensionStack, x509_ext[extensionCount]);
	for (int i = 0; i < extensionCount + 1; i++)
	{
		ASN1_STRING_free(asn1_str[i]);
		ASN1_OBJECT_free(asn1_obj[i]);
	}
	BIO_free(evp_publickeyExtBio);
	//Free the evp_publickeyExtStr.
	if (evp_publickeyExtStr != NULL)
	{
		free(evp_publickeyExtStr);
	}
}

//***************************************************************************************************
//function name:csrToStr()
//in:	X509_REQ *csr
//out:  unsigned char **csrStr
//      size_t *csrStrLength
//return:	success			0
//		length overflow		1
//		csrStr malloc error	2
//Note: remember to free csrStr
//***************************************************************************************************
int csrToStr(unsigned char **csrStr, size_t *csrStrLength, X509_REQ *csr)
{
	int retValue = 0;
	BIO *bio = NULL;
	unsigned char str[30720];
	memset(str, 0, 30720);
	bio = BIO_new(BIO_s_mem());
	PEM_write_bio_X509_REQ(bio, csr);
	BIO_read_ex(bio, str, 30720, csrStrLength);
	if (*csrStrLength >= 30720)
	{
		enclave_printf("length overflow\n");
		retValue = 1;
		goto freeParameter;
	}
	*csrStr = (unsigned char *)malloc(sizeof(unsigned char) * *csrStrLength+1);
	if (*csrStr == NULL)
	{
		enclave_printf("csrStr malloc error\n");
		retValue = 2;
		goto freeParameter;
	}
	memset(*csrStr, 0, *csrStrLength+1);
	memcpy(*csrStr, str, *csrStrLength);
freeParameter:
	if (bio != NULL)
	{
		BIO_free(bio);
	}
	return retValue;
}

//***************************************************************************************************
//function name:makeCsr()
//in:       Entry *entry
//          int entryCount
//          X509_EXTENSIONS *extensionStack
//          const EVP_MD *signHash
//          EVP_PKEY *evp_publickey
//          EVP_PKEY *evp_secretkey
//out:	    X509_REQ **csr
//return:   success:                        0
//          X509_REQ_set_version error      1
//          X509_NAME_add_entry_by_txt      2
//          no field Named 'CN' or 'O'      3
//          X509_REQ_set_pubkey error       4
//          X509_REQ_add_extensions error   5
//          X509_REQ_sign error             6
//Note: remember to free extensionStack
//***************************************************************************************************
int makeCsr(X509_REQ **csr, Entry *entry, int entryCount, X509_EXTENSIONS *extensionStack, const EVP_MD *signHash, EVP_PKEY *evp_publickey, EVP_PKEY *evp_secretkey)
{
	int ret = 0;
	X509_NAME *x509_name = NULL;
	int commonNameFlag = 0;
	int organizationFlag = 0;
	*csr = X509_REQ_new();
	if (X509_REQ_set_version(*csr, 2) != 1)
	{
		enclave_printf("makeCsr X509_REQ_set_version error\n");
		return 1;
	}
	x509_name = X509_REQ_get_subject_name(*csr);
	for (int i = 0; i < entryCount; i++)
	{
		if (commonNameFlag || (memcmp(entry[i].field, "CN", 2) == 0))
		{
			commonNameFlag = 1;
		}
		if (organizationFlag || (memcmp(entry[i].field, "O", 1) == 0))
		{
			organizationFlag = 1;
		}
		ret = X509_NAME_add_entry_by_txt(x509_name, (const char *)entry[i].field, MBSTRING_ASC, (const unsigned char *)entry[i].bytes, entry[i].bytesLength, -1, 0);
		if (ret != 1)
		{
			enclave_printf("makeCsr X509_NAME_add_entry_by_txt %d error\n", i);
			enclave_printf("field:%s  bytes:%s\n", entry[i].field, entry[i].bytes);
			return 2;
		}
	}
	if (!(commonNameFlag && organizationFlag))
	{
		enclave_printf("there is no field Named 'CN' or 'O'\n");
		return 3;
	}
	if (X509_REQ_set_pubkey(*csr, evp_publickey) != 1)
	{
		enclave_printf("makeCsr X509_REQ_set_pubkey error\n");
		return 4;
	}
	if (X509_REQ_add_extensions(*csr, extensionStack) != 1)
	{
		enclave_printf("makeCsr X509_REQ_add_extensions error\n");
		return 5;
	}
	ret = X509_REQ_sign(*csr, evp_secretkey, signHash);
	if (ret <= 0)
	{
		enclave_printf("makeCsr X509_REQ_sign error %d\n", ret);
		return 6;
	}
	return 0;
}

//***************************************************************************************************
//function name:getCsr()
//in:       Entry *entry
//          int entryCount
//          Extension *extension
//          int extensionCount
//          const EVP_MD *signHash
//out:      X509_REQ **csr
//return:   success:					0
//          initOfLoadKey error			1
//          getReportB64 error			2
//          makeCsr error				3
//			csrToStr error				4
//***************************************************************************************************
int getCsr(unsigned char **csrStr, size_t *csrStrLength, Entry *entry, int entryCount, Extension *extension, int extensionCount, const EVP_MD *signHash)
{
	int ret = 0;
	int retValue = 0;
	X509_REQ *csr = NULL;
	EVP_PKEY *evp_secretkey = NULL;
	EVP_PKEY *evp_publickey = NULL;
	EVP_PKEY *evp_secretkeyExt = NULL;
	EVP_PKEY *evp_publickeyExt = NULL;

	/*
	unsigned char *quoteB64 = NULL;
	size_t quoteB64Size;
	*/
	unsigned char *reportB64 = NULL;
	size_t reportB64Size;

	X509_EXTENSIONS *extensionStack = NULL;
	ret = loadKey(&evp_secretkey, &evp_publickey, &evp_secretkeyExt, &evp_publickeyExt);
	if (ret != 0)
	{
		enclave_printf("loadKey error %d\n", ret);
		retValue = 1;
		goto freeParameter;
	}

	ret = getReportB64(&reportB64, &reportB64Size);
	if (ret != 0)
	{
		enclave_printf("getReportB64 error %d\n", ret);
		retValue = 2;
		goto freeParameter;
	}
	getExtensions(&extensionStack, extension, extensionCount, reportB64, reportB64Size, evp_publickeyExt);
	ret = makeCsr(&csr, entry, entryCount, extensionStack, signHash, evp_publickey, evp_secretkey);
	if (ret != 0)
	{
		enclave_printf("makeCsr error %d\n", ret);
		retValue = 3;
		goto freeParameter;
	}
	ret = csrToStr(csrStr, csrStrLength, csr);
	if (ret != 0)
	{
		enclave_printf("csrToStr error %d\n", ret);
		retValue = 4;
		goto freeParameter;
	}
freeParameter:
	if (retValue == 0)
		enclave_printf("getCsr success\n");

	if (evp_secretkey != NULL)
	{
		EVP_PKEY_free(evp_secretkey);
	}
	if (evp_publickey != NULL)
	{
		EVP_PKEY_free(evp_publickey);
	}
	if (evp_secretkeyExt != NULL)
	{
		EVP_PKEY_free(evp_secretkeyExt);
	}
	if (evp_publickeyExt != NULL)
	{
		EVP_PKEY_free(evp_publickeyExt);
	}
	if (reportB64 != NULL)
	{
		free(reportB64);
	}
	if (extensionStack != NULL)
	{
		sk_X509_EXTENSION_pop_free(extensionStack, X509_EXTENSION_free);
	}
	return retValue;
}

int CheckCSR()
{
	int ret = 0;
	int retValue = ENCLAVE_OK;
	SGX_FILE *fp = NULL;
	Entry *entry = NULL;
	int entryCount;
	Extension *extension = NULL;
	int extensionCount;
	unsigned char *csrStr = NULL;
	size_t csrStrLength;
	const EVP_MD *signHash = EVP_sha256();

	// TODO: override for re-initialization or forbid re-write?
	if ((fp = sgx_fopen(TEE_CSR_FILE_PATH, "rb", NULL)) != NULL)
	{
		enclave_printf("csr exists\n");
		sgx_fclose(fp);
		retValue = ENCLAVE_OK;
		goto end_procedure;
	}
	getentry(&entry, &entryCount);
	getextension(&extension, &extensionCount);
	ret = getCsr(&csrStr, &csrStrLength, entry, entryCount, extension, extensionCount, signHash);
	if (ret != 0)
	{
		enclave_printf("getCsr error\n");
		retValue = ENCLAVE_ERROR;
		goto end_procedure;
	}

	//enclave_printf("csrStr in getCsr:  %s \n", csrStr);
	if ((fp = sgx_fopen(TEE_CSR_FILE_PATH, "wb", NULL)) != NULL)
	{
		enclave_printf("writing csr\n");
		ret = sgx_fwrite(csrStr, csrStrLength, 1, fp);
		sgx_fclose(fp);
		if (ret <= 0)
		{
			enclave_printf("localy write csr failed\n");
			retValue = ENCLAVE_ERROR;
			goto end_procedure;
		}
	}

	//export csr
	ocall_export_csr((char *)csrStr);

end_procedure:
	if (entry != NULL)
	{
		for (int i = 0; i < entryCount; i++)
		{
			free(entry[i].bytes);
			free(entry[i].field);
		}
		free(entry);
	}
	if (extension != NULL)
	{
		for (int i = 0; i < extensionCount; i++)
		{
			free(extension[i].data);
		}
		free(extension);
	}
	if (csrStr != NULL)
	{
		free(csrStr);
	}

	return retValue;
}

int checkTeeCert(char *cert)
{
	int ret = 0;
	BIO *bio = NULL;
	X509 *x = NULL;
	EVP_PKEY *pkey = NULL;

	bio = BIO_new(BIO_s_mem());
	if (bio == NULL)
	{
		enclave_printf("new bio error in checkTeeCert\n");
		return -1;
	}
	BIO_puts(bio, (const char *)cert);

	x = PEM_read_bio_X509(bio, NULL, 0, NULL);
	if (x == NULL)
	{
		enclave_printf("read bio x509 in checkTeeCert error\n");
		BIO_free(bio);
		return -2;
	}
	BIO_free(bio);

	ret = getPriKey(&pkey, SECRETKEY_FILE_PATH);
	if (ret != 0)
	{
		enclave_printf("getPriKey error in checkTeeCert: %d\n", ret);
		if (x != NULL)
		{
			X509_free(x);
		}
		return -3;
	}
	ret = X509_check_private_key(x, pkey);
	if (ret != 1)
	{
		enclave_printf("x509_check_private_key error in checkTeeCert: %d\n", ret);
		if (x != NULL)
		{
			X509_free(x);
		}
		if (pkey != NULL)
		{
			EVP_PKEY_free(pkey);
		}
		return -4;
	}
	if (x != NULL)
	{
		X509_free(x);
	}
	if (pkey != NULL)
	{
		EVP_PKEY_free(pkey);
	}

	return 0;
}

int LoadTeeCert()
{
	int wlen = 0;
	SGX_FILE *fp = NULL;
	char *certStr;
	char *mycertStr;
	char existingCert[4096];
	uint32_t certStrLength;

	if ((fp = sgx_fopen(TEE_CERT_FILE_PATH, "rb", NULL)) != NULL)
	{
		enclave_printf("tee cert exists\n");
		sgx_fread(existingCert, 1, 4096, fp);
		sgx_fclose(fp);
		//check cert here
		int ret = checkTeeCert(existingCert);
		if (ret != 0)
		{
			enclave_printf("failed to check existing tee cert,error code :[%d]\n",ret);
			return ENCLAVE_ERROR;
		}
		enclave_printf("check tee cert ok\n");
		return ENCLAVE_OK;
	}

	//get cert
	ocall_get_teecert(&certStr, &certStrLength);

	if (certStrLength == 0)
	{
		enclave_printf("get teecert error\n");
		return ENCLAVE_ERROR;
	}
	mycertStr = (char *)malloc(certStrLength + 1);
	memset(mycertStr, 0, certStrLength + 1);
	memcpy(mycertStr, certStr, certStrLength);

	//check cert here
	int ret = checkTeeCert(mycertStr);
	if (ret != 0)
	{
		enclave_printf("failed to check tee cert,error code :[%d]\n",ret);
		return ENCLAVE_ERROR;
	}
	enclave_printf("check tee cert ok\n");

	if ((fp = sgx_fopen(TEE_CERT_FILE_PATH, "wb", NULL)) != NULL)
	{
		enclave_printf("writing tee cert\n");
		wlen = sgx_fwrite(mycertStr, 1, certStrLength, fp);
		enclave_printf("wlen = %d\n", wlen);
		sgx_fclose(fp);
	}

	return ENCLAVE_OK;
}

int tee_get_proof_size(char *challenge, uint32_t challenge_len, uint32_t *proof_len)
{
	int ret = ENCLAVE_OK, key_type;
	EVP_PKEY *signing_key = NULL;
	BIO *cert_bio = NULL;
	X509 *certificate = NULL;
	unsigned char *cert_pem = NULL, *cert = NULL, *sig = NULL, *msg = NULL, *report = NULL;
	size_t cert_pem_len, cert_len, sig_len, msg_len, report_len;

	if (challenge == NULL)
	{
		enclave_printf("challenge is NULL\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get signing key
	if (getPriKey(&signing_key, (char *)SECRETKEY_FILE_PATH) != 0)
	{
		enclave_printf("load signing key error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get certificate
	if (read_file(&cert_pem, &cert_pem_len, (char *)TEE_CERT_FILE_PATH) != ENCLAVE_OK)
	{
		enclave_printf("load cert error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	cert_bio = BIO_new(BIO_s_mem());
	if (BIO_puts(cert_bio, (const char *)cert_pem) <= 0)
	{
		enclave_printf("fail to convert certificate\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	certificate = PEM_read_bio_X509(cert_bio, NULL, NULL, NULL);
	if (certificate == NULL)
	{
		enclave_printf("read bio x509 error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	cert_len = i2d_X509(certificate, (unsigned char **)&cert);
	if (cert_len <= 0)
	{
		enclave_printf("fail to convert cert DER\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get report
	if (getReportB64((unsigned char **)&report, &report_len) != 0)
	{
		enclave_printf("fail to get report\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// compose message
	msg_len = challenge_len + report_len + cert_len + KLV_LENGTH_SIZE * 3;
	msg = (unsigned char *)malloc(sizeof(char) * (msg_len));
	if (msg == NULL)
	{
		enclave_printf("fail to allocate memory for proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	if (uint32_serialization(msg, challenge_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE, challenge, challenge_len);
	if (uint32_serialization(msg + KLV_LENGTH_SIZE + challenge_len, report_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE * 2 + challenge_len, report, report_len);
	if (uint32_serialization(msg + KLV_LENGTH_SIZE * 2 + challenge_len + report_len, cert_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE * 3 + challenge_len + report_len, cert, cert_len);

	// sign
	key_type = EVP_PKEY_base_id(signing_key);
	switch (key_type)
	{
	case EVP_PKEY_RSA:
	{
		sig = (unsigned char *)malloc(sizeof(char) * RSA3072_LENGTH);
		if (sig == NULL)
		{
			enclave_printf("malloc failed for sig\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (RSA_PSS_sign(signing_key, (unsigned char *)msg, msg_len, (unsigned char *)sig, &sig_len) != CRYPTO_OK)
		{
			enclave_printf("fail to sign on challenge\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (sig_len != RSA3072_LENGTH)
		{
			enclave_printf("invalid signature length\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		break;
	}
	case EVP_PKEY_EC:
	{
		sig = (unsigned char *)malloc(sizeof(char) * ECDSA256_MAX_LENGTH);
		if (sig == NULL)
		{
			enclave_printf("malloc failed for sig\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (ECDSA_SHA256_sign(signing_key, (unsigned char *)msg, msg_len, (unsigned char *)sig, &sig_len) != CRYPTO_OK)
		{
			enclave_printf("fail to sign on challenge\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (sig_len > ECDSA256_MAX_LENGTH)
		{
			enclave_printf("invalid signature length\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		else {
			sig_len = ECDSA256_MAX_LENGTH;
		}
		break;
	}
	default:
	{
		enclave_printf("unsupported key type\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
		break;
	}
	}
	
	*proof_len = msg_len + sig_len + KLV_LENGTH_SIZE;

end_procedure:
	if (cert_pem != NULL)
	{
		free(cert_pem);
	}
	if (cert != NULL)
	{
		free(cert);
	}
	if (cert_bio != NULL)
	{
		BIO_free(cert_bio);
	}
	if (sig != NULL)
	{
		free(sig);
	}
	if (msg != NULL)
	{
		free(msg);
	}
	if (report != NULL)
	{
		free(report);
	}
	if (signing_key != NULL)
	{
		EVP_PKEY_free(signing_key);
	}
	if (certificate != NULL)
	{
		X509_free(certificate);
	}

	return ret;
}

int tee_remote_attestation_prove(char *proof, uint32_t proof_len, uint32_t *actual_proof_len, char *challenge, uint32_t challenge_len)
{
	int ret = ENCLAVE_OK, key_type;
	EVP_PKEY *signing_key = NULL;
	BIO *cert_bio = NULL;
	X509 *certificate = NULL;
	unsigned char *cert_pem = NULL, *cert = NULL, *sig = NULL, *msg = NULL, *report = NULL;
	size_t cert_pem_len, cert_len, sig_len, msg_len, report_len;

	if (proof == NULL)
	{
		enclave_printf("proof is unallocated\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	if (challenge == NULL)
	{
		enclave_printf("challenge is NULL\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get signing key
	if (getPriKey(&signing_key, (char *)SECRETKEY_FILE_PATH) != 0)
	{
		enclave_printf("load signing key error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get certificate
	if (read_file(&cert_pem, &cert_pem_len, (char *)TEE_CERT_FILE_PATH) != ENCLAVE_OK)
	{
		enclave_printf("load cert error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	cert_bio = BIO_new(BIO_s_mem());
	if (BIO_puts(cert_bio, (const char *)cert_pem) <= 0)
	{
		enclave_printf("fail to convert certificate\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	certificate = PEM_read_bio_X509(cert_bio, NULL, NULL, NULL);
	if (certificate == NULL)
	{
		enclave_printf("read bio x509 error\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	cert_len = i2d_X509(certificate, (unsigned char **)&cert);
	if (cert_len <= 0)
	{
		enclave_printf("fail to convert cert DER\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// get report
	if (getReportB64((unsigned char **)&report, &report_len) != 0)
	{
		enclave_printf("fail to get report\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	// compose message
	msg_len = challenge_len + report_len + cert_len + KLV_LENGTH_SIZE * 3;
	msg = (unsigned char *)malloc(sizeof(char) * (msg_len));
	if (msg == NULL)
	{
		enclave_printf("fail to allocate memory for proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	if (uint32_serialization(msg, challenge_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE, challenge, challenge_len);
	if (uint32_serialization(msg + KLV_LENGTH_SIZE + challenge_len, report_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE * 2 + challenge_len, report, report_len);
	if (uint32_serialization(msg + KLV_LENGTH_SIZE * 2 + challenge_len + report_len, cert_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof msg\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(msg + KLV_LENGTH_SIZE * 3 + challenge_len + report_len, cert, cert_len);

	// sign
	key_type = EVP_PKEY_base_id(signing_key);
	switch (key_type)
	{
	case EVP_PKEY_RSA:
	{
		sig = (unsigned char *)malloc(sizeof(char) * RSA3072_LENGTH);
		if (sig == NULL)
		{
			enclave_printf("malloc failed for sig\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (RSA_PSS_sign(signing_key, (unsigned char *)msg, msg_len, (unsigned char *)sig, &sig_len) != CRYPTO_OK)
		{
			enclave_printf("fail to sign on challenge\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (sig_len != RSA3072_LENGTH)
		{
			enclave_printf("invalid signature length\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		break;
	}
	case EVP_PKEY_EC:
	{
		sig = (unsigned char *)malloc(sizeof(char) * ECDSA256_MAX_LENGTH);
		if (sig == NULL)
		{
			enclave_printf("malloc failed for sig\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (ECDSA_SHA256_sign(signing_key, (unsigned char *)msg, msg_len, (unsigned char *)sig, &sig_len) != CRYPTO_OK)
		{
			enclave_printf("fail to sign on challenge\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		if (sig_len > ECDSA256_MAX_LENGTH)
		{
			enclave_printf("invalid signature length\n");
			ret = ENCLAVE_ERROR;
			goto end_procedure;
		}
		break;
	}
	default:
	{
		enclave_printf("unsupported key type\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
		break;
	}
	}

	if (proof_len >= msg_len + sig_len + KLV_LENGTH_SIZE)
	{
		*actual_proof_len = msg_len + sig_len + KLV_LENGTH_SIZE;
	}
	else
	{
		enclave_printf("allocated memory for proof is too small\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}

	memcpy(proof, msg, msg_len);
	if (uint32_serialization((unsigned char *)(proof + msg_len), sig_len) != ENCLAVE_OK)
	{
		enclave_printf("fail to compose proof\n");
		ret = ENCLAVE_ERROR;
		goto end_procedure;
	}
	memcpy(proof + msg_len + KLV_LENGTH_SIZE, sig, sig_len);

end_procedure:
	if (cert_pem != NULL)
	{
		free(cert_pem);
	}
	if (cert != NULL)
	{
		free(cert);
	}
	if (cert_bio != NULL)
	{
		BIO_free(cert_bio);
	}
	if (sig != NULL)
	{
		free(sig);
	}
	if (msg != NULL)
	{
		free(msg);
	}
	if (report != NULL)
	{
		free(report);
	}
	if (signing_key != NULL)
	{
		EVP_PKEY_free(signing_key);
	}
	if (certificate != NULL)
	{
		X509_free(certificate);
	}

	return ret;
}

int tee_get_report_size(uint32_t *reportSize)
{
    int ret = 0;
	unsigned char *reportB64 = NULL;
	size_t reportB64Size;

	ret = getReportB64(&reportB64, &reportB64Size);
	if (ret != 0)
	{
		enclave_printf("getReportB64 error in tee_get_report_size = %d\n", ret);
        if (reportB64 != NULL)
        {
            free(reportB64);
        }
        return ret;
	}
	enclave_printf("reportB64Size in tee_get_report_size = %d\n", reportB64Size);

	*reportSize = (size_t)reportB64Size;
	if (reportB64 != NULL)
	{
		free(reportB64);
	}
	return 0;
}

int tee_get_report(uint32_t reportSize, unsigned char *report)
{
	int ret = 0;
	int ret_code = 0;
	unsigned char *reportB64 = NULL;
	size_t reportB64Size;

	ret = getReportB64(&reportB64, &reportB64Size);
	if (ret != 0)
	{
		enclave_printf("getReportB64 error in tee_get_report = %d\n", ret);
		ret_code = 1;
		goto out;
	}
	enclave_printf("reportB64Size in tee_get_report = %d\n", reportB64Size);
	//enclave_printf("reportB64 in tee_get_report = %s\n", reportB64);

	if (reportB64Size > reportSize)
	{
		enclave_printf("invalid length[%d vs %d] in tee_get_report\n", reportSize, reportB64Size);
		ret_code = 2;
		goto out;
	}
	if (report == NULL)
	{
		enclave_printf("invalid report parameter in tee_get_report\n");
		ret_code = 3;
		goto out;
	}
	memcpy(report, reportB64, reportB64Size);

out:
	if (reportB64 != NULL)
	{
		free(reportB64);
	}

	return ret_code;
}
