/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#define CRYPTO_OK 0
#define CRYPTO_ERROR_OPENSSL -1
#define CRYPTO_ERROR_UNKNOWN -2

#define ENCLAVE_OK 0
#define ENCLAVE_ERROR -1

