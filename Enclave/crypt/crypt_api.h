/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef _CRYPT_API_H_
#define _CRYPT_API_H_

#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/ec.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#if defined(__cplusplus)
extern "C" {
#endif

#define AES_GCM_CIPHER_BLOCK_LENGTH 16
#define AES_GCM_TAG_LENGTH 16
#define AES_GCM_IV_LENGTH 12
#define CIPHERSIZE 3072
#define SHARE_LENGTH 32
#define RSA3072_LENGTH 384
#define ECDSA256_MAX_LENGTH 73
#define SEAL_KEY_LENGTH 16
#define SGX_KEYID_SIZE 32

#define KLV_LENGTH_SIZE 4

int tee_crypt_init(char *signAlgorithm);
int tee_remote_attestation_prove(char *proof, uint32_t proof_len, uint32_t *actual_proof_len, char *challenge, uint32_t challenge_len);
int tee_get_proof_size(char *challenge, uint32_t challenge_len, uint32_t *proof_len);

int tee_rsa_decrypt(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen, int pad_type);

int tee_cert_sign(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen);
int tee_cert_verify(unsigned char *data, size_t datalen, unsigned char *sig, size_t siglen);

int aes_gcm_encrypt(char* key, int key_length, char* iv, int iv_length, char* plaintext, int plain_length, char* ciphertext, int* cipher_length, char* tag);
int aes_gcm_decrypt(char* key, int key_length, char* iv, int iv_length, char* ciphertext, int cipher_length, char* tag, char* plaintext, int* plain_length);

int RSA_OAEP_encrypt(EVP_PKEY *pkey, unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen);
int RSA_OAEP_decrypt(EVP_PKEY *pkey, unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen);

int RSA_PSS_sign(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t *siglen);
int RSA_PSS_verify(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t siglen);

int ECDSA_SHA256_sign(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t *siglen);
int ECDSA_SHA256_verify(EVP_PKEY *pkey, unsigned char *msg, size_t msglen, unsigned char *sig, size_t siglen);

int getReportHash(unsigned char *reportHash, unsigned int *reportHashSize);

int tee_get_report_size(uint32_t *reportSize);
int tee_get_report(uint32_t reportSize, unsigned char *report);

void sgxssl_test();

#if defined(__cplusplus)
}
#endif

#endif
