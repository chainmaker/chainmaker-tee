/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave.h"
#include "Enclave_t.h" /* print_string */
#include "crypt_api.h"
#include "crypt_internal.h"

#include <stdarg.h>
#include <stdio.h> /* vsn//enclave_printf */
#include <string.h>
#include <sgx_tprotected_fs.h>

#include "errors.h"

int tee_cert_sign(unsigned char *in, size_t inlen, unsigned char *out, size_t *outlen) {
	int ret = 0;
	int key_type = 0;
	unsigned int hash_len = 0;
	size_t keylen = 0;
	EVP_PKEY *pkey = NULL;
	SGX_FILE *fp = NULL;
	BIO *prikey_bio;
	unsigned char *prikey_buff;
	unsigned char hash_in[32];
	uint8_t keyWord[SEAL_KEY_LENGTH] = {0};
        
	enclave_printf("tee_cert_sign in\n");
	prikey_bio = BIO_new(BIO_s_mem());
	if (prikey_bio == NULL) { 
		enclave_printf("new bio error\n");
	    return -1;
	}
	fp = sgx_fopen(SECRETKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		enclave_printf("sgx_fopen error\n");
		BIO_free(prikey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -3;
	}
	keylen = sgx_ftell(fp);
	if (0 != sgx_fseek(fp, 0, SEEK_SET)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -4;
	}
	prikey_buff = (unsigned char*)malloc(keylen);
	if (prikey_buff == NULL) {
		enclave_printf("malloc error\n");
		BIO_free(prikey_bio);
		sgx_fclose(fp);
		return -5;
	}
	memset(prikey_buff, 0, keylen);
	sgx_fread(prikey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("prikey in tee_cert_sign\n");
	//enclave_printf("%s\n", prikey_buff);
	BIO_puts(prikey_bio, (const char*)prikey_buff);
	ret = getKeyWord(keyWord);
	if(ret != 0){
		enclave_printf("tee_cert_sign getKeyWord error %d\n",ret);
		BIO_free(prikey_bio);
		return -6;
	}

	pkey = PEM_read_bio_PrivateKey(prikey_bio, NULL, pem_passphrase_cb, keyWord);
	if (pkey == NULL) {
		enclave_printf("read bio for eccprivtekey error\n");
		BIO_free(prikey_bio);
		return -7;
	}
	BIO_free(prikey_bio);
	key_type = EVP_PKEY_base_id(pkey);
	enclave_printf("key_type in tee_cert_sign: %d \n");

	if (key_type == EVP_PKEY_RSA) {
		enclave_printf("RSA signing in tee_cert_sign\n");
		ret = RSA_PSS_sign(pkey, in, inlen, out, outlen);
	} else {
		enclave_printf("ECC signing in tee_cert_sign\n");
		ret = ECDSA_SHA256_sign(pkey, in, inlen, out, outlen);
	}

	enclave_printf("ret for sign api in tee_cert_sign: %d \n", ret);
	EVP_PKEY_free(pkey);
	if(ret != CRYPTO_OK){
		enclave_printf("tee_cert_sign errori: %d\n",ret);
		return -8;
	}

	enclave_printf("out length of tee_cert_sign: %d\n", *outlen);
	enclave_print_hex_bytes(out, *outlen);
//	enclave_printf("out : \n");
//	for (int j=0; j<*outlen; j++) {
//		enclave_printf("%02x ", out[j]);
//	}
//	enclave_printf("\n");
	enclave_printf("tee_cert_sign out\n");
	return 0;	
}

int tee_cert_verify(unsigned char *data, size_t datalen, unsigned char *sig, size_t siglen) {
	int ret = 0;
	int key_type = 0;
	unsigned int hash_len = 0;
	size_t keylen = 0;
	EVP_PKEY *pkey = NULL;
	SGX_FILE *fp = NULL;
	BIO *pubkey_bio;
	unsigned char *pubkey_buff;

	enclave_printf("tee_cert_verify in\n");
	pubkey_bio = BIO_new(BIO_s_mem());
	if (pubkey_bio == NULL) { 
		enclave_printf("new bio error\n");
		return -1;
	}
	fp = sgx_fopen(PUBLICKEY_FILE_PATH, "rb", NULL);
	if (fp == NULL) {
		enclave_printf("sgx_fopen error\n");
		BIO_free(pubkey_bio);
		return -2;
	}
	if (0 != sgx_fseek(fp, 0, SEEK_END)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -3;
	}
	keylen = sgx_ftell(fp);
	if (0 != sgx_fseek(fp, 0, SEEK_SET)) {
		enclave_printf("sgx_fseek error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -4;
	}
	pubkey_buff = (unsigned char*)malloc(keylen);
	if (pubkey_buff == NULL) {
		enclave_printf("malloc error\n");
		BIO_free(pubkey_bio);
		sgx_fclose(fp);
		return -5;
	}
	memset(pubkey_buff, 0, keylen);
	sgx_fread(pubkey_buff, 1, keylen, fp);
	sgx_fclose(fp);
	//enclave_printf("tee cert pubkey\n");
	//enclave_printf("%s\n", pubkey_buff);
	BIO_puts(pubkey_bio, (const char*)pubkey_buff);
	pkey = PEM_read_bio_PUBKEY(pubkey_bio,NULL,NULL,NULL);
	if (pkey == NULL) {
		enclave_printf("read bio for teecert publickey error\n");
		BIO_free(pubkey_bio);
		return -6;
	}
	BIO_free(pubkey_bio);

	key_type = EVP_PKEY_base_id(pkey);
	enclave_printf("key_type in tee_cert_verify: %d \n");

	if (key_type == EVP_PKEY_RSA) {
		enclave_printf("RSA verifying in tee_cert_verify\n");
		ret = RSA_PSS_verify(pkey, data, datalen, sig, siglen);
	} else {
		enclave_printf("ECC verifying in tee_cert_verify\n");
		ret = ECDSA_SHA256_verify(pkey, data, datalen, sig, siglen);
	}

	enclave_printf("ret for verify api in tee_cert_verify: %d \n", ret);
	EVP_PKEY_free(pkey);
	if(ret != CRYPTO_OK){
		enclave_printf("tee_cert_verify error: %d\n",ret);
		return -7;
	}
	enclave_printf("tee_cert_verify out\n");
	return 0;	
}

