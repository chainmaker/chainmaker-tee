#include "Enclave_t.h"

#include "sgx_trts.h" /* for sgx_ocalloc, sgx_is_outside_enclave */
#include "sgx_lfence.h" /* for sgx_lfence */

#include <errno.h>
#include <mbusafecrt.h> /* for memcpy_s etc */
#include <stdlib.h> /* for malloc/free etc */

#define CHECK_REF_POINTER(ptr, siz) do {	\
	if (!(ptr) || ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define CHECK_UNIQUE_POINTER(ptr, siz) do {	\
	if ((ptr) && ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define CHECK_ENCLAVE_POINTER(ptr, siz) do {	\
	if ((ptr) && ! sgx_is_within_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define ADD_ASSIGN_OVERFLOW(a, b) (	\
	((a) += (b)) < (b)	\
)


typedef struct ms_sign_data_t {
	char* ms_retval;
	char* ms_data;
	uint32_t ms_data_len;
} ms_sign_data_t;

typedef struct ms_ecall_invoke_t {
	sgx_status_t ms_retval;
	char* ms_contract_name;
	size_t ms_contract_name_len;
	char* ms_version;
	size_t ms_version_len;
	unsigned char* ms_code_hash;
	uint32_t ms_hash_len;
	unsigned char* ms_private_rlp_data;
	uint32_t ms_data_len;
	unsigned char* ms_password;
	uint32_t ms_password_len;
	uint8_t* ms_byte_code;
	uint32_t ms_code_len;
	char* ms_private_req;
	uint32_t ms_private_req_len;
	uint32_t ms_is_deployment;
	evm_result_data_t* ms_evm_result;
} ms_ecall_invoke_t;

typedef struct ms_ecall_crypt_init_t {
	int ms_retval;
	char* ms_signAlgorithm;
	size_t ms_signAlgorithm_len;
} ms_ecall_crypt_init_t;

typedef struct ms_ecall_get_proof_size_t {
	int ms_retval;
	char* ms_challenge;
	uint32_t ms_challenge_len;
	uint32_t* ms_proof_len;
} ms_ecall_get_proof_size_t;

typedef struct ms_ecall_remote_attestation_prove_t {
	int ms_retval;
	char* ms_challenge;
	uint32_t ms_challenge_len;
	char* ms_proof;
	uint32_t ms_proof_len;
	uint32_t* ms_actual_proof_len;
} ms_ecall_remote_attestation_prove_t;

typedef struct ms_ecall_get_report_size_t {
	int ms_retval;
	uint32_t* ms_report_len;
} ms_ecall_get_report_size_t;

typedef struct ms_ecall_get_report_t {
	int ms_retval;
	uint32_t ms_report_len;
	unsigned char* ms_report;
} ms_ecall_get_report_t;

typedef struct ms_ocall_print_string_t {
	char* ms_str;
} ms_ocall_print_string_t;

typedef struct ms_ocall_get_t {
	int ms_retval;
	char* ms_contract_name;
	char* ms_key;
	char* ms_result;
	uint32_t* ms_result_len;
} ms_ocall_get_t;

typedef struct ms_ocall_export_csr_t {
	char* ms_csr;
} ms_ocall_export_csr_t;

typedef struct ms_ocall_export_pubkey_t {
	char* ms_key;
} ms_ocall_export_pubkey_t;

typedef struct ms_ocall_get_teecert_t {
	char** ms_cert;
	uint32_t* ms_cert_len;
} ms_ocall_get_teecert_t;

typedef struct ms_ocall_export_report_t {
	char* ms_report;
} ms_ocall_export_report_t;

typedef struct ms_u_sgxssl_ftime_t {
	void* ms_timeptr;
	uint32_t ms_timeb_len;
} ms_u_sgxssl_ftime_t;

typedef struct ms_sgx_oc_cpuidex_t {
	int* ms_cpuinfo;
	int ms_leaf;
	int ms_subleaf;
} ms_sgx_oc_cpuidex_t;

typedef struct ms_sgx_thread_wait_untrusted_event_ocall_t {
	int ms_retval;
	const void* ms_self;
} ms_sgx_thread_wait_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_set_untrusted_event_ocall_t {
	int ms_retval;
	const void* ms_waiter;
} ms_sgx_thread_set_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_setwait_untrusted_events_ocall_t {
	int ms_retval;
	const void* ms_waiter;
	const void* ms_self;
} ms_sgx_thread_setwait_untrusted_events_ocall_t;

typedef struct ms_sgx_thread_set_multiple_untrusted_events_ocall_t {
	int ms_retval;
	const void** ms_waiters;
	size_t ms_total;
} ms_sgx_thread_set_multiple_untrusted_events_ocall_t;

typedef struct ms_u_sgxprotectedfs_exclusive_file_open_t {
	void* ms_retval;
	const char* ms_filename;
	uint8_t ms_read_only;
	int64_t* ms_file_size;
	int32_t* ms_error_code;
} ms_u_sgxprotectedfs_exclusive_file_open_t;

typedef struct ms_u_sgxprotectedfs_check_if_file_exists_t {
	uint8_t ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_check_if_file_exists_t;

typedef struct ms_u_sgxprotectedfs_fread_node_t {
	int32_t ms_retval;
	void* ms_f;
	uint64_t ms_node_number;
	uint8_t* ms_buffer;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_fread_node_t;

typedef struct ms_u_sgxprotectedfs_fwrite_node_t {
	int32_t ms_retval;
	void* ms_f;
	uint64_t ms_node_number;
	uint8_t* ms_buffer;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_fwrite_node_t;

typedef struct ms_u_sgxprotectedfs_fclose_t {
	int32_t ms_retval;
	void* ms_f;
} ms_u_sgxprotectedfs_fclose_t;

typedef struct ms_u_sgxprotectedfs_fflush_t {
	uint8_t ms_retval;
	void* ms_f;
} ms_u_sgxprotectedfs_fflush_t;

typedef struct ms_u_sgxprotectedfs_remove_t {
	int32_t ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_remove_t;

typedef struct ms_u_sgxprotectedfs_recovery_file_open_t {
	void* ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_recovery_file_open_t;

typedef struct ms_u_sgxprotectedfs_fwrite_recovery_node_t {
	uint8_t ms_retval;
	void* ms_f;
	uint8_t* ms_data;
	uint32_t ms_data_length;
} ms_u_sgxprotectedfs_fwrite_recovery_node_t;

typedef struct ms_u_sgxprotectedfs_do_file_recovery_t {
	int32_t ms_retval;
	const char* ms_filename;
	const char* ms_recovery_filename;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_do_file_recovery_t;

static sgx_status_t SGX_CDECL sgx_sign_data(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_sign_data_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_sign_data_t* ms = SGX_CAST(ms_sign_data_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	char* _tmp_data = ms->ms_data;
	size_t _len_data = sizeof(char);
	char* _in_data = NULL;

	CHECK_UNIQUE_POINTER(_tmp_data, _len_data);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_data != NULL && _len_data != 0) {
		if ( _len_data % sizeof(*_tmp_data) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_data = (char*)malloc(_len_data);
		if (_in_data == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_data, _len_data, _tmp_data, _len_data)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}

	ms->ms_retval = sign_data(_in_data, ms->ms_data_len);

err:
	if (_in_data) free(_in_data);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_invoke(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_invoke_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_invoke_t* ms = SGX_CAST(ms_ecall_invoke_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	char* _tmp_contract_name = ms->ms_contract_name;
	size_t _len_contract_name = ms->ms_contract_name_len ;
	char* _in_contract_name = NULL;
	char* _tmp_version = ms->ms_version;
	size_t _len_version = ms->ms_version_len ;
	char* _in_version = NULL;
	unsigned char* _tmp_code_hash = ms->ms_code_hash;
	uint32_t _tmp_hash_len = ms->ms_hash_len;
	size_t _len_code_hash = _tmp_hash_len;
	unsigned char* _in_code_hash = NULL;
	unsigned char* _tmp_private_rlp_data = ms->ms_private_rlp_data;
	uint32_t _tmp_data_len = ms->ms_data_len;
	size_t _len_private_rlp_data = _tmp_data_len;
	unsigned char* _in_private_rlp_data = NULL;
	unsigned char* _tmp_password = ms->ms_password;
	uint32_t _tmp_password_len = ms->ms_password_len;
	size_t _len_password = _tmp_password_len;
	unsigned char* _in_password = NULL;
	uint8_t* _tmp_byte_code = ms->ms_byte_code;
	uint32_t _tmp_code_len = ms->ms_code_len;
	size_t _len_byte_code = _tmp_code_len;
	uint8_t* _in_byte_code = NULL;
	char* _tmp_private_req = ms->ms_private_req;
	uint32_t _tmp_private_req_len = ms->ms_private_req_len;
	size_t _len_private_req = _tmp_private_req_len;
	char* _in_private_req = NULL;
	evm_result_data_t* _tmp_evm_result = ms->ms_evm_result;
	size_t _len_evm_result = sizeof(evm_result_data_t);
	evm_result_data_t* _in_evm_result = NULL;

	CHECK_UNIQUE_POINTER(_tmp_contract_name, _len_contract_name);
	CHECK_UNIQUE_POINTER(_tmp_version, _len_version);
	CHECK_UNIQUE_POINTER(_tmp_code_hash, _len_code_hash);
	CHECK_UNIQUE_POINTER(_tmp_private_rlp_data, _len_private_rlp_data);
	CHECK_UNIQUE_POINTER(_tmp_password, _len_password);
	CHECK_UNIQUE_POINTER(_tmp_byte_code, _len_byte_code);
	CHECK_UNIQUE_POINTER(_tmp_private_req, _len_private_req);
	CHECK_UNIQUE_POINTER(_tmp_evm_result, _len_evm_result);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_contract_name != NULL && _len_contract_name != 0) {
		_in_contract_name = (char*)malloc(_len_contract_name);
		if (_in_contract_name == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_contract_name, _len_contract_name, _tmp_contract_name, _len_contract_name)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

		_in_contract_name[_len_contract_name - 1] = '\0';
		if (_len_contract_name != strlen(_in_contract_name) + 1)
		{
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}
	if (_tmp_version != NULL && _len_version != 0) {
		_in_version = (char*)malloc(_len_version);
		if (_in_version == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_version, _len_version, _tmp_version, _len_version)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

		_in_version[_len_version - 1] = '\0';
		if (_len_version != strlen(_in_version) + 1)
		{
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}
	if (_tmp_code_hash != NULL && _len_code_hash != 0) {
		if ( _len_code_hash % sizeof(*_tmp_code_hash) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_code_hash = (unsigned char*)malloc(_len_code_hash);
		if (_in_code_hash == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_code_hash, _len_code_hash, _tmp_code_hash, _len_code_hash)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_private_rlp_data != NULL && _len_private_rlp_data != 0) {
		if ( _len_private_rlp_data % sizeof(*_tmp_private_rlp_data) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_private_rlp_data = (unsigned char*)malloc(_len_private_rlp_data);
		if (_in_private_rlp_data == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_private_rlp_data, _len_private_rlp_data, _tmp_private_rlp_data, _len_private_rlp_data)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_password != NULL && _len_password != 0) {
		if ( _len_password % sizeof(*_tmp_password) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_password = (unsigned char*)malloc(_len_password);
		if (_in_password == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_password, _len_password, _tmp_password, _len_password)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_byte_code != NULL && _len_byte_code != 0) {
		if ( _len_byte_code % sizeof(*_tmp_byte_code) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_byte_code = (uint8_t*)malloc(_len_byte_code);
		if (_in_byte_code == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_byte_code, _len_byte_code, _tmp_byte_code, _len_byte_code)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_private_req != NULL && _len_private_req != 0) {
		if ( _len_private_req % sizeof(*_tmp_private_req) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_private_req = (char*)malloc(_len_private_req);
		if (_in_private_req == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_private_req, _len_private_req, _tmp_private_req, _len_private_req)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_evm_result != NULL && _len_evm_result != 0) {
		if ((_in_evm_result = (evm_result_data_t*)malloc(_len_evm_result)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_evm_result, 0, _len_evm_result);
	}

	ms->ms_retval = ecall_invoke(_in_contract_name, _in_version, _in_code_hash, _tmp_hash_len, _in_private_rlp_data, _tmp_data_len, _in_password, _tmp_password_len, _in_byte_code, _tmp_code_len, _in_private_req, _tmp_private_req_len, ms->ms_is_deployment, _in_evm_result);
	if (_in_evm_result) {
		if (memcpy_s(_tmp_evm_result, _len_evm_result, _in_evm_result, _len_evm_result)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

err:
	if (_in_contract_name) free(_in_contract_name);
	if (_in_version) free(_in_version);
	if (_in_code_hash) free(_in_code_hash);
	if (_in_private_rlp_data) free(_in_private_rlp_data);
	if (_in_password) free(_in_password);
	if (_in_byte_code) free(_in_byte_code);
	if (_in_private_req) free(_in_private_req);
	if (_in_evm_result) free(_in_evm_result);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_crypt_init(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_crypt_init_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_crypt_init_t* ms = SGX_CAST(ms_ecall_crypt_init_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	char* _tmp_signAlgorithm = ms->ms_signAlgorithm;
	size_t _len_signAlgorithm = ms->ms_signAlgorithm_len ;
	char* _in_signAlgorithm = NULL;

	CHECK_UNIQUE_POINTER(_tmp_signAlgorithm, _len_signAlgorithm);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_signAlgorithm != NULL && _len_signAlgorithm != 0) {
		_in_signAlgorithm = (char*)malloc(_len_signAlgorithm);
		if (_in_signAlgorithm == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_signAlgorithm, _len_signAlgorithm, _tmp_signAlgorithm, _len_signAlgorithm)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

		_in_signAlgorithm[_len_signAlgorithm - 1] = '\0';
		if (_len_signAlgorithm != strlen(_in_signAlgorithm) + 1)
		{
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

	ms->ms_retval = ecall_crypt_init(_in_signAlgorithm);

err:
	if (_in_signAlgorithm) free(_in_signAlgorithm);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_get_proof_size(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_get_proof_size_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_get_proof_size_t* ms = SGX_CAST(ms_ecall_get_proof_size_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	char* _tmp_challenge = ms->ms_challenge;
	uint32_t _tmp_challenge_len = ms->ms_challenge_len;
	size_t _len_challenge = _tmp_challenge_len;
	char* _in_challenge = NULL;
	uint32_t* _tmp_proof_len = ms->ms_proof_len;
	size_t _len_proof_len = sizeof(uint32_t);
	uint32_t* _in_proof_len = NULL;

	CHECK_UNIQUE_POINTER(_tmp_challenge, _len_challenge);
	CHECK_UNIQUE_POINTER(_tmp_proof_len, _len_proof_len);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_challenge != NULL && _len_challenge != 0) {
		if ( _len_challenge % sizeof(*_tmp_challenge) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_challenge = (char*)malloc(_len_challenge);
		if (_in_challenge == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_challenge, _len_challenge, _tmp_challenge, _len_challenge)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_proof_len != NULL && _len_proof_len != 0) {
		if ( _len_proof_len % sizeof(*_tmp_proof_len) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		if ((_in_proof_len = (uint32_t*)malloc(_len_proof_len)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_proof_len, 0, _len_proof_len);
	}

	ms->ms_retval = ecall_get_proof_size(_in_challenge, _tmp_challenge_len, _in_proof_len);
	if (_in_proof_len) {
		if (memcpy_s(_tmp_proof_len, _len_proof_len, _in_proof_len, _len_proof_len)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

err:
	if (_in_challenge) free(_in_challenge);
	if (_in_proof_len) free(_in_proof_len);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_remote_attestation_prove(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_remote_attestation_prove_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_remote_attestation_prove_t* ms = SGX_CAST(ms_ecall_remote_attestation_prove_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	char* _tmp_challenge = ms->ms_challenge;
	uint32_t _tmp_challenge_len = ms->ms_challenge_len;
	size_t _len_challenge = _tmp_challenge_len;
	char* _in_challenge = NULL;
	char* _tmp_proof = ms->ms_proof;
	uint32_t _tmp_proof_len = ms->ms_proof_len;
	size_t _len_proof = _tmp_proof_len;
	char* _in_proof = NULL;
	uint32_t* _tmp_actual_proof_len = ms->ms_actual_proof_len;
	size_t _len_actual_proof_len = sizeof(uint32_t);
	uint32_t* _in_actual_proof_len = NULL;

	CHECK_UNIQUE_POINTER(_tmp_challenge, _len_challenge);
	CHECK_UNIQUE_POINTER(_tmp_proof, _len_proof);
	CHECK_UNIQUE_POINTER(_tmp_actual_proof_len, _len_actual_proof_len);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_challenge != NULL && _len_challenge != 0) {
		if ( _len_challenge % sizeof(*_tmp_challenge) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		_in_challenge = (char*)malloc(_len_challenge);
		if (_in_challenge == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		if (memcpy_s(_in_challenge, _len_challenge, _tmp_challenge, _len_challenge)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}

	}
	if (_tmp_proof != NULL && _len_proof != 0) {
		if ( _len_proof % sizeof(*_tmp_proof) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		if ((_in_proof = (char*)malloc(_len_proof)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_proof, 0, _len_proof);
	}
	if (_tmp_actual_proof_len != NULL && _len_actual_proof_len != 0) {
		if ( _len_actual_proof_len % sizeof(*_tmp_actual_proof_len) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		if ((_in_actual_proof_len = (uint32_t*)malloc(_len_actual_proof_len)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_actual_proof_len, 0, _len_actual_proof_len);
	}

	ms->ms_retval = ecall_remote_attestation_prove(_in_challenge, _tmp_challenge_len, _in_proof, _tmp_proof_len, _in_actual_proof_len);
	if (_in_proof) {
		if (memcpy_s(_tmp_proof, _len_proof, _in_proof, _len_proof)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}
	if (_in_actual_proof_len) {
		if (memcpy_s(_tmp_actual_proof_len, _len_actual_proof_len, _in_actual_proof_len, _len_actual_proof_len)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

err:
	if (_in_challenge) free(_in_challenge);
	if (_in_proof) free(_in_proof);
	if (_in_actual_proof_len) free(_in_actual_proof_len);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_get_report_size(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_get_report_size_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_get_report_size_t* ms = SGX_CAST(ms_ecall_get_report_size_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	uint32_t* _tmp_report_len = ms->ms_report_len;
	size_t _len_report_len = sizeof(uint32_t);
	uint32_t* _in_report_len = NULL;

	CHECK_UNIQUE_POINTER(_tmp_report_len, _len_report_len);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_report_len != NULL && _len_report_len != 0) {
		if ( _len_report_len % sizeof(*_tmp_report_len) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		if ((_in_report_len = (uint32_t*)malloc(_len_report_len)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_report_len, 0, _len_report_len);
	}

	ms->ms_retval = ecall_get_report_size(_in_report_len);
	if (_in_report_len) {
		if (memcpy_s(_tmp_report_len, _len_report_len, _in_report_len, _len_report_len)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

err:
	if (_in_report_len) free(_in_report_len);
	return status;
}

static sgx_status_t SGX_CDECL sgx_ecall_get_report(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_ecall_get_report_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_ecall_get_report_t* ms = SGX_CAST(ms_ecall_get_report_t*, pms);
	sgx_status_t status = SGX_SUCCESS;
	unsigned char* _tmp_report = ms->ms_report;
	uint32_t _tmp_report_len = ms->ms_report_len;
	size_t _len_report = _tmp_report_len;
	unsigned char* _in_report = NULL;

	CHECK_UNIQUE_POINTER(_tmp_report, _len_report);

	//
	// fence after pointer checks
	//
	sgx_lfence();

	if (_tmp_report != NULL && _len_report != 0) {
		if ( _len_report % sizeof(*_tmp_report) != 0)
		{
			status = SGX_ERROR_INVALID_PARAMETER;
			goto err;
		}
		if ((_in_report = (unsigned char*)malloc(_len_report)) == NULL) {
			status = SGX_ERROR_OUT_OF_MEMORY;
			goto err;
		}

		memset((void*)_in_report, 0, _len_report);
	}

	ms->ms_retval = ecall_get_report(_tmp_report_len, _in_report);
	if (_in_report) {
		if (memcpy_s(_tmp_report, _len_report, _in_report, _len_report)) {
			status = SGX_ERROR_UNEXPECTED;
			goto err;
		}
	}

err:
	if (_in_report) free(_in_report);
	return status;
}

SGX_EXTERNC const struct {
	size_t nr_ecall;
	struct {void* ecall_addr; uint8_t is_priv; uint8_t is_switchless;} ecall_table[7];
} g_ecall_table = {
	7,
	{
		{(void*)(uintptr_t)sgx_sign_data, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_invoke, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_crypt_init, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_get_proof_size, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_remote_attestation_prove, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_get_report_size, 0, 0},
		{(void*)(uintptr_t)sgx_ecall_get_report, 0, 0},
	}
};

SGX_EXTERNC const struct {
	size_t nr_ocall;
	uint8_t entry_table[22][7];
} g_dyn_entry_table = {
	22,
	{
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
		{0, 0, 0, 0, 0, 0, 0, },
	}
};


sgx_status_t SGX_CDECL ocall_print_string(char* str)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_str = str ? strlen(str) + 1 : 0;

	ms_ocall_print_string_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_print_string_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(str, _len_str);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (str != NULL) ? _len_str : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_print_string_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_print_string_t));
	ocalloc_size -= sizeof(ms_ocall_print_string_t);

	if (str != NULL) {
		ms->ms_str = (char*)__tmp;
		if (_len_str % sizeof(*str) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, str, _len_str)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_str);
		ocalloc_size -= _len_str;
	} else {
		ms->ms_str = NULL;
	}
	
	status = sgx_ocall(0, ms);

	if (status == SGX_SUCCESS) {
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL ocall_get(int* retval, char* contract_name, char* key, char* result, uint32_t* result_len)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_contract_name = contract_name ? strlen(contract_name) + 1 : 0;
	size_t _len_key = key ? strlen(key) + 1 : 0;
	size_t _len_result = 32;
	size_t _len_result_len = sizeof(uint32_t);

	ms_ocall_get_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_get_t);
	void *__tmp = NULL;

	void *__tmp_result = NULL;
	void *__tmp_result_len = NULL;

	CHECK_ENCLAVE_POINTER(contract_name, _len_contract_name);
	CHECK_ENCLAVE_POINTER(key, _len_key);
	CHECK_ENCLAVE_POINTER(result, _len_result);
	CHECK_ENCLAVE_POINTER(result_len, _len_result_len);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (contract_name != NULL) ? _len_contract_name : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (key != NULL) ? _len_key : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (result != NULL) ? _len_result : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (result_len != NULL) ? _len_result_len : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_get_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_get_t));
	ocalloc_size -= sizeof(ms_ocall_get_t);

	if (contract_name != NULL) {
		ms->ms_contract_name = (char*)__tmp;
		if (_len_contract_name % sizeof(*contract_name) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, contract_name, _len_contract_name)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_contract_name);
		ocalloc_size -= _len_contract_name;
	} else {
		ms->ms_contract_name = NULL;
	}
	
	if (key != NULL) {
		ms->ms_key = (char*)__tmp;
		if (_len_key % sizeof(*key) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, key, _len_key)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_key);
		ocalloc_size -= _len_key;
	} else {
		ms->ms_key = NULL;
	}
	
	if (result != NULL) {
		ms->ms_result = (char*)__tmp;
		__tmp_result = __tmp;
		if (_len_result % sizeof(*result) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_result, 0, _len_result);
		__tmp = (void *)((size_t)__tmp + _len_result);
		ocalloc_size -= _len_result;
	} else {
		ms->ms_result = NULL;
	}
	
	if (result_len != NULL) {
		ms->ms_result_len = (uint32_t*)__tmp;
		__tmp_result_len = __tmp;
		if (_len_result_len % sizeof(*result_len) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_result_len, 0, _len_result_len);
		__tmp = (void *)((size_t)__tmp + _len_result_len);
		ocalloc_size -= _len_result_len;
	} else {
		ms->ms_result_len = NULL;
	}
	
	status = sgx_ocall(1, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
		if (result) {
			if (memcpy_s((void*)result, _len_result, __tmp_result, _len_result)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
		if (result_len) {
			if (memcpy_s((void*)result_len, _len_result_len, __tmp_result_len, _len_result_len)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL ocall_export_csr(char* csr)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_csr = csr ? strlen(csr) + 1 : 0;

	ms_ocall_export_csr_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_export_csr_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(csr, _len_csr);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (csr != NULL) ? _len_csr : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_export_csr_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_export_csr_t));
	ocalloc_size -= sizeof(ms_ocall_export_csr_t);

	if (csr != NULL) {
		ms->ms_csr = (char*)__tmp;
		if (_len_csr % sizeof(*csr) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, csr, _len_csr)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_csr);
		ocalloc_size -= _len_csr;
	} else {
		ms->ms_csr = NULL;
	}
	
	status = sgx_ocall(2, ms);

	if (status == SGX_SUCCESS) {
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL ocall_export_pubkey(char* key)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_key = key ? strlen(key) + 1 : 0;

	ms_ocall_export_pubkey_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_export_pubkey_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(key, _len_key);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (key != NULL) ? _len_key : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_export_pubkey_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_export_pubkey_t));
	ocalloc_size -= sizeof(ms_ocall_export_pubkey_t);

	if (key != NULL) {
		ms->ms_key = (char*)__tmp;
		if (_len_key % sizeof(*key) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, key, _len_key)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_key);
		ocalloc_size -= _len_key;
	} else {
		ms->ms_key = NULL;
	}
	
	status = sgx_ocall(3, ms);

	if (status == SGX_SUCCESS) {
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL ocall_get_teecert(char** cert, uint32_t* cert_len)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_cert = sizeof(char*);
	size_t _len_cert_len = sizeof(uint32_t);

	ms_ocall_get_teecert_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_get_teecert_t);
	void *__tmp = NULL;

	void *__tmp_cert = NULL;
	void *__tmp_cert_len = NULL;

	CHECK_ENCLAVE_POINTER(cert, _len_cert);
	CHECK_ENCLAVE_POINTER(cert_len, _len_cert_len);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (cert != NULL) ? _len_cert : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (cert_len != NULL) ? _len_cert_len : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_get_teecert_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_get_teecert_t));
	ocalloc_size -= sizeof(ms_ocall_get_teecert_t);

	if (cert != NULL) {
		ms->ms_cert = (char**)__tmp;
		__tmp_cert = __tmp;
		if (_len_cert % sizeof(*cert) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_cert, 0, _len_cert);
		__tmp = (void *)((size_t)__tmp + _len_cert);
		ocalloc_size -= _len_cert;
	} else {
		ms->ms_cert = NULL;
	}
	
	if (cert_len != NULL) {
		ms->ms_cert_len = (uint32_t*)__tmp;
		__tmp_cert_len = __tmp;
		if (_len_cert_len % sizeof(*cert_len) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_cert_len, 0, _len_cert_len);
		__tmp = (void *)((size_t)__tmp + _len_cert_len);
		ocalloc_size -= _len_cert_len;
	} else {
		ms->ms_cert_len = NULL;
	}
	
	status = sgx_ocall(4, ms);

	if (status == SGX_SUCCESS) {
		if (cert) {
			if (memcpy_s((void*)cert, _len_cert, __tmp_cert, _len_cert)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
		if (cert_len) {
			if (memcpy_s((void*)cert_len, _len_cert_len, __tmp_cert_len, _len_cert_len)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL ocall_export_report(char* report)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_report = report ? strlen(report) + 1 : 0;

	ms_ocall_export_report_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_ocall_export_report_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(report, _len_report);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (report != NULL) ? _len_report : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_ocall_export_report_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_ocall_export_report_t));
	ocalloc_size -= sizeof(ms_ocall_export_report_t);

	if (report != NULL) {
		ms->ms_report = (char*)__tmp;
		if (_len_report % sizeof(*report) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, report, _len_report)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_report);
		ocalloc_size -= _len_report;
	} else {
		ms->ms_report = NULL;
	}
	
	status = sgx_ocall(5, ms);

	if (status == SGX_SUCCESS) {
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxssl_ftime(void* timeptr, uint32_t timeb_len)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_timeptr = timeb_len;

	ms_u_sgxssl_ftime_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxssl_ftime_t);
	void *__tmp = NULL;

	void *__tmp_timeptr = NULL;

	CHECK_ENCLAVE_POINTER(timeptr, _len_timeptr);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (timeptr != NULL) ? _len_timeptr : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxssl_ftime_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxssl_ftime_t));
	ocalloc_size -= sizeof(ms_u_sgxssl_ftime_t);

	if (timeptr != NULL) {
		ms->ms_timeptr = (void*)__tmp;
		__tmp_timeptr = __tmp;
		memset(__tmp_timeptr, 0, _len_timeptr);
		__tmp = (void *)((size_t)__tmp + _len_timeptr);
		ocalloc_size -= _len_timeptr;
	} else {
		ms->ms_timeptr = NULL;
	}
	
	ms->ms_timeb_len = timeb_len;
	status = sgx_ocall(6, ms);

	if (status == SGX_SUCCESS) {
		if (timeptr) {
			if (memcpy_s((void*)timeptr, _len_timeptr, __tmp_timeptr, _len_timeptr)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_oc_cpuidex(int cpuinfo[4], int leaf, int subleaf)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_cpuinfo = 4 * sizeof(int);

	ms_sgx_oc_cpuidex_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_oc_cpuidex_t);
	void *__tmp = NULL;

	void *__tmp_cpuinfo = NULL;

	CHECK_ENCLAVE_POINTER(cpuinfo, _len_cpuinfo);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (cpuinfo != NULL) ? _len_cpuinfo : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_oc_cpuidex_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_oc_cpuidex_t));
	ocalloc_size -= sizeof(ms_sgx_oc_cpuidex_t);

	if (cpuinfo != NULL) {
		ms->ms_cpuinfo = (int*)__tmp;
		__tmp_cpuinfo = __tmp;
		if (_len_cpuinfo % sizeof(*cpuinfo) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_cpuinfo, 0, _len_cpuinfo);
		__tmp = (void *)((size_t)__tmp + _len_cpuinfo);
		ocalloc_size -= _len_cpuinfo;
	} else {
		ms->ms_cpuinfo = NULL;
	}
	
	ms->ms_leaf = leaf;
	ms->ms_subleaf = subleaf;
	status = sgx_ocall(7, ms);

	if (status == SGX_SUCCESS) {
		if (cpuinfo) {
			if (memcpy_s((void*)cpuinfo, _len_cpuinfo, __tmp_cpuinfo, _len_cpuinfo)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_wait_untrusted_event_ocall(int* retval, const void* self)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_wait_untrusted_event_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_wait_untrusted_event_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_wait_untrusted_event_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_wait_untrusted_event_ocall_t));
	ocalloc_size -= sizeof(ms_sgx_thread_wait_untrusted_event_ocall_t);

	ms->ms_self = self;
	status = sgx_ocall(8, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_set_untrusted_event_ocall(int* retval, const void* waiter)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_set_untrusted_event_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_set_untrusted_event_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_set_untrusted_event_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_set_untrusted_event_ocall_t));
	ocalloc_size -= sizeof(ms_sgx_thread_set_untrusted_event_ocall_t);

	ms->ms_waiter = waiter;
	status = sgx_ocall(9, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_setwait_untrusted_events_ocall(int* retval, const void* waiter, const void* self)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_sgx_thread_setwait_untrusted_events_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_setwait_untrusted_events_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_setwait_untrusted_events_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_setwait_untrusted_events_ocall_t));
	ocalloc_size -= sizeof(ms_sgx_thread_setwait_untrusted_events_ocall_t);

	ms->ms_waiter = waiter;
	ms->ms_self = self;
	status = sgx_ocall(10, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL sgx_thread_set_multiple_untrusted_events_ocall(int* retval, const void** waiters, size_t total)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_waiters = total * sizeof(void*);

	ms_sgx_thread_set_multiple_untrusted_events_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_sgx_thread_set_multiple_untrusted_events_ocall_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(waiters, _len_waiters);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (waiters != NULL) ? _len_waiters : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_sgx_thread_set_multiple_untrusted_events_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_sgx_thread_set_multiple_untrusted_events_ocall_t));
	ocalloc_size -= sizeof(ms_sgx_thread_set_multiple_untrusted_events_ocall_t);

	if (waiters != NULL) {
		ms->ms_waiters = (const void**)__tmp;
		if (_len_waiters % sizeof(*waiters) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, waiters, _len_waiters)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_waiters);
		ocalloc_size -= _len_waiters;
	} else {
		ms->ms_waiters = NULL;
	}
	
	ms->ms_total = total;
	status = sgx_ocall(11, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_exclusive_file_open(void** retval, const char* filename, uint8_t read_only, int64_t* file_size, int32_t* error_code)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_filename = filename ? strlen(filename) + 1 : 0;
	size_t _len_file_size = sizeof(int64_t);
	size_t _len_error_code = sizeof(int32_t);

	ms_u_sgxprotectedfs_exclusive_file_open_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_exclusive_file_open_t);
	void *__tmp = NULL;

	void *__tmp_file_size = NULL;
	void *__tmp_error_code = NULL;

	CHECK_ENCLAVE_POINTER(filename, _len_filename);
	CHECK_ENCLAVE_POINTER(file_size, _len_file_size);
	CHECK_ENCLAVE_POINTER(error_code, _len_error_code);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (filename != NULL) ? _len_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (file_size != NULL) ? _len_file_size : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (error_code != NULL) ? _len_error_code : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_exclusive_file_open_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_exclusive_file_open_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_exclusive_file_open_t);

	if (filename != NULL) {
		ms->ms_filename = (const char*)__tmp;
		if (_len_filename % sizeof(*filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, filename, _len_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_filename);
		ocalloc_size -= _len_filename;
	} else {
		ms->ms_filename = NULL;
	}
	
	ms->ms_read_only = read_only;
	if (file_size != NULL) {
		ms->ms_file_size = (int64_t*)__tmp;
		__tmp_file_size = __tmp;
		if (_len_file_size % sizeof(*file_size) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_file_size, 0, _len_file_size);
		__tmp = (void *)((size_t)__tmp + _len_file_size);
		ocalloc_size -= _len_file_size;
	} else {
		ms->ms_file_size = NULL;
	}
	
	if (error_code != NULL) {
		ms->ms_error_code = (int32_t*)__tmp;
		__tmp_error_code = __tmp;
		if (_len_error_code % sizeof(*error_code) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_error_code, 0, _len_error_code);
		__tmp = (void *)((size_t)__tmp + _len_error_code);
		ocalloc_size -= _len_error_code;
	} else {
		ms->ms_error_code = NULL;
	}
	
	status = sgx_ocall(12, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
		if (file_size) {
			if (memcpy_s((void*)file_size, _len_file_size, __tmp_file_size, _len_file_size)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
		if (error_code) {
			if (memcpy_s((void*)error_code, _len_error_code, __tmp_error_code, _len_error_code)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_check_if_file_exists(uint8_t* retval, const char* filename)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_filename = filename ? strlen(filename) + 1 : 0;

	ms_u_sgxprotectedfs_check_if_file_exists_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_check_if_file_exists_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(filename, _len_filename);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (filename != NULL) ? _len_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_check_if_file_exists_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_check_if_file_exists_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_check_if_file_exists_t);

	if (filename != NULL) {
		ms->ms_filename = (const char*)__tmp;
		if (_len_filename % sizeof(*filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, filename, _len_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_filename);
		ocalloc_size -= _len_filename;
	} else {
		ms->ms_filename = NULL;
	}
	
	status = sgx_ocall(13, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_fread_node(int32_t* retval, void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_buffer = node_size;

	ms_u_sgxprotectedfs_fread_node_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_fread_node_t);
	void *__tmp = NULL;

	void *__tmp_buffer = NULL;

	CHECK_ENCLAVE_POINTER(buffer, _len_buffer);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (buffer != NULL) ? _len_buffer : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_fread_node_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_fread_node_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_fread_node_t);

	ms->ms_f = f;
	ms->ms_node_number = node_number;
	if (buffer != NULL) {
		ms->ms_buffer = (uint8_t*)__tmp;
		__tmp_buffer = __tmp;
		if (_len_buffer % sizeof(*buffer) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		memset(__tmp_buffer, 0, _len_buffer);
		__tmp = (void *)((size_t)__tmp + _len_buffer);
		ocalloc_size -= _len_buffer;
	} else {
		ms->ms_buffer = NULL;
	}
	
	ms->ms_node_size = node_size;
	status = sgx_ocall(14, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
		if (buffer) {
			if (memcpy_s((void*)buffer, _len_buffer, __tmp_buffer, _len_buffer)) {
				sgx_ocfree();
				return SGX_ERROR_UNEXPECTED;
			}
		}
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_fwrite_node(int32_t* retval, void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_buffer = node_size;

	ms_u_sgxprotectedfs_fwrite_node_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_fwrite_node_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(buffer, _len_buffer);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (buffer != NULL) ? _len_buffer : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_fwrite_node_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_fwrite_node_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_fwrite_node_t);

	ms->ms_f = f;
	ms->ms_node_number = node_number;
	if (buffer != NULL) {
		ms->ms_buffer = (uint8_t*)__tmp;
		if (_len_buffer % sizeof(*buffer) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, buffer, _len_buffer)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_buffer);
		ocalloc_size -= _len_buffer;
	} else {
		ms->ms_buffer = NULL;
	}
	
	ms->ms_node_size = node_size;
	status = sgx_ocall(15, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_fclose(int32_t* retval, void* f)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_u_sgxprotectedfs_fclose_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_fclose_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_fclose_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_fclose_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_fclose_t);

	ms->ms_f = f;
	status = sgx_ocall(16, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_fflush(uint8_t* retval, void* f)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_u_sgxprotectedfs_fflush_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_fflush_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_fflush_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_fflush_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_fflush_t);

	ms->ms_f = f;
	status = sgx_ocall(17, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_remove(int32_t* retval, const char* filename)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_filename = filename ? strlen(filename) + 1 : 0;

	ms_u_sgxprotectedfs_remove_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_remove_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(filename, _len_filename);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (filename != NULL) ? _len_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_remove_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_remove_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_remove_t);

	if (filename != NULL) {
		ms->ms_filename = (const char*)__tmp;
		if (_len_filename % sizeof(*filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, filename, _len_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_filename);
		ocalloc_size -= _len_filename;
	} else {
		ms->ms_filename = NULL;
	}
	
	status = sgx_ocall(18, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_recovery_file_open(void** retval, const char* filename)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_filename = filename ? strlen(filename) + 1 : 0;

	ms_u_sgxprotectedfs_recovery_file_open_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_recovery_file_open_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(filename, _len_filename);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (filename != NULL) ? _len_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_recovery_file_open_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_recovery_file_open_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_recovery_file_open_t);

	if (filename != NULL) {
		ms->ms_filename = (const char*)__tmp;
		if (_len_filename % sizeof(*filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, filename, _len_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_filename);
		ocalloc_size -= _len_filename;
	} else {
		ms->ms_filename = NULL;
	}
	
	status = sgx_ocall(19, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_fwrite_recovery_node(uint8_t* retval, void* f, uint8_t* data, uint32_t data_length)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_data = data_length * sizeof(uint8_t);

	ms_u_sgxprotectedfs_fwrite_recovery_node_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_fwrite_recovery_node_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(data, _len_data);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (data != NULL) ? _len_data : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_fwrite_recovery_node_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_fwrite_recovery_node_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_fwrite_recovery_node_t);

	ms->ms_f = f;
	if (data != NULL) {
		ms->ms_data = (uint8_t*)__tmp;
		if (_len_data % sizeof(*data) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, data, _len_data)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_data);
		ocalloc_size -= _len_data;
	} else {
		ms->ms_data = NULL;
	}
	
	ms->ms_data_length = data_length;
	status = sgx_ocall(20, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

sgx_status_t SGX_CDECL u_sgxprotectedfs_do_file_recovery(int32_t* retval, const char* filename, const char* recovery_filename, uint32_t node_size)
{
	sgx_status_t status = SGX_SUCCESS;
	size_t _len_filename = filename ? strlen(filename) + 1 : 0;
	size_t _len_recovery_filename = recovery_filename ? strlen(recovery_filename) + 1 : 0;

	ms_u_sgxprotectedfs_do_file_recovery_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_u_sgxprotectedfs_do_file_recovery_t);
	void *__tmp = NULL;


	CHECK_ENCLAVE_POINTER(filename, _len_filename);
	CHECK_ENCLAVE_POINTER(recovery_filename, _len_recovery_filename);

	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (filename != NULL) ? _len_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;
	if (ADD_ASSIGN_OVERFLOW(ocalloc_size, (recovery_filename != NULL) ? _len_recovery_filename : 0))
		return SGX_ERROR_INVALID_PARAMETER;

	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_u_sgxprotectedfs_do_file_recovery_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_u_sgxprotectedfs_do_file_recovery_t));
	ocalloc_size -= sizeof(ms_u_sgxprotectedfs_do_file_recovery_t);

	if (filename != NULL) {
		ms->ms_filename = (const char*)__tmp;
		if (_len_filename % sizeof(*filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, filename, _len_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_filename);
		ocalloc_size -= _len_filename;
	} else {
		ms->ms_filename = NULL;
	}
	
	if (recovery_filename != NULL) {
		ms->ms_recovery_filename = (const char*)__tmp;
		if (_len_recovery_filename % sizeof(*recovery_filename) != 0) {
			sgx_ocfree();
			return SGX_ERROR_INVALID_PARAMETER;
		}
		if (memcpy_s(__tmp, ocalloc_size, recovery_filename, _len_recovery_filename)) {
			sgx_ocfree();
			return SGX_ERROR_UNEXPECTED;
		}
		__tmp = (void *)((size_t)__tmp + _len_recovery_filename);
		ocalloc_size -= _len_recovery_filename;
	} else {
		ms->ms_recovery_filename = NULL;
	}
	
	ms->ms_node_size = node_size;
	status = sgx_ocall(21, ms);

	if (status == SGX_SUCCESS) {
		if (retval) *retval = ms->ms_retval;
	}
	sgx_ocfree();
	return status;
}

