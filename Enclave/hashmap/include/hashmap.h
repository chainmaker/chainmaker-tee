/*
   The latest version of this library is available on GitHub;
   https://github.com/sheredom/hashmap.h
*/

/*
   This is free and unencumbered software released into the public domain.
   Anyone is free to copy, modify, publish, use, compile, sell, or
   distribute this software, either in source code form or as a compiled
   binary, for any purpose, commercial or non-commercial, and by any
   means.
   In jurisdictions that recognize copyright laws, the author or authors
   of this software dedicate any and all copyright interest in the
   software to the public domain. We make this dedication for the benefit
   of the public at large and to the detriment of our heirs and
   successors. We intend this dedication to be an overt act of
   relinquishment in perpetuity of all present and future rights to this
   software under copyright law.
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
   OTHER DEALINGS IN THE SOFTWARE.
   For more information, please refer to <http://unlicense.org/>
*/

#ifndef TEEEVM_HASHMAP_H
#define TEEEVM_HASHMAP_H

#include <stdint.h>
/* We need to keep keys and values. */
struct hashmap_element_s {
    const char *key;
    uint32_t key_len;
    int in_use;
    void *data;
    uint32_t data_len;
};

/* A hashmap has some maximum size and current size, as well as the data to
 * hold. */
struct hashmap_s {
    unsigned table_size;
    unsigned size;
    struct hashmap_element_s *data;
};

#define HASHMAP_MAX_CHAIN_LENGTH (8)

#define HASHMAP_CAST(type, x) ((type)x)
#define HASHMAP_PTR_CAST(type, x) ((type)x)

#define HASHMAP_NULL 0

/// @brief Create a hashmap.
/// @param initial_size The initial size of the hashmap. Must be a power of two.
/// @param out_hashmap The storage for the created hashmap.
/// @return On success 0 is returned.
///
/// Note that the initial size of the hashmap must be a power of two, and
/// creation of the hashmap will fail if this is not the case.
int hashmap_create(const unsigned initial_size,
                   struct hashmap_s *const out_hashmap);

/// @brief Put an element into the hashmap.
/// @param hashmap The hashmap to insert into.
/// @param key The string key to use.
/// @param len The length of the string key.
/// @param value The value to insert.
/// @return On success 0 is returned.
///
/// The key string slice is not copied when creating the hashmap entry, and thus
/// must remain a valid pointer until the hashmap entry is removed or the
/// hashmap is destroyed.
int hashmap_put(struct hashmap_s *const hashmap, const char *const key,
                const uint32_t key_len, void *const value, const uint32_t value_len);

/// @brief Get an element from the hashmap.
/// @param hashmap The hashmap to get from.
/// @param key The string key to use.
/// @param len The length of the string key.
/// @return The previously set element, or NULL if none exists.
void *hashmap_get(const struct hashmap_s *const hashmap,
                    const char *const key,
                    const unsigned len);

/// @brief Remove an element from the hashmap.
/// @param hashmap The hashmap to remove from.
/// @param key The string key to use.
/// @param len The length of the string key.
/// @return On success 0 is returned.
int hashmap_remove(struct hashmap_s *const hashmap,
                    const char *const key,
                    const unsigned len);

/// @brief Remove an element from the hashmap.
/// @param hashmap The hashmap to remove from.
/// @param key The string key to use.
/// @param len The length of the string key.
/// @return On success the original stored key pointer is returned, on failure
/// NULL is returned.
const char *
hashmap_remove_and_return_key(struct hashmap_s *const hashmap,
                              const char *const key,
                              const unsigned len);

/// @brief Iterate over all the elements in a hashmap.
/// @param hashmap The hashmap to iterate over.
/// @param f The function pointer to call on each element.
/// @param context The context to pass as the first argument to f.
/// @return If the entire hashmap was iterated then 0 is returned. Otherwise if
/// the callback function f returned non-zero then non-zero is returned.
int hashmap_iterate(const struct hashmap_s *const hashmap,
                    int (*f)(void *const context, void *const value),
                    void *const context);

/// @brief Iterate over all the elements in a hashmap.
/// @param hashmap The hashmap to iterate over.
/// @param f The function pointer to call on each element.
/// @param context The context to pass as the first argument to f.
/// @return If the entire hashmap was iterated then 0 is returned.
/// Otherwise if the callback function f returned positive then the positive
/// value is returned.  If the callback function returns -1, the current item
/// is removed and iteration continues.
int hashmap_iterate_pairs(struct hashmap_s *const hashmap,
                            int (*f)(void *const, struct hashmap_element_s *const),
                                    void *const context);

/// @brief Destroy the hashmap.
/// @param hashmap The hashmap to destroy.
void hashmap_destroy(struct hashmap_s *const hashmap);

static unsigned hashmap_crc32_helper(const char *const s,
                                     const unsigned len);

static unsigned hashmap_hash_helper_int_helper(const struct hashmap_s *const m,
                                               const char *const keystring,
                                               const unsigned len);

static int hashmap_match_helper(const struct hashmap_element_s *const element,
                                const char *const key,
                                const unsigned len);

static int hashmap_hash_helper(const struct hashmap_s *const m,
                               const char *const key, const unsigned len,
                               unsigned *const out_index);

static int hashmap_rehash_iterator(void *const new_hash,
                                struct hashmap_element_s *const e);

static int hashmap_rehash_helper(struct hashmap_s *const m);

int print_elem(void *const context, struct hashmap_element_s *const p_elem);

#endif