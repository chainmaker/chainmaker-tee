#ifndef ENCLAVE_T_H__
#define ENCLAVE_T_H__

#include <stdint.h>
#include <wchar.h>
#include <stddef.h>
#include "sgx_edger8r.h" /* for sgx_ocall etc. */

#include "user_types.h"
#include "/opt/intel/sgxsdk/include/sgx_edger8r.h"
#include "/opt/intel/sgxsdk/include/sgx_report.h"
#include "/opt/intel/sgxsdk/include/sgx_utils.h"
#include "/opt/intel/sgxsdk/include/sgx_tcrypto.h"

#include <stdlib.h> /* for size_t */

#define SGX_CAST(type, item) ((type)(item))

#ifdef __cplusplus
extern "C" {
#endif

char* sign_data(char* data, uint32_t data_len);
sgx_status_t ecall_invoke(char* contract_name, char* version, unsigned char* code_hash, uint32_t hash_len, unsigned char* private_rlp_data, uint32_t data_len, unsigned char* password, uint32_t password_len, uint8_t* byte_code, uint32_t code_len, char* private_req, uint32_t private_req_len, uint32_t is_deployment, evm_result_data_t* evm_result);
int ecall_crypt_init(char* signAlgorithm);
int ecall_get_proof_size(char* challenge, uint32_t challenge_len, uint32_t* proof_len);
int ecall_remote_attestation_prove(char* challenge, uint32_t challenge_len, char* proof, uint32_t proof_len, uint32_t* actual_proof_len);
int ecall_get_report_size(uint32_t* report_len);
int ecall_get_report(uint32_t report_len, unsigned char* report);

sgx_status_t SGX_CDECL ocall_print_string(char* str);
sgx_status_t SGX_CDECL ocall_get(int* retval, char* contract_name, char* key, char* result, uint32_t* result_len);
sgx_status_t SGX_CDECL ocall_export_csr(char* csr);
sgx_status_t SGX_CDECL ocall_export_pubkey(char* key);
sgx_status_t SGX_CDECL ocall_get_teecert(char** cert, uint32_t* cert_len);
sgx_status_t SGX_CDECL ocall_export_report(char* report);
sgx_status_t SGX_CDECL u_sgxssl_ftime(void* timeptr, uint32_t timeb_len);
sgx_status_t SGX_CDECL sgx_oc_cpuidex(int cpuinfo[4], int leaf, int subleaf);
sgx_status_t SGX_CDECL sgx_thread_wait_untrusted_event_ocall(int* retval, const void* self);
sgx_status_t SGX_CDECL sgx_thread_set_untrusted_event_ocall(int* retval, const void* waiter);
sgx_status_t SGX_CDECL sgx_thread_setwait_untrusted_events_ocall(int* retval, const void* waiter, const void* self);
sgx_status_t SGX_CDECL sgx_thread_set_multiple_untrusted_events_ocall(int* retval, const void** waiters, size_t total);
sgx_status_t SGX_CDECL u_sgxprotectedfs_exclusive_file_open(void** retval, const char* filename, uint8_t read_only, int64_t* file_size, int32_t* error_code);
sgx_status_t SGX_CDECL u_sgxprotectedfs_check_if_file_exists(uint8_t* retval, const char* filename);
sgx_status_t SGX_CDECL u_sgxprotectedfs_fread_node(int32_t* retval, void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size);
sgx_status_t SGX_CDECL u_sgxprotectedfs_fwrite_node(int32_t* retval, void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size);
sgx_status_t SGX_CDECL u_sgxprotectedfs_fclose(int32_t* retval, void* f);
sgx_status_t SGX_CDECL u_sgxprotectedfs_fflush(uint8_t* retval, void* f);
sgx_status_t SGX_CDECL u_sgxprotectedfs_remove(int32_t* retval, const char* filename);
sgx_status_t SGX_CDECL u_sgxprotectedfs_recovery_file_open(void** retval, const char* filename);
sgx_status_t SGX_CDECL u_sgxprotectedfs_fwrite_recovery_node(uint8_t* retval, void* f, uint8_t* data, uint32_t data_length);
sgx_status_t SGX_CDECL u_sgxprotectedfs_do_file_recovery(int32_t* retval, const char* filename, const char* recovery_filename, uint32_t node_size);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
