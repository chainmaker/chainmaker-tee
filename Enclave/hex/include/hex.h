/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#ifndef TEEEVM_HEX_H
#define TEEEVM_HEX_H

int decode_hex(unsigned char *in, int len, unsigned char *out);
int encode_hex(unsigned char *in, int len, unsigned char *out);

#endif //TEEEVM_HEX_H
