/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include<stdlib.h>
#include<stdio.h>

int decode_hex(char *in, int len, unsigned char *out) {
    if ((len % 2) != 0) {
        enclave_printf("invalid hex str len");
        return -1;
    }

    for (int i = 0; i < len; i+=2) {
        char i_char = in[i];
        char ai_char = in[i + 1];
        if (i_char >= '0' && i_char <= '9') {
            out[i/2] = (i_char & ~0x30) << 4;
        } else if (i_char >= 'a' && i_char <= 'f') {
            out[i/2] = (i_char - 'a') + 10;
        } else if (i_char >= 'A' && i_char <= 'F') {
            out[i/2] = (i_char - 'A') + 10;
        } else {
            enclave_printf("invalid hex char %c", i_char);
            return -1;
        }

        if (ai_char >= '0' && ai_char <= '9') {
            out[i/2] |= (ai_char & ~0x30) << 4;
        } else if (i_char >= 'a' && i_char <= 'f') {
            out[i/2] |= (ai_char - 'a') + 10;
        } else if (i_char >= 'A' && i_char <= 'F') {
            out[i/2] |= (ai_char - 'A') + 10;
        } else {
            enclave_printf("invalid hex char %c", ai_char);
            return -1;
        }
    }
    return 0;
}

int encode_hex(unsigned char *in, int len, unsigned char *out) {
    if (len <= 0) {
        enclave_printf("encode_hex len error, len: %d", len);
        return -1;
    }

    unsigned char high_byte, low_byte;
    for (int i = 0; i < len; i++) {
        high_byte = in[i] >> 4;
        low_byte = in[i] & 0x0f;

        high_byte += 0x30;
        if (high_byte > 0x39) {
            out[i * 2] = high_byte + 0x27;
        } else {
            out[i * 2] = high_byte;
        }

        low_byte += 0x30;
        if (low_byte > 0x39) {
            out[i * 2 + 1] = low_byte + 0x27;
        } else {
            out[i * 2 + 1] = low_byte;
        }
    }
    return 0;
}