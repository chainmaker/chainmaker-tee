/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdint.h>
#include <stdio.h>
#ifndef ENCLAVE_LOG_H
#define ENCLAVE_LOG_H

#define EVM_TRACE
//#define EVM_ALONE

#define LOG_BUFSIZE 8192
int enclave_printf(const char* fmt, ...);
void enclave_print_hex_bytes(unsigned char *content, uint32_t len);

#endif //ENCLAVE_LOG_H
