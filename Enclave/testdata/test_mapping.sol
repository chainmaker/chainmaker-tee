// SPDX-License-Identifier: SimPL-2.0

pragma solidity >0.4.11;

contract Test {
    uint256 param1;
    mapping(uint256 => uint256) mapping_u;
    mapping(string => string) mapping_s;

    constructor() {
        mapping_u[0xBBBB] = 0xBBBB;
        mapping_s["I am original key, I have 58 character. +12345678901234567"] = "I am original value, I have 58 character. +123456789012345";
    }

    function put_u256(uint256 key, uint256 value) public {
        mapping_u[key] = value;
    }

    function put_string(string memory key, string memory value) public {
        mapping_s[key] = value;
    }

    function load_u256(uint256 key) public returns(uint256) {
        return mapping_u[key];
    }

    function load_string(string memory key) public returns(string memory) {
        return mapping_s[key];
    }
}