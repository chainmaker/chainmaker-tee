/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include "Enclave_t.h" /* print_string */
#include "Enclave.h"
#include "crypt_api.h"
#include <stdarg.h>
#include <stdio.h> /* vsn//printf */
#include <string.h>
#include <sgx_report.h>
#include <sgx_utils.h>
// #include <sgx_tcrypto.h>

#include "openssl/ec.h"
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include "enclave_log.h"

#include <stdio.h>
#include "evm.h"
#include "hex.h"

#define IV_LENGTH 12
#define TAG_LENGTH 16
#define MAX_DEPLOYED_OR_RESULT_LENGTH 3000
//uint8_t  g_deployed_or_result[MAX_DEPLOYED_OR_RESULT_LENGTH];
//uint64_t g_deployed_or_result_len = 0;

int ecall_crypt_init(char *signAlgorithm){
    enclave_printf("in ecall_crypt_init: %s", signAlgorithm);
    return tee_crypt_init(signAlgorithm);
}

int ecall_remote_attestation_prove(char *challenge, uint32_t challenge_len, char *proof, uint32_t proof_len, uint32_t *actual_proof_len)
{
    enclave_printf("generate proof");
    return tee_remote_attestation_prove(proof, proof_len, actual_proof_len, challenge, challenge_len);
}

int ecall_get_proof_size(char *challenge, uint32_t challenge_len, uint32_t *proof_len)
{
    enclave_printf("get proof size");
    return tee_get_proof_size(challenge, challenge_len, proof_len);
}

int ecall_get_report_size(uint32_t *report_len)
{
    enclave_printf("ecall_get_report_size");
    return tee_get_report_size(report_len);
}

int ecall_get_report(uint32_t report_len, unsigned char *report)
{
    enclave_printf("ecall_get_report_size");
    return tee_get_report(report_len, report);
}

int convert_element_to_rset(void *const context, struct hashmap_element_s *const p_elem) {
    if (p_elem->key_len > MAX_KEY_LENGTH || p_elem->data_len > MAX_VALUE_LENGTH) {
        return -1;
    }
    evm_result_data_t *result = (evm_result_data_t*)context;
    RSet *rset = &result->rsets[result->rset_pos++];
    memcpy(rset->key, p_elem->key, p_elem->key_len);
    rset->key_len = p_elem->key_len;
    memcpy(rset->value, p_elem->data, p_elem->data_len);
    rset->value_len = p_elem->data_len;
//    rset->version = malloc(strlen(p_elem->version));
    return 0;
}

int convert_element_to_wset(void *const context, struct hashmap_element_s *const p_elem) {
    if (p_elem->key_len > MAX_KEY_LENGTH || p_elem->data_len > MAX_VALUE_LENGTH) {
        return -1;
    }
    evm_result_data_t *result = (evm_result_data_t*)context;
    WSet *wset = &result->wsets[result->wset_pos++];
    memcpy(wset->key, p_elem->key, p_elem->key_len);
    wset->key_len = p_elem->key_len;
    memcpy(wset->value, p_elem->data, p_elem->data_len);
    wset->value_len = p_elem->data_len;
    return 0;
}

sgx_status_t ecall_invoke(char *contract_name, char *version, unsigned char *code_hash, uint32_t hash_len,
    unsigned char *private_rlp_data, uint32_t data_len, unsigned char *password, uint32_t password_len,
    uint8_t *byte_code, uint32_t code_len, char *private_req, uint32_t private_req_len, uint32_t is_deployment,
    evm_result_data_t *evm_result) {

    int ret = 0;

    unsigned char hex_rlp_data[data_len * 2 + 1];
    for(int i=0;i<(data_len * 2 + 1);i++) {
        hex_rlp_data[i] = 0;
    }
    encode_hex((unsigned char *)private_rlp_data, data_len, hex_rlp_data);
    enclave_printf("original rlp data: %s, len: %d", hex_rlp_data, data_len);
	// decrypt
	if (data_len < (IV_LENGTH + TAG_LENGTH)) {
		enclave_printf("original rlp data: %s, data_len is too short(%d<(%d+%d))",
		    hex_rlp_data, data_len, IV_LENGTH, TAG_LENGTH);
		return SGX_ERROR_INVALID_PARAMETER;
	}

    unsigned char *decrypt_offset = private_rlp_data;
    unsigned char iv[IV_LENGTH];
    unsigned char tag[TAG_LENGTH];
	// copy iv
	memcpy(iv, decrypt_offset, IV_LENGTH);
	decrypt_offset += IV_LENGTH;
    // copy crypto data
    int crypto_data_len = data_len - IV_LENGTH - TAG_LENGTH;
	unsigned char *crypto_data = (unsigned char *)malloc(crypto_data_len);
	if (crypto_data == NULL) {
	    return SGX_ERROR_OUT_OF_MEMORY;
	}
	memcpy(crypto_data, decrypt_offset, crypto_data_len);
    // copy tag
	decrypt_offset += crypto_data_len;
	memcpy(tag, decrypt_offset, TAG_LENGTH);

    enclave_printf("original rlp data: %s, contract name: %s", hex_rlp_data, contract_name);
    unsigned char hex_password[password_len * 2 + 1];
    for(int i=0;i<(password_len * 2 + 1);i++) {
        hex_password[i] = 0;
    }
    encode_hex((unsigned char *)password, password_len, hex_password);
    enclave_printf("original rlp data: %s, password: %s, len: %d",
        hex_rlp_data, hex_password, password_len);
    unsigned char rsa_out[384];
    size_t rsa_outlen = 384;
    ret = tee_rsa_decrypt(password, password_len, rsa_out, &rsa_outlen, 0);
    if (ret != 0) {
        enclave_printf("original rlp data: %s, rsa decrypt ret: %d", hex_rlp_data, ret);
        free(crypto_data);
        return SGX_ERROR_UNEXPECTED;
    }
    enclave_printf("original rlp data: %s, rsa decrypt rsa_outlen: %d", hex_rlp_data, rsa_outlen);
    if (rsa_outlen <= 0) {
        enclave_printf("original rlp data: %s, rsa decrypt rsa_outlen error !", hex_rlp_data);
        free(crypto_data);
        return SGX_ERROR_UNEXPECTED;
    }
    int decrypted_rlp_data_len;
    char decrypted_rlp_data[MAX_DECODED_RLP_DATA_LENGTH] = {0};
    ret = aes_gcm_decrypt((char *) rsa_out, rsa_outlen, (char *) iv, IV_LENGTH, (char *) crypto_data, crypto_data_len,
    	(char *) tag, decrypted_rlp_data, &decrypted_rlp_data_len);
    free(crypto_data);
    if (ret != 0) {
        enclave_printf("original rlp data: %s, aes decrypt fail ret: %d", hex_rlp_data, ret);
        return SGX_ERROR_UNEXPECTED;
    }

    enclave_printf("original rlp data: %s, aes decrypted rlp data length: %d", hex_rlp_data, decrypted_rlp_data_len);
    if (decrypted_rlp_data_len <= 0) {
        enclave_printf("original rlp data: %s, aes decrypt decrypted_rlp_data_len error !", hex_rlp_data);
        return SGX_ERROR_UNEXPECTED;
    }
    struct hashmap_s read_set;
    struct hashmap_s write_set;
    if (hashmap_create(64, &read_set) != 0) {
        enclave_printf("create read_set hashmap failed");
        return SGX_ERROR_OUT_OF_MEMORY;
    }
    if (hashmap_create(64, &write_set) != 0) {
        enclave_printf("create write_set hashmap failed");
        return SGX_ERROR_OUT_OF_MEMORY;
    }

    Machine *main_vm = (Machine *)malloc(sizeof(Machine));
    if (main_vm == NULL) {
        return SGX_ERROR_OUT_OF_MEMORY;
    }
    init_machine(main_vm, &read_set, &write_set);
    unsigned char *full_codes;
    int32_t full_codes_length;
    if (is_deployment) {
        full_codes_length = code_len + decrypted_rlp_data_len;
        full_codes = (unsigned char *)malloc(full_codes_length);
        if (full_codes == NULL) {
            return SGX_ERROR_OUT_OF_MEMORY;
        }
        memcpy(full_codes, byte_code, code_len);
        memcpy(full_codes + code_len, decrypted_rlp_data, decrypted_rlp_data_len);
    } else {
        full_codes = byte_code;
        full_codes_length = code_len;
        memcpy(main_vm->message.data, decrypted_rlp_data, decrypted_rlp_data_len);
        main_vm->message.datasize = decrypted_rlp_data_len;
    }

    UPPER(UPPER(main_vm->message.caller)) = 0xca35b7d915;
    UPPER(LOWER(main_vm->message.caller)) = 0x00000000ca35b7d9;
    LOWER(UPPER(main_vm->message.caller)) = 0x15458ef540ade606;
    LOWER(LOWER(main_vm->message.caller)) = 0x8dfe2f44e8fa733c;

    UPPER(UPPER(main_vm->message.call_value)) = 0x00;
    UPPER(LOWER(main_vm->message.call_value)) = 0x00;
    LOWER(UPPER(main_vm->message.call_value)) = 0x00;
    LOWER(LOWER(main_vm->message.call_value)) = 0x00;

    unsigned char hex_code[code_len * 2 + 1];
    for(int i=0;i<(code_len * 2 + 1);i++) {
        hex_code[i] = 0;
    }
    encode_hex((unsigned char *)byte_code, code_len, hex_code);
    enclave_printf("original rlp data: %s, original contract: %s, len: %d", hex_rlp_data, hex_code, code_len);
    main_vm->message.codesize = full_codes_length;
    int32_t result = execute_contract( main_vm, contract_name, full_codes, full_codes_length);
    if (is_deployment) {
        free(full_codes);
    }
    uint8_t *result_data_ptr = main_vm->result_data_ptr;
    uint32_t result_data_size = main_vm->result_data_size;
    if (result == EVM_ERROR) {
        if (result_data_size) {
            free(result_data_ptr);
        }
        free(main_vm);
        hashmap_destroy(&read_set);
        hashmap_destroy(&write_set);
        return SGX_ERROR_UNEXPECTED;
    }

    enclave_printf("original rlp data: %s, VM->pc %p ", hex_rlp_data, main_vm->PC);
    unsigned char hex_result[result_data_size * 2 + 1];
    for(int i=0;i<(result_data_size * 2 + 1);i++) {
        hex_result[i] = 0;
    }
    encode_hex((unsigned char *)result_data_ptr, result_data_size, hex_result);
    enclave_printf("original rlp data: %s, result: %s, len: %d", hex_rlp_data, hex_result, result_data_size);

    memcpy(evm_result->result, result_data_ptr, result_data_size);
    evm_result->result_len = result_data_size;
    if (is_deployment > 0) {
        int code_header_len = main_vm->PC+2;
        memcpy(evm_result->code_header, byte_code, code_header_len);
	    evm_result->code_header_len = code_header_len;

        unsigned char hex_code_header[code_header_len * 2 + 1];
        for(int i=0;i<(code_header_len * 2 + 1);i++) {
            hex_code_header[i] = 0;
        }
        encode_hex((unsigned char *)byte_code, code_header_len, hex_code_header);
        enclave_printf("original rlp data: %s, code header: %s, len: %ld", hex_rlp_data, hex_code_header, code_header_len);
    }

    enclave_printf("original rlp data: %s, Read set => ", hex_rlp_data);
    hashmap_iterate_pairs(&read_set, print_elem, NULL);
    enclave_printf("original rlp data: %s, Write set => ", hex_rlp_data);
    hashmap_iterate_pairs(&write_set, print_elem, NULL);
    hashmap_iterate_pairs(&read_set, convert_element_to_rset, evm_result);
    hashmap_iterate_pairs(&write_set, convert_element_to_wset, evm_result);
    //release all memory
    free(result_data_ptr);
    hashmap_destroy(&read_set);
    hashmap_destroy(&write_set);
    free(main_vm);
    uint32_t contract_name_len = strlen(contract_name);
    if (contract_name_len > MAX_NAME_LENGTH) {
        enclave_printf("original rlp data: %s, contract name too long, max len: %d", hex_rlp_data, MAX_NAME_LENGTH);
        return SGX_ERROR_UNEXPECTED;
    }

    if (private_req_len > MAX_PRIVATE_REQ_LENGTH) {
        enclave_printf("original rlp data: %s, private_req is too big, max len: %d", hex_rlp_data, MAX_PRIVATE_REQ_LENGTH);
        return SGX_ERROR_UNEXPECTED;
    }

    memcpy(evm_result->contract_name, contract_name, contract_name_len);
    evm_result->name_len = contract_name_len;
    evm_result->code = 0;
    evm_result->gas = main_vm->GAS_Charge;
    memcpy(evm_result->code_version, version, strlen(version));
    evm_result->version_len = strlen(version);
    memcpy(evm_result->code_hash, code_hash, hash_len);
    evm_result->code_hash_len = hash_len;
    unsigned int report_hash_len = 0;
    ret = getReportHash((unsigned char *)evm_result->report_hash, &report_hash_len);
    if (ret < 0) {
        enclave_printf("original rlp data: %s, get report hash failed: %d", hex_rlp_data, ret);
        return SGX_ERROR_UNEXPECTED;
    }
    evm_result->report_hash_len = report_hash_len;
    memcpy(evm_result->private_req, private_req, private_req_len);
    evm_result->private_req_len = private_req_len;

	// sign
	size_t serial_evm_result_len = sizeof(uint32_t);                    // code
    serial_evm_result_len += sizeof(uint32_t) + evm_result->result_len; // result_len + result
    serial_evm_result_len += sizeof(int64_t);                           // gas

   	serial_evm_result_len += sizeof(uint32_t);
	for (int i = 0; i < evm_result->rset_pos; i++) {
		serial_evm_result_len += sizeof(uint32_t) + evm_result->rsets[i].key_len;
        serial_evm_result_len += sizeof(uint32_t) + evm_result->rsets[i].value_len;
        serial_evm_result_len += sizeof(uint32_t) + evm_result->rsets[i].version_len;
	}
	serial_evm_result_len += sizeof(uint32_t);
	for (int i = 0; i < evm_result->wset_pos; i++){
		serial_evm_result_len += sizeof(uint32_t) + evm_result->wsets[i].key_len;
        serial_evm_result_len += sizeof(uint32_t) + evm_result->wsets[i].value_len;
	}
    serial_evm_result_len += sizeof(uint32_t) + evm_result->name_len;       // name_len + contract_name
    serial_evm_result_len += sizeof(uint32_t) + evm_result->version_len;    // version_len + code_version
	serial_evm_result_len += sizeof(uint32_t) + evm_result->code_hash_len;  // code_hash_len + code_hash
    serial_evm_result_len += sizeof(uint32_t) + evm_result->report_hash_len;  // report_hash_len + report_hash
    serial_evm_result_len += sizeof(uint32_t) + evm_result->private_req_len;    // private_req_len + private_req
    serial_evm_result_len += sizeof(uint32_t) + evm_result->code_header_len;    // code_header_len + code_header
    // serial evm result
	unsigned char *serial_evm_result = (unsigned char *)malloc(serial_evm_result_len);
	if (serial_evm_result == NULL) {
	    return SGX_ERROR_OUT_OF_MEMORY;
	}
	unsigned char *sign_offset = serial_evm_result;
	//code
    uint32_to_little_endian(evm_result->code, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // result_len
    uint32_to_little_endian(evm_result->result_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // result
	memcpy(sign_offset, evm_result->result, evm_result->result_len);
    sign_offset += evm_result->result_len;
	// gas
	//memcpy(sign_offset, (char *)&evm_result->gas, sizeof(int64_t));
    uint64_to_little_endian(evm_result->gas, sign_offset, 0);
	sign_offset += sizeof(int64_t);
	// rsets
	uint32_to_little_endian(evm_result->rset_pos, sign_offset, 0);
	sign_offset += sizeof(uint32_t);
	for (int i = 0; i < evm_result->rset_pos; i++) {
		// key_len
        uint32_to_little_endian(evm_result->rsets[i].key_len, sign_offset, 0);
        sign_offset += sizeof(uint32_t);
        // key
        memcpy(sign_offset, evm_result->rsets[i].key, evm_result->rsets[i].key_len);
		sign_offset += evm_result->rsets[i].key_len;

		// value_len
        uint32_to_little_endian(evm_result->rsets[i].value_len, sign_offset, 0);
        sign_offset += sizeof(uint32_t);
        // value
		memcpy(sign_offset, evm_result->rsets[i].value, evm_result->rsets[i].value_len);
		sign_offset += evm_result->rsets[i].value_len;

		// version_len
        uint32_to_little_endian(evm_result->rsets[i].version_len, sign_offset, 0);
        sign_offset += sizeof(uint32_t);
        // version
        memcpy(sign_offset, evm_result->rsets[i].version, evm_result->rsets[i].version_len);
        sign_offset += evm_result->rsets[i].version_len;
	}
	// wsets
	uint32_to_little_endian(evm_result->wset_pos, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
	for (int i = 0; i < evm_result->wset_pos; i++){
	    // key_len
        uint32_to_little_endian(evm_result->wsets[i].key_len, sign_offset, 0);
        sign_offset += sizeof(uint32_t);
        // key
		memcpy(sign_offset, evm_result->wsets[i].key, evm_result->wsets[i].key_len);
		sign_offset += evm_result->wsets[i].key_len;

		// value_len
        uint32_to_little_endian(evm_result->wsets[i].value_len, sign_offset, 0);
        sign_offset += sizeof(uint32_t);
        // value
		memcpy(sign_offset, evm_result->wsets[i].value, evm_result->wsets[i].value_len);
		sign_offset += evm_result->wsets[i].value_len;
	}
	// contract name_len
    uint32_to_little_endian(evm_result->name_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // name
	memcpy(sign_offset, evm_result->contract_name, evm_result->name_len);
	sign_offset += evm_result->name_len;
	// version_len
    uint32_to_little_endian(evm_result->version_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
	// code_version
	memcpy(sign_offset, evm_result->code_version, evm_result->version_len);
	sign_offset += evm_result->version_len;
    // hash_len
    uint32_to_little_endian(evm_result->code_hash_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // code_hash
	memcpy(sign_offset, evm_result->code_hash, evm_result->code_hash_len);
	sign_offset += evm_result->code_hash_len;
    // report_hash_len
    uint32_to_little_endian(evm_result->report_hash_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // report_hash
    memcpy(sign_offset, evm_result->report_hash, evm_result->report_hash_len);
	sign_offset += evm_result->report_hash_len;

    // private_req_len
    uint32_to_little_endian(evm_result->private_req_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // private_req
    memcpy(sign_offset, evm_result->private_req, evm_result->private_req_len);
	sign_offset += evm_result->private_req_len;
	// code_header_len
    uint32_to_little_endian(evm_result->code_header_len, sign_offset, 0);
    sign_offset += sizeof(uint32_t);
    // code_header
    memcpy(sign_offset, evm_result->code_header, evm_result->code_header_len);
    sign_offset += evm_result->code_header_len;

	enclave_printf("original rlp data: %s, sign bytes len: %d, start pointer: %p, end pointer: %p, real len: %d",
	    hex_rlp_data, serial_evm_result_len, serial_evm_result, sign_offset, sign_offset - serial_evm_result);

    unsigned char hex_serial_evm_result[serial_evm_result_len * 2 + 1];
    for(int i=0;i<(serial_evm_result_len * 2 + 1);i++) {
        hex_serial_evm_result[i] = 0;
    }
    encode_hex((unsigned char *)serial_evm_result, serial_evm_result_len, hex_serial_evm_result);
	enclave_printf("original rlp data: %s, serial_evm_result: %s, len: %d",
	    hex_rlp_data, hex_serial_evm_result, serial_evm_result_len);

	unsigned char signed_evm_result[MAX_SIG_LENGTH];
	size_t signed_evm_result_len = MAX_SIG_LENGTH;
	ret =  tee_cert_sign(serial_evm_result, serial_evm_result_len, signed_evm_result, &signed_evm_result_len);
	enclave_printf("original rlp data: %s, tee cert sign ret: %d", hex_rlp_data, ret);
	if (ret != 0) {
		return SGX_ERROR_UNEXPECTED;
	}

    unsigned char hex_signed_evm_result[signed_evm_result_len * 2 + 1];
    for(int i=0;i<(signed_evm_result_len * 2 + 1);i++) {
        hex_signed_evm_result[i] = 0;
    }
    encode_hex((unsigned char *)signed_evm_result, signed_evm_result_len, hex_signed_evm_result);
	enclave_printf("original rlp data: %s, signature: %s, len: %d",
	    hex_rlp_data, hex_signed_evm_result, signed_evm_result_len);

	if (signed_evm_result_len > MAX_SIG_LENGTH) {
	    enclave_printf("original rlp data: %s, sig len: %d is too long, max len: %d",
	        hex_rlp_data, signed_evm_result_len, MAX_SIG_LENGTH);
	    return SGX_ERROR_UNEXPECTED;
	}
	memcpy(evm_result->sig, signed_evm_result, signed_evm_result_len);
	evm_result->sig_len = signed_evm_result_len;

    return SGX_SUCCESS;
}

int print_elem(void *const context, struct hashmap_element_s *const p_elem) {
    unsigned char hex_data[32 * 2 + 1] = {0};
    encode_hex((unsigned char *)p_elem->data, 32, hex_data);
    enclave_printf("%s => %s", p_elem->key, hex_data);
    return 0;
}

void rsa_key_gen()
{
    enclave_printf("test rsa_key_gen completed");
}

void t_sgxssl_call_apis(){
    rsa_key_gen();
    enclave_printf("test rsa_key_gen completed");
}

char* sign_data(char *data, uint32_t data_len){
    return data;
}

