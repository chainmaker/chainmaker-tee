#include <math.h>
#include <stdio.h>
#include "enclave_log.h"
#ifdef EVM_ALONE
#include "enclave_ocall.h"
#endif
#include "evm.h"
#include "keccak256.h"

static struct GAS_price GAS_TABLE = {
        .stepGas0 = 0,
        .stepGas1 = 1,
        .stepGas2 = 2,
        .stepGas3 = 3,
        .stepGas5 = 5,
        .stepGas8 = 8,
        .stepGas10 = 10,
        .sha3Gas = 30,
        .sha3WordGas = 6,
        .sloadGas = 50,
        .sstoreSetGas = 20000,
        .sstoreResetGas = 5000,
        .sstoreUnchangedGas = 200,
        .jumpdestGas = 1,
        .logGas = 375,
        .logDataGas = 8,
        .logTopicGas = 375,
        .createGas = 32000,
        .memoryGas = 3,
        .quadCoeffDiv = 512,
        .copyGas = 3,
        .valueTransferGas = 9000,
        .callStipend = 2300,
        .callNewAccount = 25000,
};

void init_machine(Machine *state, struct hashmap_s *read_set, struct hashmap_s *write_set) {
    state->PC = 0;
    state->SP = 0;
    state->GAS_Charge = 0;
    memset(state->MEM, 0, MEMORY_SPACE);
    memset(state->STACK, 0, STACK_SPACE * sizeof(uint256_t));
    state->result_data_size = 0;
    state->read_set = read_set;
    state->write_set = write_set;
}

int max_mem_offset = 0;
int max_storage_counter = 0;
int max_sp = 0;

int32_t execute_contract(Machine *machine_state, char *const contract_name, const uint8_t *s_contract, uint32_t size) {
    //Execute smart contract till end of bytecode / exit or error 
    while (machine_state->PC < size - 1) {
        // GAS can be emmited for off-chain
        if (machine_state->GAS_Charge > GAS_LIMIT) {
            //printf("Run out of GAS!");
            break;
        }
        //decode the next instruction
        int status = decode_instruction(machine_state, contract_name, s_contract[machine_state->PC], s_contract);
        //check for stack pointer
        if (machine_state->SP > max_sp) {
            max_sp = machine_state->SP;
        }
        if (status == RETURN) {
            enclave_printf("return!");
            break;
        }
        if (status == EVM_ERROR) {
            // End of smart contract execution print stats
            enclave_printf("Stack Pointer :  %u ", max_sp);
            enclave_printf("Stack usage :  %u ", max_sp * 256);
            enclave_printf("Memory usage :  %u ", max_mem_offset);
            enclave_printf("Storage usage  :  %u ", max_storage_counter * 256);
            return EVM_ERROR;
        }
        machine_state->PC++;
        if (s_contract[machine_state->PC] == STOP) {
            break;
        }
    }
    return EVM_OK;
}

//each word-machine parse through the decode state
int decode_instruction(Machine *machine_state, char *const contract_name, uint8_t op_code_exc, const uint8_t *s_contract) {
    switch (op_code_exc) {
        //****<< ORIGINAL OPCODES >>****
        case STOP: { // STOP
            enclave_printf("STOP..");
            enclave_printf("Stack output:  %llu ", *stack_peek(machine_state));
            enclave_printf("GAS spend:  %lu ", machine_state->GAS_Charge);
            shutdown_machine(machine_state);
        }
        case PUSH1:
        case PUSH2:
        case PUSH3:
        case PUSH4:
        case PUSH5:
        case PUSH6:
        case PUSH7:
        case PUSH8: { // Push x bits into the stack
            enclave_printf("PC:%li - PUSH     ", machine_state->PC);
            if (op_code_exc < 0) {
                //printf("malformed op_code: {PUSH}");
                break;
            }
            int numberOfBytes = (int) op_code_exc - (int) PUSH1 + 1;

            if (machine_state->SP + numberOfBytes >= STACK_SPACE) {
                stack_overflow_err();
                return EVM_ERROR;
            }

            machine_state->SP++;

            uint64_t element_upp_upp = 0;
            uint64_t element_upp_low = 0;
            uint64_t element_low_upp = 0;
            uint64_t element_low_low = 0;

            int i;
            for (i = 0; i < numberOfBytes; i++) {
                machine_state->PC++;
                element_low_low = (element_low_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }

            LOWER(LOWER(machine_state->STACK[machine_state->SP])) = element_low_low;
            LOWER(UPPER(machine_state->STACK[machine_state->SP])) = element_low_upp;
            UPPER(LOWER(machine_state->STACK[machine_state->SP])) = element_upp_low;
            UPPER(UPPER(machine_state->STACK[machine_state->SP])) = element_upp_upp;

            uint8_t buf[128];
            snprint256(&machine_state->STACK[machine_state->SP], buf);
            enclave_printf("    0x%s.", buf);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            break;
        }
        case PUSH9:
        case PUSH10:
        case PUSH11:
        case PUSH12:
        case PUSH13:
        case PUSH14:
        case PUSH15:
        case PUSH16: {
            enclave_printf("PC:%li - PUSH ", machine_state->PC);
            if (op_code_exc < 0) {
                enclave_printf("malformed op_code: {PUSH}");
                break;
            }
            int push_code = (int) op_code_exc - (int) PUSH1;

            int numberOfBytes = push_code + 1;

            if (machine_state->SP + numberOfBytes >= STACK_SPACE) {
                stack_overflow_err();
                return EVM_ERROR;
            }

            machine_state->SP++;

            uint64_t element_upp_upp = 0;
            uint64_t element_upp_low = 0;
            uint64_t element_low_upp = 0;
            uint64_t element_low_low = 0;


            int size_low_up = push_code - 8;
            int i;
            // //printf("size_low_up = %d\n",  size_low_up); 
            for (i = 0; i <= size_low_up; i++) {
                machine_state->PC++;
                element_low_upp = (element_low_upp << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_low_low = (element_low_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }

            LOWER(LOWER(machine_state->STACK[machine_state->SP])) = element_low_low;
            LOWER(UPPER(machine_state->STACK[machine_state->SP])) = element_low_upp;
            UPPER(LOWER(machine_state->STACK[machine_state->SP])) = element_upp_low;
            UPPER(UPPER(machine_state->STACK[machine_state->SP])) = element_upp_upp;
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            break;
        }
        case PUSH17:
        case PUSH18:
        case PUSH19:
        case PUSH20:
        case PUSH21:
        case PUSH22:
        case PUSH23:
        case PUSH24: {
            enclave_printf("PC:%li - PUSH ", machine_state->PC);
            if (op_code_exc < 0) {
                //printf("malformed op_code: {PUSH}\n");
                break;
            }
            int push_code = (int) op_code_exc - (int) PUSH1;

            int numberOfBytes = push_code + 1;

            if (machine_state->SP + numberOfBytes >= STACK_SPACE) {
                stack_overflow_err();
                return EVM_ERROR;
            }

            machine_state->SP++;

            uint64_t element_upp_upp = 0;
            uint64_t element_upp_low = 0;
            uint64_t element_low_upp = 0;
            uint64_t element_low_low = 0;


            int size_low_up = push_code - 16;
            int i;
            // //printf("size_low_up = %d\n",  size_low_up); 
            for (i = 0; i <= size_low_up; i++) {
                machine_state->PC++;
                element_upp_low = (element_upp_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_low_upp = (element_low_upp << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_low_low = (element_low_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }

            LOWER(LOWER(machine_state->STACK[machine_state->SP])) = element_low_low;
            LOWER(UPPER(machine_state->STACK[machine_state->SP])) = element_low_upp;
            UPPER(LOWER(machine_state->STACK[machine_state->SP])) = element_upp_low;
            UPPER(UPPER(machine_state->STACK[machine_state->SP])) = element_upp_upp;

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            break;
        }
        case PUSH25:
        case PUSH26:
        case PUSH27:
        case PUSH28:
        case PUSH29:
        case PUSH30:
        case PUSH31:
        case PUSH32: {
            enclave_printf("PC:%li - PUSH ", machine_state->PC);
            if (op_code_exc < 0) {
                enclave_printf("malformed op_code: {PUSH}");
                break;
            }
            int push_code = (int) op_code_exc - (int) PUSH1;

            int numberOfBytes = push_code + 1;

            if (machine_state->SP + numberOfBytes >= STACK_SPACE) {
                stack_overflow_err();
                return EVM_ERROR;
            }
            machine_state->SP++;

            uint64_t element_upp_upp = 0;
            uint64_t element_upp_low = 0;
            uint64_t element_low_upp = 0;
            uint64_t element_low_low = 0;


            int size_low_up = push_code - 24;
            int i;
            for (i = 0; i <= size_low_up; i++) {
                machine_state->PC++;
                element_upp_upp = (element_upp_upp << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_upp_low = (element_upp_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_low_upp = (element_low_upp << 8) | (uint64_t) s_contract[machine_state->PC];
            }
            for (i = 0; i < 8; i++) {
                machine_state->PC++;
                element_low_low = (element_low_low << 8) | (uint64_t) s_contract[machine_state->PC];
            }

            LOWER(LOWER(machine_state->STACK[machine_state->SP])) = element_low_low;
            LOWER(UPPER(machine_state->STACK[machine_state->SP])) = element_low_upp;
            UPPER(LOWER(machine_state->STACK[machine_state->SP])) = element_upp_low;
            UPPER(UPPER(machine_state->STACK[machine_state->SP])) = element_upp_upp;

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;

            break;
        }
        case ADD: { // Add top two values of the stack
            enclave_printf("PC:%li - ADD ", machine_state->PC);
            uint256_t target = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);

            add256(&number_1, &number_2, &target);
            stack_push(machine_state, target);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case MUL: { // Multiply top two values of the stack
            enclave_printf("PC:%li - MUL ", machine_state->PC);
            uint256_t target = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);

            mul256(&number_1, &number_2, &target);
            stack_push(machine_state, target);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            break;
        }
        case SUB: { // Subtract top two values of the stack
            enclave_printf("PC:%li - SUB \n", machine_state->PC);
            uint256_t target = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);

            minus256(&number_1, &number_2, &target);
            stack_push(machine_state, target);
            break;
        }
        case DIV: { // Divide (unsign) top two values of the stack
            enclave_printf("PC:%li - DIV \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            uint256_t target = {0}, target_mod = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);
            if (zero256(&number_2)) {
                enclave_printf("divide by zero\n");
                return EVM_ERROR;
            }
            divmod256(&number_1, &number_2, &target, &target_mod);
            stack_push(machine_state, target);
            break;
        }
        case SDIV: { // SDIV top two values of the stack
            enclave_printf("PC:%li - SDIV \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            uint256_t target = {0};
            uint256_t modulo = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);
            if (zero256(&number_2)) {
                enclave_printf("divide by zero\n");
                return EVM_ERROR;
            }
            divmod256(&number_1, &number_2, &target, &modulo);
            stack_push(machine_state, target);
            break;
        }
        case MOD: { // Modulo using top two of the stack
            enclave_printf("PC:%li - MOD \n", machine_state->PC);
            uint256_t target = {0};
            uint256_t modulo = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);

            divmod256(&number_1, &number_2, &target, &modulo);
            stack_push(machine_state, modulo);
            break;
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            break;
        }
        case SMOD: { // SMOD
            enclave_printf("PC:%li - SMOD \n", machine_state->PC);
            uint256_t target = {0};
            uint256_t modulo = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);

            divmod256(&number_1, &number_2, &target, &modulo);
            stack_push(machine_state, modulo);
            break;
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            break;
        }
        case ADDMOD: {    //Add two values and modulo N (take the three values from strack)
            enclave_printf("PC:%li - ADDMOD \n", machine_state->PC);
            uint256_t div = {0};
            uint256_t modulo = {0};
            uint256_t sum = {0};
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);
            uint256_t modulo_N = stack_pop(machine_state);

            add256(&number_1, &number_2, &sum);
            divmod256(&sum, &modulo_N, &div, &modulo);
            stack_push(machine_state, modulo);
            break;
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            break;
        }
        case MULMOD: { //Multiply two values and modulo N (take the three values from strack)
            enclave_printf("PC:%li - MULMOD \n", machine_state->PC);
            uint256_t number_1 = stack_pop(machine_state);
            uint256_t number_2 = stack_pop(machine_state);
            uint256_t modulo_N = stack_pop(machine_state);
            uint256_t mul_res = {0};
            uint256_t modulo = {0};
            uint256_t div = {0};

            mul256(&number_1, &number_2, &mul_res);
            divmod256(&mul_res, &modulo_N, &div, &modulo);
            stack_push(machine_state, modulo);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas8;
            break;
        }
        case EXP: { // Exponentiation modulo top two values of the stack

            enclave_printf("PC:%li - EXP \n", machine_state->PC);
            // very inefficient way to implement this
            uint256_t base = stack_pop(machine_state);
            uint64_t exponent = LOWER(LOWER(stack_pop(machine_state)));
            uint256_t accumulator = {0};
            uint256_t result = {0};
            uint64_t i;
            if (exponent == 0) {
                LOWER(LOWER(result)) = 0x01;
            } else {
                for (i = 0; i < exponent; i++) {
                    mul256(&base, &accumulator, &result);
                    accumulator = result;
                }
            }
            stack_push(machine_state, result);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas10;
            break;
        }
        case SIGNEXTEND: { // Sign and extends using top two ...
//            enclave_printf("SIGNEXTEND not supported\n");
            enclave_printf("PC:%li - SIGNEXTEND \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas5;
            uint64_t bytesNum = LOWER(LOWER(stack_pop(machine_state)));
            uint256_t *value = stack_peek(machine_state);
            sign_extend(value, bytesNum);
            break;
        }

        case LT: { // Less Than comparison top two ...
            enclave_printf("PC:%li - LT \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool bot_greater = gt256(&bot, &top);
            if (bot_greater) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case GT: { // Greater than comparion top two ...
            enclave_printf("PC:%li - GT \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool top_greater = gt256(&top, &bot);
            if (top_greater) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SLT: { // Less Than comparison treat values by 2 compliment (note: in C values are 2complement by default)
            enclave_printf("PC:%li - SLT \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool bot_greater = gt256(&bot, &top);
            if (bot_greater) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SGT: { //Greater Than comparison treat values by 2 compliment (note: in C values are 2complement by default)
            enclave_printf("PC:%li - SGT \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool top_greater = gt256(&top, &bot);
            if (top_greater) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case EQ: { // Equal comparison
            enclave_printf("PC:%li - EQ \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool TopBotEquals = equal256(&top, &bot);
            if (TopBotEquals) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case ISZERO: { // Test if top is zero
            enclave_printf("PC:%li - ISZERO \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t zeroORone = {0};

            bool TopZero = zero256(&top);
            if (TopZero) {
                LOWER(LOWER(zeroORone)) = 0x01;
                stack_push(machine_state, zeroORone);
            } else {
                stack_push(machine_state, zeroORone);
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case AND: { // AND on top two values
            enclave_printf("PC:%li - AND \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t andRes = {0};
            and256(&top, &bot, &andRes);

            stack_push(machine_state, andRes);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case OR: { // OR on top two values
            enclave_printf("PC:%li - OR \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t orRes = {0};
            or256(&top, &bot, &orRes);

            stack_push(machine_state, orRes);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case XOR: { // XOR on top two values
            enclave_printf("PC:%li - XOR \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            uint256_t bot = stack_pop(machine_state);
            uint256_t xorRes = {0};
            xor256(&top, &bot, &xorRes);


            stack_push(machine_state, xorRes);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case NOT: { // NOT on top value
            enclave_printf("PC:%li - NOT \n", machine_state->PC);
            uint256_t top = stack_pop(machine_state);
            not256(&top);
            stack_push(machine_state, top);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case BYTE: { // BYTE on top two values
            enclave_printf("PC:%li - BYTE \n", machine_state->PC);
            uint256_t i = stack_pop(machine_state);
            uint256_t x = stack_pop(machine_state);


            uint32_t shift_value = (uint32_t) LOWER(LOWER(i));
            shift_value = 248 - shift_value * 8;
            uint256_t target = {0};
            shiftr256(&x, shift_value, &target);
            uint256_t tmp0ff = {0};
            LOWER(LOWER(tmp0ff)) = 0xFF;
            uint256_t y = {0};
            and256(&target, &tmp0ff, &y);
            stack_push(machine_state, y);

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SHL: { //shift left
            enclave_printf("PC:%li - SHL \n", machine_state->PC);
            uint256_t shift = stack_pop(machine_state);
            uint256_t value = stack_pop(machine_state);
            uint32_t shift_v = (uint32_t) LOWER(LOWER(shift));
            uint256_t target = {0};
            shiftl256(&value, shift_v, &target);

            stack_push(machine_state, target);

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SHR: { // shift right
            enclave_printf("PC:%li - SHR \n", machine_state->PC);
            uint256_t shift = stack_pop(machine_state);
            uint256_t value = stack_pop(machine_state);


            uint32_t shift_v = (uint32_t) LOWER(LOWER(shift));

            uint256_t target = {0};
            shiftr256(&value, shift_v, &target);

            stack_push(machine_state, target);

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SAR: { // shift int right
            enclave_printf("PC:%li - SAR \n", machine_state->PC);
            uint256_t shift = stack_pop(machine_state);
            uint256_t value = stack_pop(machine_state);


            uint32_t shift_v = (uint32_t) LOWER(LOWER(shift));

            uint256_t target = {0};
            shiftr256(&value, shift_v, &target);

            stack_push(machine_state, target);

            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            break;
        }
        case SHA3: {
            enclave_printf("PC:%li - SHA3 \n", machine_state->PC);
            uint256_t Offset_256 = stack_pop(machine_state);
            uint256_t Length_256 = stack_pop(machine_state);

            uint64_t offset = LOWER(LOWER(Offset_256));
            uint64_t length = LOWER(LOWER(Length_256));
            if (length > 256) {
                return EVM_ERROR;
            }

            // copy data from memory
            uint8_t data[length];
            if (offset < 0 || offset > MEMORY_SPACE) {
                enclave_printf("MEM Offeset: 0x%llX is invalid\n", offset);
                return EVM_ERROR;
            }

            memcpy(&data, &machine_state->MEM[offset], sizeof(uint8_t) * length);
            enclave_printf("    uint8 from memory => ");
            for(int i = 0; i < length; i++) {
                enclave_printf("%02X", data[i]);
            }
            enclave_printf("\n");

            // calculate the hash of data
            uint8_t result[32];
            get_keccak256(data, sizeof(uint8_t) * length, result);

            enclave_printf("    keccak256 data => ");
            for(int i = 0; i < 32; i++) {
                enclave_printf("%02X", result[i]);
            }
            enclave_printf("\n");

            // turn hash bytes into uint256
            uint256_t hashTOpush = {0};
            bytes_to_uint256(&result, &hashTOpush);

            // push uint256 into stack
            stack_push(machine_state, hashTOpush);

            break;
        }

        case ADDRESS: {
            enclave_printf("PC:%li - ADDRESS \n", machine_state->PC);
            stack_push(machine_state, machine_state->message.address);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            break;
        }
        case CALLER: {
            enclave_printf("PC:%li - CALLER \n", machine_state->PC);
            stack_push(machine_state, machine_state->message.caller);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;

            break;
        }
        case CALLVALUE: {
            enclave_printf("PC:%li - CALLVALUE \n", machine_state->PC);
            stack_push(machine_state, machine_state->message.call_value);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;

            break;
        }
        case CALLDATALOAD: {
            enclave_printf("PC:%li - CALLDATALOAD \n", machine_state->PC);
            uint32_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint256_t dataFROMmessage = {0};

            if (offset < 0 || offset > MESSAGEDATASIZE) {
                enclave_printf("DATA Offeset: 0x%lX is invalid\n", offset);
                return EVM_ERROR;
            }
            if (offset == 0) {
                // enclave_printf("fucnction nname %llX",swapLong (&function_name));
                bytes_to_uint256(&machine_state->message.data[offset], &dataFROMmessage);
                stack_push(machine_state, dataFROMmessage);
            } else {
                bytes_to_uint256(&machine_state->message.data[offset], &dataFROMmessage);
                stack_push(machine_state, dataFROMmessage);
            }
            break;

        }
        case CALLDATASIZE: {
            enclave_printf("PC:%li - CALLDATASIZE \n", machine_state->PC);
            // enclave_printf("CALLDATASIZE: data size is limited \n");
            uint256_t sizeofdata = {0};
            LOWER(LOWER(sizeofdata)) = machine_state->message.datasize;

            stack_push(machine_state, sizeofdata);
            break;
        }
        case CALLDATACOPY: {
            enclave_printf("PC:%li - CALLDATACOPY \n", machine_state->PC);
            // enclave_printf("CALLDATACOPY: CARE ABOUT MEM SIZE AND BYTEORDER \n");

            uint64_t destOffset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t length = LOWER(LOWER(stack_pop(machine_state)));

            if ((destOffset + length) < 0 || (destOffset + length) > MEMORY_SPACE) {
                enclave_printf("CALLDATACOPY: length(%llu)+ offdet(%llu) out of memory bound\n", length, destOffset);
            } else if ((offset + length) < 0 || (offset + length) > MESSAGEDATASIZE) {
                enclave_printf("CALLDATACOPY: length(%llu)+ msg.data [(%llu)] out of memory bound\n", length, offset);
            } else {
                memcpy(&machine_state->MEM[destOffset], &machine_state->message.data[offset], sizeof(uint8_t) * length);
                if (destOffset + length > max_mem_offset) {
                    max_mem_offset = destOffset + length;
                }
            }
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;

            break;
        }

        case RETURN: {
            enclave_printf("PC:%li - RETURN \n", machine_state->PC);
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t length = LOWER(LOWER(stack_pop(machine_state)));
            if ((offset + length) < 0 || (offset + length) > MEMORY_SPACE) {
                enclave_printf("MEM: length(%llu) with offdet(%llX) out of memory bound\n", length, offset);
                length = MEMORY_SPACE - 1;
                offset = 0;
            }

            machine_state->result_data_size = sizeof(uint8_t) * length;
            machine_state->result_data_ptr = calloc(length, sizeof(uint8_t));
            if (machine_state->result_data_ptr == NULL) {
                enclave_printf("malloc result data memory failed");
                return EVM_ERROR;
            }
            memcpy(machine_state->result_data_ptr, &machine_state->MEM[offset], sizeof(uint8_t) * length);
            return RETURN;
        }

        case BALANCE: {
            enclave_printf("PC:%li - BALANCE \n", machine_state->PC);
            break;
        }
        case CODECOPY: {
            enclave_printf("PC:%li - CODECOPY \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            uint64_t MEMOffset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t Offset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t length = LOWER(LOWER(stack_pop(machine_state)));

            enclave_printf("    CODECOPY:  MEMOffset 0x%llX ,Offset 0x%llX , length %llu  \n", MEMOffset, Offset, length);
            if ((MEMOffset) < 0 || (MEMOffset) > MEMORY_SPACE) {
                enclave_printf("CODECOPY: length(%llu)+ offdet(%llXF) out of memory bound, MEMORY_SPACE:%llu\n",
                    length, MEMOffset, MEMORY_SPACE);
                return EVM_ERROR;
            }
            memcpy(&machine_state->MEM[MEMOffset], &s_contract[Offset], length);
            break;
        }
        case CODESIZE: {
            enclave_printf("PC:%li - CODESIZE \n", machine_state->PC);
            uint256_t code_sz = {0};
            LOWER(LOWER(code_sz)) = machine_state->message.codesize;
            stack_push(machine_state, code_sz);
            break;
        }

        case POP: {
            enclave_printf("PC:%li - POP \n", machine_state->PC);
            // enclave_printf("[DEBUG]POP Opcode: discard the first element from the stack\n");
            stack_pop(machine_state);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas2;
            break;
        }


        case MLOAD: {
            enclave_printf("PC:%li - MLOAD \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            // enclave_printf("MLOAD 0x%llX\n", offset);
            if (offset < 0 || offset > MEMORY_SPACE) {
                enclave_printf("MEM Offeset: 0x%llX is invalid\n" , offset);
                return EVM_ERROR;
            }
            uint8_t data[32] = {0};
            memcpy(&data, &machine_state->MEM[offset], sizeof(uint8_t) * 32);

            uint256_t value = {0};
            bytes_to_uint256(&data, &value);

            uint8_t buf[128];
            enclave_printf("    load uint256 from MEM[%llu] => ", offset);
            snprint256(&value, buf);
            enclave_printf("0x%s \n", buf);
            stack_push(machine_state, value);
            break;
        }

        case MSTORE: { // Store at the memory using as offest and word top two values of the stack
            enclave_printf("PC:%li - MSTORE \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            // enclave_printf("MSTORE opcode\n");
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint256_t word = stack_pop(machine_state);

            if (offset < 0 || offset > MEMORY_SPACE) {
                enclave_printf("MEM Offeset: 0x%llX is invalid\n", offset);
                return EVM_ERROR;
            }

            enclave_printf("    put uint256 into MEM[%llu] <= ", offset);
            uint8_t buf[128];
            snprint256(&word, buf);
            enclave_printf("0x%s.\n", buf);

            uint8_t data[32] = {0};
            uint256_to_bytes(&word, data);
            memcpy(&machine_state->MEM[offset], data, 32);
            if (sizeof(word) > max_mem_offset) {
                max_mem_offset = offset + sizeof(word);
            }
            break;
        }
        case MSTORE8: {
            enclave_printf("PC:%li - MSTORE8 \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint8_t word = (uint8_t) LOWER(LOWER(stack_pop(machine_state)));
            if (offset < 0 || offset > MEMORY_SPACE) {
                 enclave_printf("MEM Offeset: 0x%llX is invalid\n", offset);
                 return EVM_ERROR;
            }
            machine_state->MEM[offset] = word & 0xFF;
            break;
        }
        case SSTORE: {
            enclave_printf("PC:%li - SSTORE \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.sstoreSetGas;
            uint256_t key = stack_pop(machine_state);
            uint256_t value = stack_pop(machine_state);
            // enclave_printf("STORAGE key: 0x%llX \n" , key);
            max_storage_counter++;

            int save_ret = save_to_chain(machine_state, contract_name, strlen(contract_name), key, &value);
            if (save_ret != 0) {
                enclave_printf("save to chain failed, code: %d", save_ret);
                return EVM_ERROR;
            }
            break;
        }
        case SLOAD: {
            enclave_printf("PC:%li - SLOAD \n", machine_state->PC);
            uint256_t key = stack_pop(machine_state);
            uint256_t value = {0};
            int load_ret = load_from_chain(machine_state, contract_name, strlen(contract_name), key, &value);
            if (load_ret != 0) {
                enclave_printf("load from chain failed, code: %d", load_ret);
                return EVM_ERROR;
            }
            stack_push(machine_state, value);
            break;
        }
        case JUMPI: {
            enclave_printf("PC:%li - JUMPI \n", machine_state->PC);
            // enclave_printf("JUMPI:\n");
            // enclave_printf("Currnet PC:  %lu\n",machine_state->PC  );
            uint64_t destination = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t cond = LOWER(LOWER(stack_pop(machine_state)));
            machine_state->PC = cond ? destination : machine_state->PC;

            // enclave_printf("Jump PC:  %lu\n",machine_state->PC  );
            break;
        }
        case JUMPDEST: {
            enclave_printf("PC:%li - JUMPDEST \n", machine_state->PC);
            // enclave_printf("JUMP label Nothing to do...\n");
            break;
        }
        case JUMP: {
            enclave_printf("PC:%li - JUMP \n", machine_state->PC);
            uint64_t destination = LOWER(LOWER(stack_pop(machine_state)));
            // enclave_printf("jump (%llu) destination\n", destination);
            if (destination < 0) {
                enclave_printf("jump (%llu) destination out of bound", destination);
            } else {
                machine_state->PC = destination;
            }
            break;
        }

        case DUP1:
        case DUP2:
        case DUP3:
        case DUP4:
        case DUP5:
        case DUP6:
        case DUP7:
        case DUP8:
        case DUP9:
        case DUP10:
        case DUP11:
        case DUP12:
        case DUP13:
        case DUP14:
        case DUP15:
        case DUP16: {

            enclave_printf("PC:%li - DUP \n", machine_state->PC);
            uint64_t offset = (uint64_t) op_code_exc - (uint64_t) DUP1;
            // enclave_printf("DUP: to clone value at:%llu \n",offset);
            int index_to_clone = machine_state->SP - offset;
            // enclave_printf("DUP Index: %d , SP:%d \n",index_to_clone,  machine_state->SP);

            if (index_to_clone < 0) {
                enclave_printf("DUP Index non valid : %d , SP:%d \n",index_to_clone,  machine_state->SP);
            } else {
                uint256_t to_clone = machine_state->STACK[index_to_clone];
                stack_push(machine_state, to_clone);
            }
            break;
        }

        case SWAP1:
        case SWAP2:
        case SWAP3:
        case SWAP4:
        case SWAP5:
        case SWAP6:
        case SWAP7:
        case SWAP8:
        case SWAP9:
        case SWAP10:
        case SWAP11:
        case SWAP12:
        case SWAP13:
        case SWAP14:
        case SWAP15:
        case SWAP16: {
            enclave_printf("PC:%li - SWAP \n", machine_state->PC);
            int SwapOffest = machine_state->SP - ((uint64_t) op_code_exc - (uint64_t) SWAP1 + 1);

            // enclave_printf("[DEBUG] SWAP with offset(%d)\n",SwapOffest);
            // enclave_printf("SP : %d \n",machine_state->SP);
            // enclave_printf("SwapOffest %016llX\n", LOWER(LOWER(machine_state->STACK[SwapOffest] )));
            if (SwapOffest < 0 || SwapOffest > STACK_SPACE) {
                enclave_printf("SWAP: offset(%d) is invalid\n", SwapOffest);
            } else {
                // enclave_printf("SWAP\n");
                uint256_t temp_store = machine_state->STACK[machine_state->SP];
                machine_state->STACK[machine_state->SP] = machine_state->STACK[SwapOffest];
                machine_state->STACK[SwapOffest] = temp_store;
            }

            break;
        }

        case LOG0:
        case LOG1:
        case LOG2:
        case LOG3:
        case LOG4: {

            enclave_printf("PC:%li - LOG \n", machine_state->PC);
            //enclave_printf("LOG unsupported \n");
            break;
        }

        case TIMESTAMP: {
            enclave_printf("PC:%li - TIMESTAMP \n", machine_state->PC);
            uint256_t timestamp = {0};
            stack_push(machine_state, timestamp);
            //enclave_printf("Unused opcode\n");
            break;
        }
        case REVERT: {
            enclave_printf("PC:%li - REVERT \n", machine_state->PC);
            machine_state->GAS_Charge = machine_state->GAS_Charge + GAS_TABLE.stepGas3;
            uint64_t offset = LOWER(LOWER(stack_pop(machine_state)));
            uint64_t length = LOWER(LOWER(stack_pop(machine_state)));

            if (offset < 0 || offset > MEMORY_SPACE) {
                enclave_printf("MEM Offeset: %llX is invalid\n", offset);
                return EVM_ERROR;
            }

            machine_state->result_data_size = sizeof(uint8_t) * length;
            machine_state->result_data_ptr = calloc(length, sizeof(uint8_t));
            if (machine_state->result_data_ptr == NULL) {
                enclave_printf("malloc result data memory failed when executing revert instruction");
                return EVM_ERROR;
            }
            memcpy(machine_state->result_data_ptr, &machine_state->MEM[offset], sizeof(uint8_t) * length);
            return RETURN;
        }
        case BLOCKHASH : {
            enclave_printf("PC:%li - BLOCKHASH \n", machine_state->PC);
            uint256_t block = {0};
            stack_push(machine_state, block);
            break;
        }
        case INVALID: {
            enclave_printf("PC:%li - INVALID \n", machine_state->PC);
            return EVM_ERROR;
        }
        default: {
            enclave_printf("Unsupported OP_CODE: %X\n", op_code_exc);
            return EVM_ERROR;
        }
    }
    return EVM_OK;
}

void shutdown_machine(Machine *machine_state) {
    machine_state->PC = 0;
    machine_state->SP = 0;
    machine_state->GAS_Charge = 0;
    free(machine_state->result_data_ptr);
}

// Stack ops
void stack_push(Machine *machine_state, uint256_t item) {
    if (machine_state->SP >= STACK_SPACE) {
        stack_overflow_err();
    } else {
        machine_state->SP++;
        machine_state->STACK[machine_state->SP] = item;
    }

}

uint256_t stack_pop(Machine *machine_state) {
    uint256_t tmp = machine_state->STACK[machine_state->SP];
    machine_state->SP--;
    return tmp;
}

uint256_t *stack_peek(Machine *machine_state) {
    return &machine_state->STACK[machine_state->SP];
}

void stack_duplicate(Machine *machine_state) {
    if (machine_state->SP == -1)
        empty_stack_err("stack_duplicate");
    stack_push(machine_state, machine_state->STACK[machine_state->SP]);
}

void stack_print(Machine *machine_state) {
    int i;
    //printf("[top]\n");
    uint256_t *strucPtr;


    unsigned char *charPtr;
    for (i = machine_state->SP; i >= 0; i--) {
        strucPtr = &machine_state->STACK[i];
        charPtr = (unsigned char *) strucPtr;
        for (i = 0; i < sizeof(uint256_t); i++) {
            //printf("%02x", charPtr[i]);
        }
        //printf("\n");
    }
}

// Errors
void stack_overflow_err() {
    //printf("[!!!] Fatal error: stack overflow.\n");
    // exit(-1);
}

void empty_stack_err(char *name) {
    //printf("[!!!] Fatal error: %s from empty stack.\n", name);
    // exit(-1);
}

void size_err(char *name) {
    //printf("[!!!] Fatal error: %s called with insufficient sized stack.\n", name);
    // exit(-1);
}

void get_keccak256(const uint8_t *data, uint16_t length, uint8_t *result) {

    SHA3_CTX context;
    keccak_init(&context);
    keccak_update(&context, (const unsigned char *) data, (size_t) length);
    keccak_final(&context, (unsigned char *) result);

    // Clear out the contents of what we hashed (in case it was secret)
    memset((char *) &context, 0, sizeof(SHA3_CTX));
}

int load_from_chain(struct machine * machine, char *contract_name, int length, uint256_t key, uint256_t *p_value) {
    // 构建 key
    char *key_str = malloc(65);
    if (key_str == NULL) {
        enclave_printf("malloc keystr failed while loading value from chain");
        return -1;
    }
    int key_str_len = 64;
    snprintf(key_str,     65, "%016lX", UPPER(UPPER(key)));
    snprintf(key_str +16, 65, "%016lX", UPPER(LOWER(key)));
    snprintf(key_str +32, 65, "%016lX", LOWER(UPPER(key)));
    snprintf(key_str +48, 65, "%016lX", LOWER(LOWER(key)));
    enclave_printf("Read from chain：contract_name = %s, key = 0x%s", contract_name, key_str);

    uint8_t buf_val[128];
    // 从写集读取数据
    void *value_in_write_set = hashmap_get(machine->write_set, key_str, key_str_len);
    if (value_in_write_set != HASHMAP_NULL) {
        bytes_to_uint256(value_in_write_set, p_value);
        // 记录读操作
        snprint256(p_value, buf_val);
        enclave_printf("Read from write_set contract_name: %s, key: 0x%s, value: 0x%s", contract_name, key_str,
            buf_val);
        return 0;
    }

    // 从读集读取数据
    void *value_in_read_set = hashmap_get(machine->read_set, key_str, key_str_len);
    if (value_in_read_set != HASHMAP_NULL) {
        bytes_to_uint256(value_in_read_set, p_value);
        // 记录读操作
        snprint256(p_value, buf_val);
        enclave_printf("Read from read_set, contract_name: %s, key: 0x%s, value: 0x%s", contract_name, key_str,
            buf_val);
        return 0;
    }

    // 调用外部函数，直接在链上读取
    uint32_t value_len = 0;
    uint8_t *value_on_chain = calloc(32, sizeof(uint8_t));
    int ret;
    int ocall_ret = ocall_get(&ret, contract_name, key_str, value_on_chain, &value_len);
    if (ocall_ret != 0) {
        enclave_printf("ocall get failed while loading value from chain, ocall ret: %d, value_len: %d", ocall_ret, value_len);
        return -1;
    }
    if (ret != 0) {
        enclave_printf("ocall get failed, ret: %d, value_len: %d", ocall_ret, value_len);
        return -1;
    }

    bytes_to_uint256(value_on_chain, p_value);
    snprint256(p_value, buf_val);
    enclave_printf("Read from chain(write to read_set) contract_name: %s, key: 0x%s, value: 0x%s",
        contract_name, key_str, buf_val);
    // 记录读操作
    hashmap_put(machine->read_set, key_str, key_str_len, value_on_chain, (uint32_t)(sizeof(uint256_t)));
    return 0;
}

int save_to_chain(struct machine * machine, char *contract_name, int length, uint256_t key, uint256_t *p_value) {
    uint8_t buf_key[128], buf_val[128];
    snprint256(&key, buf_key);
    snprint256(p_value, buf_val);
    enclave_printf("Write to chain(write to write_set)：contract_name = %s, key = 0x%s，value = 0x%s",
        contract_name, buf_key, buf_val);

    // 构建 key
    char *key_str = malloc(65);
    if (key_str == NULL) {
        enclave_printf("calloc key str memory failed while saving value to chain");
        return -1;
    }
    snprintf(key_str,     65, "%016lX", UPPER(UPPER(key)));
    snprintf(key_str +16, 65, "%016lX", UPPER(LOWER(key)));
    snprintf(key_str +32, 65, "%016lX", LOWER(UPPER(key)));
    snprintf(key_str +48, 65, "%016lX", LOWER(LOWER(key)));

    // 写入写集合
    uint256_t *p_value_copy = calloc(1, sizeof(uint256_t));
    if (p_value_copy == NULL) {
        enclave_printf("calloc value copy memory failed while saving value to chain");
        return -1;
    }
    uint256_to_bytes(p_value, (uint8_t *)p_value_copy);
    hashmap_put(machine->write_set, key_str, 64, p_value_copy, (uint32_t)sizeof(uint256_t));
    return 0;
}
