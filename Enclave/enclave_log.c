/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

#include <stdarg.h>
#include <stdlib.h>
#include "enclave_log.h"
#include <string.h>

int enclave_printf(const char* fmt, ...)
{
#ifdef EVM_TRACE
    char buf[LOG_BUFSIZE + 1] = { '\0' };
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, LOG_BUFSIZE, fmt, ap);
    va_end(ap);

#ifdef EVM_ALONE
    printf(buf);
#else
    ocall_print_string(buf);
#endif

    return (int)strnlen(buf, LOG_BUFSIZE - 1) + 1;
#else
    return 0;
#endif
}

//# ifdef  __cplusplus
//extern "C" {

void enclave_print_hex_bytes(unsigned char *content, uint32_t len) {
    if (len == 0) {
        return;
    }
    if (len > LOG_BUFSIZE) {
        len = LOG_BUFSIZE;
    }
    char buf[LOG_BUFSIZE + 1] = { '\0' };
    memcpy(buf, content, len);
//    unsigned char *content_offset = content;
//    char *buf_offset = buf;
//    for (uint32_t i = 0; i < len; i++) {
//        if (i * 2 >= LOG_BUFSIZE) {
//            break;
//        }
//
//        int result_len = vsnprintf(buf_offset, LOG_BUFSIZE, "%02X,", *content_offset);
//        buf_offset += result_len;
//        content_offset++;
//    }
    enclave_printf("%x", buf);
}
//}
//# endif
