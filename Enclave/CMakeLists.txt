cmake_minimum_required(VERSION 2.8)             #确定版本号，必须步骤
project(ENCLAVE)                           #项目名称，随便写
SET(SGX_SDK /opt/intel/sgxsdk)                  #SDK的安装路径
SET(SGX_MODE SIM)                                #参数的设置，注意，如果给变量赋值的时候，值本身有空格，那么值需要使用“”来保证赋值的正确性，否则赋值可能存在错误
SET(SGX_ARCH x64)
SET(SGX_DEBUG 1)
SET(SGX_COMMON_CFLAGS -m64)
SET(SGX_LIBRARY_PATH ${SGX_SDK}/lib64 ${PROJECT_SOURCE_DIR}/openssl/lib64)
SET(SGX_EDGER8R ${SGX_SDK}/bin/x64/sgx_edger8r)
SET(SGX_ENCLAVE_SIGNER ${SGX_SDK}/bin/x64/sgx_sign) #以上的参数都是根据自己的机器设置，注意不要设置错误
IF(SGX_DEBUG EQUAL 1)
   SET(SGX_COMMON_CFLAGS "${SGX_COMMON_CFLAGS} -O0 -g")
ELSE(SGX_DEBUG EQUAL 1)
   SET(SGX_COMMON_CFLAGS "${SGX_COMMON_CFLAGS} -O2")
ENDIF(SGX_DEBUG EQUAL 1)

SET(Trts_Library_Name sgx_trts)
SET(Service_Library_Name sgx_tservice)
SET(Crypto_Library_Name sgx_tcrypto)
FILE(GLOB_RECURSE Enclave_Cpp_Files "*.cpp" "${PROJECT_SOURCE_DIR}/crypt/*.c" "${PROJECT_SOURCE_DIR}/TrustedLibrary/*.c" "${PROJECT_SOURCE_DIR}/Enclave.c")
INCLUDE_DIRECTORIES(${PROJECT_SOURCE_DIR}/test/include ${PROJECT_SOURCE_DIR}/hashmap/include ${PROJECT_SOURCE_DIR}/endian/include ${PROJECT_SOURCE_DIR}/hex/include ${PROJECT_SOURCE_DIR} ${SGX_SDK}/include ${SGX_SDK}/include/tlibc ${SGX_SDK}/include/libcxx ${PROJECT_SOURCE_DIR}/evm/include  ${PROJECT_SOURCE_DIR}/openssl/include ${PROJECT_SOURCE_DIR}/crypt)
SET(Enclave_C_Flags "${SGX_COMMON_CFLAGS} -nostdinc -fvisibility=hidden -fpie -ffunction-sections -fdata-sections -fstack-protector-strong")
SET(Enclave_Cpp_Flags "${Enclave_C_Flags} -std=c++11 -nostdinc++")
SET(Enclave_Link_Flags "${SGX_COMMON_CFLAGS} -L${SGX_LIBRARY_PATH} -Wl,--whole-archive -lsgx_tsgxssl -Wl,--no-whole-archive -lsgx_tsgxssl_crypto -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles -Wl,--whole-archive -l${Trts_Library_Name} -Wl,--no-whole-archive -Wl,--start-group -lsgx_tstdc -lsgx_tcxx -l${Crypto_Library_Name} -l${Service_Library_Name} -Wl,--end-group -Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined -Wl,-pie,-eenclave_entry -Wl,--export-dynamic -Wl,--defsym,__ImageBase=0 -Wl,--gc-sections -Wl,--version-script=${PROJECT_SOURCE_DIR}/Enclave.lds ")
#SET(Enclave_Link_Flags "${SGX_COMMON_CFLAGS} -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles -L${SGX_LIBRARY_PATH} -Wl,--whole-archive -l${Trts_Library_Name} -Wl,--no-whole-archive -Wl,--start-group -lsgx_tstdc -lsgx_tcxx -l${Crypto_Library_Name} -l${Service_Library_Name} -Wl,--end-group -Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined -Wl,-pie,-eenclave_entry -Wl,--export-dynamic -Wl,--defsym,__ImageBase=0 -Wl,--gc-sections -Wl,--version-script=${PROJECT_SOURCE_DIR}/Enclave.lds")#设置产生so文件的链接选项注意一下-Wl,--whole-archive -l${Trts_Library_Name} -Wl,--no-whole-archive -Wl,--start-group -lsgx_tstdc -lsgx_tcxx -l${Crypto_Library_Name} -l${Service_Library_Name} -Wl,--end-group 这些部分，连接的时候要把sgx_trts库所有相关的都要连接上，不然会报上一大堆错误，后面的不需要全部链接相关的库，只需要链接对应的库即可在makefile中的原文如下：
# To generate a proper enclave, it is recommended to follow below guideline to link the trusted libraries:
#    1. Link sgx_trts with the `--whole-archive' and `--no-whole-archive' options,
#       so that the whole content of trts is included in the enclave.
#    2. For other libraries, you just need to pull the required symbols.
#       Use `--start-group' and `--end-group' to link these libraries.
# Do NOT move the libraries linked with `--start-group' and `--end-group' within `--whole-archive' and `--no-whole-archive' options.
# Otherwise, you may get some undesirable errors.

set(EVM_CORE_SRCS
        evm/src/eth_vm.c
        evm/src/keccak256.c
        evm/src/sha3.c
        evm/src/uint256.c
        hex/src/hex.c
        endian/src/little_endian.c
        hashmap/src/hashmap.c
        enclave_log.c
        )

SET(CMAKE_CXX_STANDARD 11)
SET(CMAKE_C_FLAGS ${Enclave_C_Flags})#设置Cmake编译C文件的编译选项
SET(CMAKE_CXX_FLAGS ${Enclave_Cpp_Flags})#设置CMake编译Cpp文件的编译选项
SET(CMAKE_SHARED_LINKER_FLAGS ${Enclave_Link_Flags})#设置共享库的链接选项
LINK_DIRECTORIES(${SGX_LIBRARY_PATH})#共享库链接其他库的时候的搜索路径，此处是SDK中提供的静态库
ADD_LIBRARY(enclave SHARED ${Enclave_Cpp_Files} ./Enclave_t.c ${EVM_CORE_SRCS})#生成libenclave.so
TARGET_LINK_LIBRARIES(enclave ${PROJECT_SOURCE_DIR}/openssl/lib64/libsgx_tsgxssl.a)#链接libenclave.so需要>的库
TARGET_LINK_LIBRARIES(enclave ${PROJECT_SOURCE_DIR}/openssl/lib64/libsgx_tsgxssl_crypto.a)#链接libenclave.so需要>的库
TARGET_LINK_LIBRARIES(enclave ${PROJECT_SOURCE_DIR}/openssl/lib64/libsgx_usgxssl.a)#链接libenclave.so需要>的库
TARGET_LINK_LIBRARIES(enclave sgx_tprotected_fs sgx_pthread sgx_trts)#链接libenclave.so需要的库sgx_tprotected_fs
TARGET_LINK_LIBRARIES(enclave  sgx_tcxx sgx_tstdc sgx_tcrypto sgx_tservice)#链接libenclave.so需要的库
