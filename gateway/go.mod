module gateway

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.0.0
	chainmaker.org/chainmaker/pb-go/v2 v2.0.0
	chainmaker.org/chainmaker/sdk-go/v2 v2.0.0
	github.com/ethereum/go-ethereum v1.10.4
	github.com/gin-gonic/gin v1.7.1
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.4.3
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	github.com/unrolled/secure v1.0.8
	go.uber.org/zap v1.16.0
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
