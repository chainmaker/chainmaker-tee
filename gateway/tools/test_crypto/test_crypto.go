/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	rsa1 "chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/modes"
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
)

func aesKey() aes.AESKey {
	ran := make([]byte, 16)
	_, err := rand.Read(ran)
	if err != nil {
		panic(err)
	}
	aeskey := aes.AESKey{Key: ran}
	// 校验长度，原生成aeskey方法中有，此处注掉，再论证是否安全
	//bits := len(ran) * 8
	//if bits != int(crypto.BITS_SIZE_128) && bits != crypto.BITS_SIZE_192 && bits != crypto.BITS_SIZE_256 {
	//	return aes.AESKey{Key: ran}, errors.New("生成aeskey错误，检查长度")
	//}
	fmt.Println("aes key:", aeskey)
	return aeskey
}

func aesEncrypt(data []byte, aeskey aes.AESKey) []byte {
	//cdata, err := aeskey.Encrypt(data)
	cdata, err := aeskey.EncryptWithOpts(data,
		&crypto.EncOpts{
			EncodingType: modes.PADDING_NONE,
			BlockMode:    modes.BLOCK_MODE_GCM,
			EnableMAC:    true,
			Hash:         0,
			Label:        nil,
			EnableASN1:   false,
		})
	if err != nil {
		panic(err)
	}
	fmt.Println("crypt data:", cdata)
	fmt.Println("crypt data 64:", base64.StdEncoding.EncodeToString(cdata))
	return cdata
}

func aesDecrypt(cdata []byte, key aes.AESKey) []byte {
	data, err := key.DecryptWithOpts(cdata,
		&crypto.EncOpts{
			EncodingType: modes.PADDING_NONE,
			BlockMode:    modes.BLOCK_MODE_GCM,
			EnableMAC:    true,
			Hash:         0,
			Label:        nil,
			EnableASN1:   false,
		})
	if err != nil {
		panic(err)
	}
	fmt.Println("data:", data)
	fmt.Println("data 64:", base64.StdEncoding.EncodeToString(data))
	return data
}

func aesVerify(msg string, data string) bool {
	return msg == data
}

func aesTest(msg string) {
	aeskey := aesKey()
	cdata := aesEncrypt([]byte(msg), aeskey)
	data := aesDecrypt(cdata, aeskey)
	b := aesVerify(msg, string(data))
	fmt.Println("aes test result:", b)
}

func rsakey() (priKey *rsa.PrivateKey, pubKey *rsa.PublicKey, err error) {
	if priKey, err = rsa.GenerateKey(rand.Reader, 3072); err != nil {
		return
	}
	pubKey = &priKey.PublicKey
	return priKey, pubKey, nil
}

func rsaEncrypt(pubKey *rsa.PublicKey, digest []byte) ([]byte, error) {
	//return rsa.EncryptPKCS1v15(rand.Reader, pubKey, digest)
	pk := rsa1.PublicKey{K: pubKey}
	return pk.Encrypt(digest)
}

func rsaDecrypt(priKey *rsa.PrivateKey, cdata []byte) (aes.AESKey, error) {
	//aeskey, err := rsa.DecryptPKCS1v15(nil, priKey, cdata)
	pk := rsa1.PrivateKey{
		//keyType: crypto.RSA3072,
		K: priKey,
	}
	fmt.Println(pk.Type())
	aeskey, err := pk.Decrypt(cdata)
	if err != nil {
		panic(err)
	}
	return aes.AESKey{Key: aeskey}, nil
}

func rsaTest() {
	//aeskey := aesKey().Key
	//digest := sha256.Sum256(aeskey)
	digest := []byte("test")
	pri, pub, err := rsakey()
	if err != nil {
		panic(err)
	}
	cdata, err := rsaEncrypt(pub, digest[:])
	if err != nil {
		panic(err)
	}
	decrypted, err := rsaDecrypt(pri, cdata)
	if err != nil {
		panic(err)
	}
	deaeskey := decrypted.Key
	//b := string(aeskey) == string(deaeskey)
	b := "test" == string(deaeskey)
	fmt.Println("rsa test result:", b)
}

/**
 * 加密：
 * 生成密钥—>aeskey
 * RSA(key）—>rkey（传）
 * AES(data+key)—>加密数据cdata（传）
 */
func encrypt(msg string, pub *rsa.PublicKey) (rkey, cdata []byte) {
	// 生成密钥—>aeskey
	aeskey := aesKey()
	// RSA(key）—>Rkey（传）
	digest, err := aeskey.Bytes()
	if err != nil {
		panic(err)
	}
	rkey, err = rsaEncrypt(pub, digest[:])
	if err != nil {
		panic(err)
	}
	// AES(data+key)—>加密数据cdata（传）
	cdata = aesEncrypt([]byte(msg), aeskey)
	return rkey, cdata
}

/**
 * 解密：
 * RSA(rkey)—>aeskey
 * AES(cdata+key)—>data
 */
func decrypt(rkey, cdata []byte, priv *rsa.PrivateKey) []byte {
	// RSA(rkey)—>aeskey
	aeskey, err := rsaDecrypt(priv, rkey)
	if err != nil {
		panic(err)
	}
	// AES(cdata+key)—>data
	data := aesDecrypt(cdata, aeskey)
	fmt.Println("decrypt result:", data)
	return data
}

func test(msg string) {
	fmt.Println("input msg:", msg)
	// 外部准备rsa
	priv, pub, err := rsakey()
	if err != nil {
		panic(err)
	}
	// 加密
	rkey, cdata := encrypt(msg, pub)
	fmt.Println("encrypt rkey:", rkey)
	fmt.Println("encrypt cdata:", cdata)
	// 解密
	data := decrypt(rkey, cdata, priv)
	fmt.Println("decrypt data:", data)
	fmt.Println("verify result:", msg == string(data))
}

func getPKFromPEM() {
	f, _ := os.Open("./out_pubkey.pem")
	fmt.Println(f)
	data, _ := ioutil.ReadFile("./out_pubkey.pem")
	fmt.Println(data)
	pk, _ := asym.PublicKeyFromPEM(data)
	fmt.Println(pk)
}

func main() {
	msg := "testmsg"
	//aesTest(msg)
	//rsaTest()
	test(msg)

	//getPKFromPEM()
}
