pragma solidity ^0.6.6;
contract privacy_calc {
    uint256 public data_uint256;
    int8 public data_int8;
    string public data_str;
    address public data_addr;
    address public manager;


    constructor(address managerPerson) public {
        manager = managerPerson;
    }


    function hashCompareWithLengthCheckInternal(string memory _str1, string memory _str2) pure internal returns (bool checkResult) {
        if(keccak256(abi.encodePacked(_str1)) == keccak256(abi.encodePacked(_str2))) {
            // 如果二者相等，使checkResult为true
            checkResult = true;
        }else {
            checkResult = false;
        }
        // 返回checkResult
        return checkResult;
    }

    function strConcat(string memory _a, string memory _b) internal returns (string memory){
        bytes memory _ba = bytes(_a);
        bytes memory _bb = bytes(_b);
        string memory ret = new string(_ba.length + _bb.length);
        bytes memory bret = bytes(ret);
        uint k = 0;
        for (uint i = 0; i < _ba.length; i++)  bret[k++] = _ba[i];
        for (uint i = 0; i < _bb.length; i++) bret[k++] = _bb[i];
        return string(ret);

  }

    function calc(uint256 a_uint256, uint256 b_uint256, int8 a_int8, int8 b_int8, string memory a_str, string memory b_str, address a_addr, string memory func_name) public returns (uint256, int8, string memory, address) {
      if (hashCompareWithLengthCheckInternal(func_name, "add")) {
          data_uint256 = a_uint256 + b_uint256;
          data_int8 = a_int8 + b_int8;
          data_str = strConcat(a_str, b_str);
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "sub")) {
          data_uint256 = a_uint256 - b_uint256;
          data_int8 = a_int8 - b_int8;
          data_str = strConcat(a_str, b_str);
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "mul")) {
          data_uint256 = a_uint256 * b_uint256;
          data_int8 = a_int8 * b_int8;
          data_str = strConcat(a_str, b_str);
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "div")) {
          data_uint256 = a_uint256 / b_uint256;
          data_int8 = a_int8 / b_int8;
          data_str = strConcat(a_str, b_str);
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "set_data")) {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "failure")) {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
      } else if (hashCompareWithLengthCheckInternal(func_name, "revert")) {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
          revert();
      } else if (hashCompareWithLengthCheckInternal(func_name, "assert")) {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
          assert(false);
      } else if (hashCompareWithLengthCheckInternal(func_name, "require")) {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
          require(false);
      } else {
          data_uint256 = a_uint256;
          data_int8 = a_int8;
          data_str = a_str;
          data_addr = a_addr;
      }
      return (data_uint256, data_int8, data_str, data_addr);
    }

    function get_calc() public view returns (uint256, int8, string memory, address) {
        return (data_uint256, data_int8, data_str, data_addr);
    }

}