/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"bytes"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"io/ioutil"
	"net/http"
)

const (
	chainId          = "chain1"
	orgId            = "wx-org1.chainmaker.org"
	userCertFilePath = "cert/client1.tls.crt"
	userKeyFilePath  = "cert/client1.tls.key"
)

func call_remote_attestation() {

	remoteAttestationReq := &syscontract.RemoteAttestationRequest{
		Payload: &syscontract.RemoteAttestationPayload{
			Challenge: "This is a challenge from gateway user",
			OrgId:     []string{orgId},
		},
	}

	certString, err := getCert()
	if err != nil {
		fmt.Println("get cert err is:", err.Error())
		return
	}

	payloadBytes, err := json.Marshal(remoteAttestationReq.Payload)
	if err != nil {
		fmt.Println("marshal err is:", err.Error())
		return
	}

	signature, err := getSignature(orgId, payloadBytes)
	if err != nil {
		fmt.Println("err is:", err.Error())
		return
	}
	clientSign := hex.EncodeToString(signature)

	signInfo := &syscontract.SignInfo{
		ClientSign: clientSign,
		Cert:       certString,
	}

	remoteAttestationReq.SignPair = []*syscontract.SignInfo{signInfo}

	jsonBytes, err := json.Marshal(remoteAttestationReq)
	if err != nil {
		fmt.Println("json marshal request data failed, ", err)
		return
	}
	fmt.Println(string(jsonBytes))
	resp, err := http.Post("http://192.168.12.192:9090/private/remote_attestation", "application/json", bytes.NewReader(jsonBytes))
	if err != nil {
		fmt.Println("post request failed, ", err)
		return
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("read body error: %v \n", err)
		return
	}
	fmt.Printf("body => %s \n", body)
}

func getCert() (string, error) {
	codes, err := ioutil.ReadFile(userCertFilePath)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(codes), nil
}

func getSignature(orgId string, payLoad []byte) ([]byte, error) {
	userCertBytes, err := ioutil.ReadFile(userCertFilePath)
	if err != nil {
		return nil, err
	}

	privateKey, err := getPrivateKey(userKeyFilePath)
	if err != nil {
		return nil, err
	}

	userCert, err := ParseCert(userCertBytes)
	if err != nil {
		return nil, err
	}

	signBytes, err := SignTx(privateKey, userCert, payLoad)
	if err != nil {
		return nil, fmt.Errorf("SignTx failed, %s", err)
	}

	return signBytes, nil
}

func SignTx(privateKey crypto.PrivateKey, cert *bcx509.Certificate, msg []byte) ([]byte, error) {
	var opts crypto.SignOpts
	hashalgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		return nil, fmt.Errorf("invalid algorithm: %v", err)
	}

	opts.Hash = hashalgo
	opts.UID = crypto.CRYPTO_DEFAULT_UID

	return privateKey.SignWithOpts(msg, &opts)
}

func CalcUnsignedTxRequestBytes(txReq *common.TxRequest) ([]byte, error) {
	if txReq == nil {
		return nil, errors.New("calc unsigned tx request bytes error, tx == nil")
	}

	return CalcUnsignedTxBytes(&common.Transaction{
		Payload: txReq.Payload,
	})
}

func getPrivateKey(userKeyFilePath string) (crypto.PrivateKey, error) {

	// 从私钥文件读取用户私钥，转换为privateKey对象
	skBytes, err := ioutil.ReadFile(userKeyFilePath)
	if err != nil {
		return nil, fmt.Errorf("read user key file failed, %s", err)
	}

	privateKey, err := asym.PrivateKeyFromPEM(skBytes, nil)
	if err != nil {
		return nil, fmt.Errorf("parse user key file to privateKey obj failed, %s", err)
	}

	return privateKey, nil
}

func ParseCert(crtPEM []byte) (*bcx509.Certificate, error) {
	certBlock, _ := pem.Decode(crtPEM)
	if certBlock == nil {
		return nil, fmt.Errorf("decode pem failed, invalid certificate")
	}

	cert, err := bcx509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("x509 parse cert failed, %s", err)
	}

	return cert, nil
}

// calculate unsigned transaction bytes [header bytes || request payload bytes]
func CalcUnsignedTxBytes(t *common.Transaction) ([]byte, error) {
	if t == nil {
		return nil, errors.New("calc unsigned tx bytes error, tx == nil")
	}

	payloadBytes, err := proto.Marshal(t.Payload)
	if err != nil {
		return nil, err
	}
	return payloadBytes, nil
}

func main() {
	call_remote_attestation()
}
