pragma solidity ^0.5.11;

contract compute {
    uint256 result;
    string data;
    address manager;
    constructor(address managerPerson) public {
        manager = managerPerson;
    }
    function add(uint256 a, uint256 b) public returns (uint256) {
        result = a+b;
        return result;
    }

    function query_result() public view returns (uint256) {
        return result;
    }

    function save(string memory s) public returns (string memory) {
        data = s;
        return s;
    }

    function query_data() public view returns(string memory) {
        return data;
    }
}