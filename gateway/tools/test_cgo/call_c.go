/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

// #include <stdio.h>
// #include <stdlib.h>
//
// static void myprint(unsigned char* s, uint32_t len) {
//   for (int i = 0; i < len; i++) {
//       printf("%02X,", s[i]);
//   }
//   printf("\n");
// }
import "C"
import (
	"encoding/hex"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"io/ioutil"
	"math/big"
	"strings"
)

func main() {
	filePath := "tools/contract3"
	abiPath := "tools/abi.json"
	codes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Print("read contract file failed, ", err)
		return
	}

	codeBytes := make([]byte, hex.DecodedLen(len(codes)))
	_, err = hex.Decode(codeBytes, codes)
	if err != nil {
		fmt.Println("hex decode codes failed, ", err)
		return
	}
	abiJson, err := ioutil.ReadFile(abiPath)
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	dataBytes, err := myAbi.Pack("add", big.NewInt(1), big.NewInt(2))
	method := dataBytes[0:4]
	fmt.Println("method: ")
	C.myprint((*C.uchar)(C.CBytes(method)), C.uint32_t(len(method)))
	fmt.Println("data: ")
	C.myprint((*C.uchar)(C.CBytes(dataBytes)), C.uint32_t(len(dataBytes)))
	fmt.Println("codes: ")
	C.myprint((*C.uchar)(C.CBytes(codeBytes)), C.uint32_t(len(codeBytes)))
	//C.free(unsafe.Pointer(cs))
}
