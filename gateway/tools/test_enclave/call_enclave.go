/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/hex"
	"fmt"
	"gateway/bridge"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"io/ioutil"
	"strings"
)

func main()  {
	filePath := "tools/contract_with_string"
	abiPath := "tools/abi_with_string.json"
	codes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Print("read contract file failed, ", err)
		return
	}

	codeBytes := make([]byte, hex.DecodedLen(len(codes)))
	_, err = hex.Decode(codeBytes, codes)
	if err != nil {
		fmt.Println("hex decode codes failed, ", err)
		return
	}
	fmt.Println("Contract codes: --------------")
	for _, a := range codeBytes {
		fmt.Printf("0x%02x,", a)
	}
	fmt.Println("\n--------------")

	abiJson, err := ioutil.ReadFile(abiPath)
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	//dataBytes, err := myAbi.Pack("add", big.NewInt(1), big.NewInt(2))
	dataBytes, err := myAbi.Pack("save", "xxxxxxxxx")
	
	if bridge.InitializeEnclave() < 0 {
		fmt.Println("InitializeEnclave failed")
		return
	}
	defer bridge.SgxDestroyEnclave(bridge.GlobalEid)
	password := []byte("test")
	result, err := bridge.EcallInvoke(bridge.GlobalEid, "private_compute", "1.1.0", []byte("sddsss"),
		dataBytes, password, codeBytes,[]byte("userCert"), false)
	if err != nil {
		fmt.Println("EcallInvoke failed, ", err)
		return
	}
	fmt.Println(result)
}

