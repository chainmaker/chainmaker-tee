/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"bytes"
	"strconv"
	"time"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/modes"
	"chainmaker.org/chainmaker/common/v2/crypto/x509"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	evm "chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"github.com/ethereum/go-ethereum/accounts/abi"

	//"chainmaker.org/chainmaker-sdk-go/pb/protogo/common"
	"crypto/rand"
	rsa2 "crypto/rsa"
	"crypto/sha256"
	x509_2 "crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"math/big"
	"net/http"
	"strings"
	//"sync"
	//"sync"
)

const (
	orgId            = "wx-org1.chainmaker.org"
	userCertFilePath = "cert/client1.tls.crt"
	userKeyFilePath  = "cert/client1.tls.key"
)

func call_deploy() error {
	filePath := "tools/contract_with_string"
	abiPath := "tools/abi_with_string.json"
	//filePath := "tools/contract_complex"
	//abiPath := "tools/abi_complex.json"
	codes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Print("read contract file failed, ", err.Error())
		return err
	}
	abiJson, err := ioutil.ReadFile(abiPath)
	if err != nil {
		fmt.Println("read abi file failed, ", err.Error())
		return err
	}
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	client1Addr := "1087848554046178479107522336262214072175637027873"
	dataBytes, err := myAbi.Pack("", evm.BigToAddress(evm.FromDecimalString(client1Addr)))
	//dataBytes, err := myAbi.Pack("add", big.NewInt(0).Add(
	//	big.NewInt(0).Add(big.NewInt(0).Lsh(big.NewInt(0x7fffffffffffffff), 128),
	//	big.NewInt(0).Lsh(big.NewInt(0x7fffffffffffffff), 64)),
	//	big.NewInt(0x7fffffffffffffff)), big.NewInt(0).Mul(big.NewInt(0x7fffffffffffffff), big.NewInt(4)))
	//dataBytes, err := myAbi.Pack("save", "testtestttttt")
	// crypto dataBytes & RSAkey(passwd)
	//client1Addr := "1087848554046178479107522336262214072175637027873"
	//dataBytes, err := myAbi.Pack("calc", big.NewInt(256), big.NewInt(256), int8(8), int8(8),
	//	string("ab"), string("cd"), evm.BigToAddress(evm.FromDecimalString(client1Addr)), "set_data")
	//dataBytes, err := myAbi.Pack("save", "testtestttttt")
	if err != nil {
		fmt.Printf("abi pack method failed, %s", err.Error())
		return err
	}
	fmt.Println("deploy args: ")
	for i := 0; i < len(dataBytes); i++ {
		fmt.Printf("0x%02x,", dataBytes[i])
	}
	fmt.Println()

	rkey, cdata, err := cryptoData(dataBytes)
	if err != nil {
		fmt.Println("cryptoData failed: ", err)
		return err
	}

	fmt.Println()
	codeBytes := make([]byte, hex.DecodedLen(len(codes)))
	_, err = hex.Decode(codeBytes, codes)
	if err != nil {
		fmt.Println("decode codes failed: ", err)
		return err
	}
	h := sha256.New()
	h.Write(codeBytes)
	codeHash := h.Sum(nil)
	fmt.Printf("code hash: %v\n", codeHash)
	fmt.Printf("contract codes len: %d, codes: \n", len(codeBytes))
	for i := 0; i < len(codeBytes); i++ {
		fmt.Printf("0x%02x,", codeBytes[i])
	}
	currentTime := time.Now().Unix() //second
	currentTimeStr := strconv.FormatInt(currentTime, 10)
	fmt.Printf("now time stamp : %s\n", currentTimeStr)

	privateDeployReq := &syscontract.PrivateDeployRequest{
		Payload: &syscontract.PrivateDeployPayload{
			CodeBytes:       string(codes),
			PrivateRlpData:  hex.EncodeToString(cdata),
			CodeHash:        hex.EncodeToString(codeHash[:]),
			ContractName:    "private_compute45",
			ContractVersion: "1.1.1",
			Passwd:          hex.EncodeToString(rkey),
			OrgId:           []string{orgId},
			TimeStamp:       currentTimeStr,
		},
	}

	payloadBytes, err := privateDeployReq.Payload.Marshal()
	if err != nil {
		fmt.Println("create payload bytes json marshal  err is:", err.Error())
		return err
	}

	privateKey, err := getPrivateKey(userKeyFilePath)
	if err != nil {
		fmt.Printf("get private key err is:%v", err.Error())
		return err
	}

	userCertBytes, err := ioutil.ReadFile(userCertFilePath)
	if err != nil {
		fmt.Println("read user cert file bytes err is ", err)
		return err
	}
	fmt.Println("client user cert bytes is ", userCertBytes)
	userCert, err := ParseCert(userCertBytes)
	if err != nil {
		fmt.Println("parse cert err is ", err)
		return err
	}

	signedBytes, err := signCheckCallerPayload(payloadBytes, privateKey, userCert)
	if err != nil {
		fmt.Println("sign check caller payload err is ", err)
		return err
	}

	fmt.Printf("client request payloadBytes: %v\n client signBytes: %v \n", payloadBytes, signedBytes)

	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(signedBytes),
		Cert:       hex.EncodeToString(userCertBytes),
	}

	privateDeployReq.SignPair = append(privateDeployReq.SignPair, signInfo)
	jsonBytes, err := json.Marshal(privateDeployReq)
	if err != nil {
		fmt.Println("json marshal request data failed, ", err.Error())
		return err
	}

	resp, err := http.Post("http://192.168.12.192:9090/private/deploy", "application/json", bytes.NewReader(jsonBytes))
	if err != nil {
		fmt.Println("post request failed, ", err.Error())
		return err
	}
	if resp.StatusCode > 300 {
		return fmt.Errorf("api return failed, resp: %v\n", resp)
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("err is:", err.Error())
		return err
	}

	fmt.Println("resp.body is:", string(respBody))
	return nil
}

func call_tee(i int64) error {
	filePath := "tools/contract_with_string"
	abiPath := "tools/abi_with_string.json"
	//filePath := "tools/contract_complex"
	//abiPath := "tools/abi_complex.json"
	codes, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("read contract file failed, ", err.Error())
		return err
	}

	abiJson, err := ioutil.ReadFile(abiPath)
	if err != nil {
		fmt.Println("read abi file failed, ", err.Error())
		return err
	}
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	if err != nil {
		fmt.Println("abi.json failed, ", err.Error())
		return err
	}
	//client1Addr := "1087848554046178479107522336262214072175637027873"
	//dataBytes, err := myAbi.Pack("calc", big.NewInt(256),big.NewInt(256),int8(8),int8(8),string("ab"),
	//	string("cd"),evm.BigToAddress(evm.FromDecimalString(client1Addr)),"set_data")
	//dataBytes, err := myAbi.Pack("add", big.NewInt(5), big.NewInt(5))
	//dataBytes, err := myAbi.Pack("add", big.NewInt(0).Add(
	//	big.NewInt(0).Add(big.NewInt(0).Lsh(big.NewInt(0x7fffffffffffffff), 128),
	//	big.NewInt(0).Lsh(big.NewInt(0x7fffffffffffffff), 64)),
	//	big.NewInt(0x7fffffffffffffff)), big.NewInt(0).Mul(big.NewInt(0x7fffffffffffffff), big.NewInt(4)))

	dataBytes, err := myAbi.Pack("add", big.NewInt(i), big.NewInt(i))
	// crypto dataBytes & RSAkey(passwd)
	//client1Addr := "1087848554046178479107522336262214072175637027873"
	//dataBytes, err := myAbi.Pack("calc", big.NewInt(256), big.NewInt(256), int8(8), int8(8),
	//	string("ab"), string("cd"), evm.BigToAddress(evm.FromDecimalString(client1Addr)), "set_data")

	//client1Addr := "1087848554046178479107522336262214072175637027873"
	//dataBytes, err := myAbi.Pack("calc", big.NewInt(256), big.NewInt(0), int8(8), int8(0),
	//	string("ab"), string("cd"), evm.BigToAddress(evm.FromDecimalString(client1Addr)), "div")
	//dataBytes, err := myAbi.Pack("save", "testtestttttt")
	if err != nil {
		fmt.Println("abi pack failed, ", err.Error())
		return err
	}
	fmt.Println("calldata: ")
	for i := 0; i < len(dataBytes); i++ {
		fmt.Printf("0x%02x,", dataBytes[i])
	}
	fmt.Println()

	rkey, cdata, err := cryptoData(dataBytes)
	if err != nil {
		fmt.Println("cryptoData failed: ", err)
		return err
	}

	codeBytes := make([]byte, hex.DecodedLen(len(codes)))
	_, err = hex.Decode(codeBytes, codes)
	if err != nil {
		fmt.Println("decode codes failed: ", err)
		return err
	}
	h := sha256.New()
	h.Write(codeBytes)
	codeHash := h.Sum(nil)
	fmt.Printf("code hash: %v\n", codeHash)
	fmt.Printf("contract codes len: %d, codes: \n", len(codeBytes))
	for i := 0; i < len(codeBytes); i++ {
		fmt.Printf("0x%02x,", codeBytes[i])
	}
	fmt.Println()

	currentTime := time.Now().Unix() //second
	currentTimeStr := strconv.FormatInt(currentTime, 10)
	fmt.Printf("now time stamp : %s\n", currentTimeStr)

	privateComputeReq := &syscontract.PrivateComputeRequest{
		Payload: &syscontract.PrivateComputePayload{
			PrivateRlpData: hex.EncodeToString(cdata),
			CodeHash:       hex.EncodeToString(codeHash[:]),
			ContractName:   "private_compute45",
			Passwd:         hex.EncodeToString(rkey),
			OrgId:          []string{orgId},
			TimeStamp:      currentTimeStr,
		},
	}
	payloadBytes, err := privateComputeReq.Payload.Marshal()
	if err != nil {
		fmt.Println("create payload bytes json marshal err: ", err.Error())
		return err
	}

	privateKey, err := getPrivateKey(userKeyFilePath)
	if err != nil {
		fmt.Println("get private key err: ", err)
		return err
	}

	userCertBytes, err := ioutil.ReadFile(userCertFilePath)
	if err != nil {
		fmt.Println("read user cert file bytes err: ", err.Error())
		return err
	}

	fmt.Println("client user cert bytes is:", userCertBytes)

	userCert, err := ParseCert(userCertBytes)
	if err != nil {
		fmt.Println("parse cert err: ", err)
		return err
	}

	signedBytes, err := signCheckCallerPayload(payloadBytes, privateKey, userCert)
	if err != nil {
		fmt.Println("sign check caller payload err: ", err.Error())
		return err
	}

	fmt.Printf("PayloadBytes: %v\n SignedBytes: %v ", payloadBytes, signedBytes)

	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(signedBytes),
		Cert:       hex.EncodeToString(userCertBytes),
	}

	privateComputeReq.SignPair = append(privateComputeReq.SignPair, signInfo)
	//fmt.Println("Cert len: ", len(privateComputeReq.Cert))
	jsonBytes, err := json.Marshal(privateComputeReq)
	if err != nil {
		fmt.Println("json marshal request data failed, ", err.Error())
		return err
	}

	//jsonBytes, err := privateComputeReq.Marshal()
	//if err != nil{
	//	fmt.Println("json marshal request data failed, ", err.Error())
	//	return
	//}

	resp, err := http.Post("http://192.168.12.192:9090/private/compute", "application/json", bytes.NewReader(jsonBytes))
	if err != nil {
		fmt.Println("post request failed, ", err.Error())
		return err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("err is:", err.Error())
		return err
	}

	fmt.Println("resp.body is:", string(respBody))
	return nil
}

func ParseCert(crtPEM []byte) (*bcx509.Certificate, error) {
	certBlock, _ := pem.Decode(crtPEM)
	if certBlock == nil {
		return nil, fmt.Errorf("decode pem failed, invalid certificate")
	}

	cert, err := bcx509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("x509 parse cert failed, %s", err.Error())
	}

	return cert, nil
}

func SignTx(privateKey crypto.PrivateKey, cert *bcx509.Certificate, msg []byte) ([]byte, error) {
	var opts crypto.SignOpts
	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		return nil, fmt.Errorf("invalid algorithm: %v", err.Error())
	}

	opts.Hash = hashAlgo
	opts.UID = crypto.CRYPTO_DEFAULT_UID

	return privateKey.SignWithOpts(msg, &opts)
}

func getPrivateKey(userKeyFilePath string) (crypto.PrivateKey, error) {
	// 从私钥文件读取用户私钥，转换为privateKey对象
	skBytes, err := ioutil.ReadFile(userKeyFilePath)
	if err != nil {
		return nil, fmt.Errorf("read user key file failed, %s", err.Error())
	}

	privateKey, err := asym.PrivateKeyFromPEM(skBytes, nil)
	if err != nil {
		return nil, fmt.Errorf("parse user key file to privateKey obj failed, %s", err.Error())
	}

	return privateKey, nil
}

func cryptoData(dataBytes []byte) ([]byte, []byte, error) {
	var OidExtPubKey = asn1.ObjectIdentifier{1, 2, 840, 113549, 1, 12, 10, 1, 1}
	fmt.Printf("Encrypting data = \n")
	for _, v := range dataBytes {
		fmt.Printf("%02x ", v)
	}
	fmt.Printf("\n")

	pemCert, err := ioutil.ReadFile("in_teecert.pem")
	if err != nil {
		return nil, nil, errors.New("read cert error")
	}

	derCert, _ := pem.Decode(pemCert)
	if derCert == nil {
		return nil, nil, errors.New("decode cert error")
	}

	cert, err := x509.ParseCertificate(derCert.Bytes)
	if err != nil {
		return nil, nil, errors.New("parsecertificate error")
	}

	pubk, err := x509.GetExtByOid(OidExtPubKey, cert.Extensions)
	if err != nil {
		return nil, nil, err
	}
	fmt.Printf("pubkey = \n%s\n", pubk)

	block, _ := pem.Decode(pubk)
	if block == nil {
		return nil, nil, errors.New("decode pubk error")
	}

	//pk, err := x509.ParsePKCS1PublicKey(block.Bytes)
	pk, err := x509_2.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, nil, err
	}

	// 生成密钥—>aeskey
	ran := make([]byte, 16)
	_, err = rand.Read(ran)
	if err != nil {
		return nil, nil, err
	}

	aeskey := aes.AESKey{Key: ran}
	// RSA(key）—>Rkey（传）
	digest, err := aeskey.Bytes()
	if err != nil {
		return nil, nil, err
	}

	//publicKey := rsa.PublicKey{K: pk}
	publicKey := rsa.PublicKey{K: pk.(*rsa2.PublicKey)}
	rkey, err := publicKey.Encrypt(digest)
	if err != nil {
		return nil, nil, err
	}

	// AES(data+key)—>加密数据cdata（传）
	//cdata, err := aeskey.Encrypt(dataBytes)
	cdata, err := aeskey.EncryptWithOpts(dataBytes,
		&crypto.EncOpts{
			EncodingType: modes.PADDING_NONE,
			BlockMode:    modes.BLOCK_MODE_GCM,
			EnableMAC:    true,
			Hash:         0,
			Label:        nil,
			EnableASN1:   false,
		})
	if err != nil {
		return nil, nil, err
	}

	fmt.Println("AES encrypt result len:", len(cdata))
	fmt.Println("AES encrypt result (base64 encoded):", base64.StdEncoding.EncodeToString(cdata))
	fmt.Println("RSA encrypted aes key:", rkey)
	return rkey, cdata, nil
}

func signCheckCallerPayload(payLoadBytes []byte, privateKey crypto.PrivateKey, cert *x509.Certificate) ([]byte, error) {

	signBytes, err := signPayload(privateKey, cert, payLoadBytes)
	if err != nil {
		return nil, fmt.Errorf("SignPayload failed, %s", err.Error())
	}

	return signBytes, nil
}

func signPayload(privateKey crypto.PrivateKey, cert *bcx509.Certificate, msg []byte) ([]byte, error) {
	return SignTx(privateKey, cert, msg)
}

func main() {
	var err error

	//err = call_deploy()
	//if err != nil {
	//	fmt.Println("call deploy failed", err)
	//	return
	//}

	err = call_tee(1)
	if err != nil {
		fmt.Println("call tee failed, err: ", err)
		return
	}
	//var wg sync.WaitGroup
	//for i := int64(0); i < 100000; i++ {
	//	for j := 0; j < 10; j++ {
	//		go func() {
	//			wg.Add(1)
	//			defer wg.Done()
	//			err = call_tee(i)
	//			if err != nil {
	//				fmt.Println("call tee failed, err: ", err)
	//				return
	//			}
	//		}()
	//		i++
	//	}
	//	wg.Wait()
	//}
}
