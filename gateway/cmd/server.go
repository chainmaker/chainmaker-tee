/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmd

import (
	"context"
	"fmt"
	"gateway/cmclient"
	config2 "gateway/config"
	"gateway/loggers"
	"gateway/modules"

	"golang.org/x/time/rate"

	//"gateway/verify"
	"gateway/routes"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/unrolled/secure"
)

var (
	config    string
	port      string
	ServerCmd = &cobra.Command{
		Use:     "start",
		Short:   "start API server",
		Example: "sgx server ./config.yml",
		RunE: func(cmd *cobra.Command, args []string) error {
			return startServer()
		},
	}
)

//startServer  start server
func startServer() error {
	s := newService()
	//start
	go s.startService()
	//print usage
	s.usage()
	//wait signal
	s.waitSignal()

	return s.showDownService()
}

func PreServer() {
	ServerCmd.PersistentFlags().StringVarP(&config, "config", "c", "./config.yml", "start server with provided configuration file")
	ServerCmd.PersistentFlags().StringVarP(&port, "port", "p", "", "tcp port server listening on")
	//print server info
	usage()
	//load config
	config2.LoadConfig(config)

	// init log
	loggers.Init()

	//init client
	if err := cmclient.InitCmClient(); err != nil {
		loggers.Error(err.Error())
		fmt.Println(err)
		os.Exit(-1)
	}
}

func usage() {
	usageStr := `starting sgx server`
	log.Printf("%s\n", usageStr)
}

//SetConfig
func SetConfig(configPath string, key string, value interface{}) {
	viper.AddConfigPath(configPath)
	viper.Set(key, value)
	_ = viper.WriteConfig()
}

type server struct {
	srv    *http.Server
	ctx    context.Context
	cancel context.CancelFunc
}

func newService() *server {
	srv := &http.Server{
		Addr:    config2.ApplicationConfig.Host + ":" + config2.ApplicationConfig.Port,
		Handler: initEngine(),
	}

	if port != "" {
		SetConfig(config, "settings.application.port", port)
	}

	return &server{srv: srv}
}

func (s *server) startService() {
	if config2.ApplicationConfig.IsHttps {
		if err := s.srv.ListenAndServeTLS(config2.SslConfig.Pem, config2.SslConfig.KeyStr); err != nil && err != http.ErrServerClosed {
			log.Printf("listen: %s\n", err)
			return
		}
	} else {
		if err := s.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
			return
		}
	}
}

func (s *server) usage() {
	fmt.Printf("%s Server Run http://%s:%s/ \r\n",
		time.Now().Format("2006/01/02 15:04:05"),
		config2.ApplicationConfig.Host,
		config2.ApplicationConfig.Port)
	fmt.Printf("%s Enter Control + C Shutdown Server \r\n", time.Now().Format("2006/01/02 15:04:05"))
}

func (s *server) waitSignal() {
	// wait signal to graceful exit
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
}

func (s *server) showDownService() error {
	fmt.Printf("%s Shutdown Server ... \r\n", time.Now().Format("2006/01/02 15:04:05"))
	s.ctx, s.cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer s.cancel()
	if err := s.srv.Shutdown(s.ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
		return err
	}
	log.Println("Server exiting...")
	return nil
}

func initEngine() *gin.Engine {
	r := gin.New()
	if config2.ApplicationConfig.IsHttps {
		r.Use(tlsHandler())
	}

	//limiter
	if config2.ApplicationConfig.Concurrency > 0 {
		r.Use(limitMiddleWere())
	}

	routes.InitRouters(r)
	return r
}

func tlsHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		secureMiddleware := secure.New(secure.Options{
			SSLRedirect: true,
			SSLHost:     config2.ApplicationConfig.Domain,
		})
		err := secureMiddleware.Process(c.Writer, c.Request)
		if err != nil {
			return
		}
		c.Next()
	}
}

func limitMiddleWere() gin.HandlerFunc {
	limitMax := config2.ApplicationConfig.Concurrency
	limiter := rate.NewLimiter(rate.Every(time.Second), limitMax)
	loggers.Info("limit max is：", limitMax)

	return func(c *gin.Context) {
		if !limiter.Allow() {
			loggers.Info("over max concurrent limit", limitMax)
			modules.FailResponse(c, "over max concurrent limit")
			return
		}
		c.Next()
	}
}
