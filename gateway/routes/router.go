/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package routes

import (
	"gateway/controller"
	"github.com/gin-gonic/gin"
)

func InitRouters(e *gin.Engine) {
	e.POST("private/deploy", controller.Deploy)
	e.POST("private/compute", controller.Compute)
	e.POST("private/remote_attestation", controller.RemoteAttestation)
	e.GET("healthy", controller.Healthy)
}
