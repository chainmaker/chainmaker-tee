#include "Enclave_u.h"
#include <errno.h>

typedef struct ms_sign_data_t {
	char* ms_retval;
	char* ms_data;
	uint32_t ms_data_len;
} ms_sign_data_t;

typedef struct ms_ecall_invoke_t {
	sgx_status_t ms_retval;
	char* ms_contract_name;
	size_t ms_contract_name_len;
	char* ms_version;
	size_t ms_version_len;
	unsigned char* ms_code_hash;
	uint32_t ms_hash_len;
	unsigned char* ms_private_rlp_data;
	uint32_t ms_data_len;
	unsigned char* ms_password;
	uint32_t ms_password_len;
	uint8_t* ms_byte_code;
	uint32_t ms_code_len;
	char* ms_private_req;
	uint32_t ms_private_req_len;
	uint32_t ms_is_deployment;
	evm_result_data_t* ms_evm_result;
} ms_ecall_invoke_t;

typedef struct ms_ecall_crypt_init_t {
	int ms_retval;
	char* ms_signAlgorithm;
	size_t ms_signAlgorithm_len;
} ms_ecall_crypt_init_t;

typedef struct ms_ecall_get_proof_size_t {
	int ms_retval;
	char* ms_challenge;
	uint32_t ms_challenge_len;
	uint32_t* ms_proof_len;
} ms_ecall_get_proof_size_t;

typedef struct ms_ecall_remote_attestation_prove_t {
	int ms_retval;
	char* ms_challenge;
	uint32_t ms_challenge_len;
	char* ms_proof;
	uint32_t ms_proof_len;
	uint32_t* ms_actual_proof_len;
} ms_ecall_remote_attestation_prove_t;

typedef struct ms_ecall_get_report_size_t {
	int ms_retval;
	uint32_t* ms_report_len;
} ms_ecall_get_report_size_t;

typedef struct ms_ecall_get_report_t {
	int ms_retval;
	uint32_t ms_report_len;
	unsigned char* ms_report;
} ms_ecall_get_report_t;

typedef struct ms_ocall_print_string_t {
	char* ms_str;
} ms_ocall_print_string_t;

typedef struct ms_ocall_get_t {
	int ms_retval;
	char* ms_contract_name;
	char* ms_key;
	char* ms_result;
	uint32_t* ms_result_len;
} ms_ocall_get_t;

typedef struct ms_ocall_export_csr_t {
	char* ms_csr;
} ms_ocall_export_csr_t;

typedef struct ms_ocall_export_pubkey_t {
	char* ms_key;
} ms_ocall_export_pubkey_t;

typedef struct ms_ocall_get_teecert_t {
	char** ms_cert;
	uint32_t* ms_cert_len;
} ms_ocall_get_teecert_t;

typedef struct ms_ocall_export_report_t {
	char* ms_report;
} ms_ocall_export_report_t;

typedef struct ms_u_sgxssl_ftime_t {
	void* ms_timeptr;
	uint32_t ms_timeb_len;
} ms_u_sgxssl_ftime_t;

typedef struct ms_sgx_oc_cpuidex_t {
	int* ms_cpuinfo;
	int ms_leaf;
	int ms_subleaf;
} ms_sgx_oc_cpuidex_t;

typedef struct ms_sgx_thread_wait_untrusted_event_ocall_t {
	int ms_retval;
	const void* ms_self;
} ms_sgx_thread_wait_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_set_untrusted_event_ocall_t {
	int ms_retval;
	const void* ms_waiter;
} ms_sgx_thread_set_untrusted_event_ocall_t;

typedef struct ms_sgx_thread_setwait_untrusted_events_ocall_t {
	int ms_retval;
	const void* ms_waiter;
	const void* ms_self;
} ms_sgx_thread_setwait_untrusted_events_ocall_t;

typedef struct ms_sgx_thread_set_multiple_untrusted_events_ocall_t {
	int ms_retval;
	const void** ms_waiters;
	size_t ms_total;
} ms_sgx_thread_set_multiple_untrusted_events_ocall_t;

typedef struct ms_u_sgxprotectedfs_exclusive_file_open_t {
	void* ms_retval;
	const char* ms_filename;
	uint8_t ms_read_only;
	int64_t* ms_file_size;
	int32_t* ms_error_code;
} ms_u_sgxprotectedfs_exclusive_file_open_t;

typedef struct ms_u_sgxprotectedfs_check_if_file_exists_t {
	uint8_t ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_check_if_file_exists_t;

typedef struct ms_u_sgxprotectedfs_fread_node_t {
	int32_t ms_retval;
	void* ms_f;
	uint64_t ms_node_number;
	uint8_t* ms_buffer;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_fread_node_t;

typedef struct ms_u_sgxprotectedfs_fwrite_node_t {
	int32_t ms_retval;
	void* ms_f;
	uint64_t ms_node_number;
	uint8_t* ms_buffer;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_fwrite_node_t;

typedef struct ms_u_sgxprotectedfs_fclose_t {
	int32_t ms_retval;
	void* ms_f;
} ms_u_sgxprotectedfs_fclose_t;

typedef struct ms_u_sgxprotectedfs_fflush_t {
	uint8_t ms_retval;
	void* ms_f;
} ms_u_sgxprotectedfs_fflush_t;

typedef struct ms_u_sgxprotectedfs_remove_t {
	int32_t ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_remove_t;

typedef struct ms_u_sgxprotectedfs_recovery_file_open_t {
	void* ms_retval;
	const char* ms_filename;
} ms_u_sgxprotectedfs_recovery_file_open_t;

typedef struct ms_u_sgxprotectedfs_fwrite_recovery_node_t {
	uint8_t ms_retval;
	void* ms_f;
	uint8_t* ms_data;
	uint32_t ms_data_length;
} ms_u_sgxprotectedfs_fwrite_recovery_node_t;

typedef struct ms_u_sgxprotectedfs_do_file_recovery_t {
	int32_t ms_retval;
	const char* ms_filename;
	const char* ms_recovery_filename;
	uint32_t ms_node_size;
} ms_u_sgxprotectedfs_do_file_recovery_t;

static sgx_status_t SGX_CDECL Enclave_ocall_print_string(void* pms)
{
	ms_ocall_print_string_t* ms = SGX_CAST(ms_ocall_print_string_t*, pms);
	ocall_print_string(ms->ms_str);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_ocall_get(void* pms)
{
	ms_ocall_get_t* ms = SGX_CAST(ms_ocall_get_t*, pms);
	ms->ms_retval = ocall_get(ms->ms_contract_name, ms->ms_key, ms->ms_result, ms->ms_result_len);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_ocall_export_csr(void* pms)
{
	ms_ocall_export_csr_t* ms = SGX_CAST(ms_ocall_export_csr_t*, pms);
	ocall_export_csr(ms->ms_csr);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_ocall_export_pubkey(void* pms)
{
	ms_ocall_export_pubkey_t* ms = SGX_CAST(ms_ocall_export_pubkey_t*, pms);
	ocall_export_pubkey(ms->ms_key);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_ocall_get_teecert(void* pms)
{
	ms_ocall_get_teecert_t* ms = SGX_CAST(ms_ocall_get_teecert_t*, pms);
	ocall_get_teecert(ms->ms_cert, ms->ms_cert_len);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_ocall_export_report(void* pms)
{
	ms_ocall_export_report_t* ms = SGX_CAST(ms_ocall_export_report_t*, pms);
	ocall_export_report(ms->ms_report);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxssl_ftime(void* pms)
{
	ms_u_sgxssl_ftime_t* ms = SGX_CAST(ms_u_sgxssl_ftime_t*, pms);
	u_sgxssl_ftime(ms->ms_timeptr, ms->ms_timeb_len);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_sgx_oc_cpuidex(void* pms)
{
	ms_sgx_oc_cpuidex_t* ms = SGX_CAST(ms_sgx_oc_cpuidex_t*, pms);
	sgx_oc_cpuidex(ms->ms_cpuinfo, ms->ms_leaf, ms->ms_subleaf);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_sgx_thread_wait_untrusted_event_ocall(void* pms)
{
	ms_sgx_thread_wait_untrusted_event_ocall_t* ms = SGX_CAST(ms_sgx_thread_wait_untrusted_event_ocall_t*, pms);
	ms->ms_retval = sgx_thread_wait_untrusted_event_ocall(ms->ms_self);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_sgx_thread_set_untrusted_event_ocall(void* pms)
{
	ms_sgx_thread_set_untrusted_event_ocall_t* ms = SGX_CAST(ms_sgx_thread_set_untrusted_event_ocall_t*, pms);
	ms->ms_retval = sgx_thread_set_untrusted_event_ocall(ms->ms_waiter);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_sgx_thread_setwait_untrusted_events_ocall(void* pms)
{
	ms_sgx_thread_setwait_untrusted_events_ocall_t* ms = SGX_CAST(ms_sgx_thread_setwait_untrusted_events_ocall_t*, pms);
	ms->ms_retval = sgx_thread_setwait_untrusted_events_ocall(ms->ms_waiter, ms->ms_self);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_sgx_thread_set_multiple_untrusted_events_ocall(void* pms)
{
	ms_sgx_thread_set_multiple_untrusted_events_ocall_t* ms = SGX_CAST(ms_sgx_thread_set_multiple_untrusted_events_ocall_t*, pms);
	ms->ms_retval = sgx_thread_set_multiple_untrusted_events_ocall(ms->ms_waiters, ms->ms_total);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_exclusive_file_open(void* pms)
{
	ms_u_sgxprotectedfs_exclusive_file_open_t* ms = SGX_CAST(ms_u_sgxprotectedfs_exclusive_file_open_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_exclusive_file_open(ms->ms_filename, ms->ms_read_only, ms->ms_file_size, ms->ms_error_code);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_check_if_file_exists(void* pms)
{
	ms_u_sgxprotectedfs_check_if_file_exists_t* ms = SGX_CAST(ms_u_sgxprotectedfs_check_if_file_exists_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_check_if_file_exists(ms->ms_filename);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_fread_node(void* pms)
{
	ms_u_sgxprotectedfs_fread_node_t* ms = SGX_CAST(ms_u_sgxprotectedfs_fread_node_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_fread_node(ms->ms_f, ms->ms_node_number, ms->ms_buffer, ms->ms_node_size);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_fwrite_node(void* pms)
{
	ms_u_sgxprotectedfs_fwrite_node_t* ms = SGX_CAST(ms_u_sgxprotectedfs_fwrite_node_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_fwrite_node(ms->ms_f, ms->ms_node_number, ms->ms_buffer, ms->ms_node_size);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_fclose(void* pms)
{
	ms_u_sgxprotectedfs_fclose_t* ms = SGX_CAST(ms_u_sgxprotectedfs_fclose_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_fclose(ms->ms_f);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_fflush(void* pms)
{
	ms_u_sgxprotectedfs_fflush_t* ms = SGX_CAST(ms_u_sgxprotectedfs_fflush_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_fflush(ms->ms_f);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_remove(void* pms)
{
	ms_u_sgxprotectedfs_remove_t* ms = SGX_CAST(ms_u_sgxprotectedfs_remove_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_remove(ms->ms_filename);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_recovery_file_open(void* pms)
{
	ms_u_sgxprotectedfs_recovery_file_open_t* ms = SGX_CAST(ms_u_sgxprotectedfs_recovery_file_open_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_recovery_file_open(ms->ms_filename);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_fwrite_recovery_node(void* pms)
{
	ms_u_sgxprotectedfs_fwrite_recovery_node_t* ms = SGX_CAST(ms_u_sgxprotectedfs_fwrite_recovery_node_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_fwrite_recovery_node(ms->ms_f, ms->ms_data, ms->ms_data_length);

	return SGX_SUCCESS;
}

static sgx_status_t SGX_CDECL Enclave_u_sgxprotectedfs_do_file_recovery(void* pms)
{
	ms_u_sgxprotectedfs_do_file_recovery_t* ms = SGX_CAST(ms_u_sgxprotectedfs_do_file_recovery_t*, pms);
	ms->ms_retval = u_sgxprotectedfs_do_file_recovery(ms->ms_filename, ms->ms_recovery_filename, ms->ms_node_size);

	return SGX_SUCCESS;
}

static const struct {
	size_t nr_ocall;
	void * table[22];
} ocall_table_Enclave = {
	22,
	{
		(void*)Enclave_ocall_print_string,
		(void*)Enclave_ocall_get,
		(void*)Enclave_ocall_export_csr,
		(void*)Enclave_ocall_export_pubkey,
		(void*)Enclave_ocall_get_teecert,
		(void*)Enclave_ocall_export_report,
		(void*)Enclave_u_sgxssl_ftime,
		(void*)Enclave_sgx_oc_cpuidex,
		(void*)Enclave_sgx_thread_wait_untrusted_event_ocall,
		(void*)Enclave_sgx_thread_set_untrusted_event_ocall,
		(void*)Enclave_sgx_thread_setwait_untrusted_events_ocall,
		(void*)Enclave_sgx_thread_set_multiple_untrusted_events_ocall,
		(void*)Enclave_u_sgxprotectedfs_exclusive_file_open,
		(void*)Enclave_u_sgxprotectedfs_check_if_file_exists,
		(void*)Enclave_u_sgxprotectedfs_fread_node,
		(void*)Enclave_u_sgxprotectedfs_fwrite_node,
		(void*)Enclave_u_sgxprotectedfs_fclose,
		(void*)Enclave_u_sgxprotectedfs_fflush,
		(void*)Enclave_u_sgxprotectedfs_remove,
		(void*)Enclave_u_sgxprotectedfs_recovery_file_open,
		(void*)Enclave_u_sgxprotectedfs_fwrite_recovery_node,
		(void*)Enclave_u_sgxprotectedfs_do_file_recovery,
	}
};
sgx_status_t sign_data(sgx_enclave_id_t eid, char** retval, char* data, uint32_t data_len)
{
	sgx_status_t status;
	ms_sign_data_t ms;
	ms.ms_data = data;
	ms.ms_data_len = data_len;
	status = sgx_ecall(eid, 0, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_invoke(sgx_enclave_id_t eid, sgx_status_t* retval, char* contract_name, char* version, unsigned char* code_hash, uint32_t hash_len, unsigned char* private_rlp_data, uint32_t data_len, unsigned char* password, uint32_t password_len, uint8_t* byte_code, uint32_t code_len, char* private_req, uint32_t private_req_len, uint32_t is_deployment, evm_result_data_t* evm_result)
{
	sgx_status_t status;
	ms_ecall_invoke_t ms;
	ms.ms_contract_name = contract_name;
	ms.ms_contract_name_len = contract_name ? strlen(contract_name) + 1 : 0;
	ms.ms_version = version;
	ms.ms_version_len = version ? strlen(version) + 1 : 0;
	ms.ms_code_hash = code_hash;
	ms.ms_hash_len = hash_len;
	ms.ms_private_rlp_data = private_rlp_data;
	ms.ms_data_len = data_len;
	ms.ms_password = password;
	ms.ms_password_len = password_len;
	ms.ms_byte_code = byte_code;
	ms.ms_code_len = code_len;
	ms.ms_private_req = private_req;
	ms.ms_private_req_len = private_req_len;
	ms.ms_is_deployment = is_deployment;
	ms.ms_evm_result = evm_result;
	status = sgx_ecall(eid, 1, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_crypt_init(sgx_enclave_id_t eid, int* retval, char* signAlgorithm)
{
	sgx_status_t status;
	ms_ecall_crypt_init_t ms;
	ms.ms_signAlgorithm = signAlgorithm;
	ms.ms_signAlgorithm_len = signAlgorithm ? strlen(signAlgorithm) + 1 : 0;
	status = sgx_ecall(eid, 2, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_get_proof_size(sgx_enclave_id_t eid, int* retval, char* challenge, uint32_t challenge_len, uint32_t* proof_len)
{
	sgx_status_t status;
	ms_ecall_get_proof_size_t ms;
	ms.ms_challenge = challenge;
	ms.ms_challenge_len = challenge_len;
	ms.ms_proof_len = proof_len;
	status = sgx_ecall(eid, 3, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_remote_attestation_prove(sgx_enclave_id_t eid, int* retval, char* challenge, uint32_t challenge_len, char* proof, uint32_t proof_len, uint32_t* actual_proof_len)
{
	sgx_status_t status;
	ms_ecall_remote_attestation_prove_t ms;
	ms.ms_challenge = challenge;
	ms.ms_challenge_len = challenge_len;
	ms.ms_proof = proof;
	ms.ms_proof_len = proof_len;
	ms.ms_actual_proof_len = actual_proof_len;
	status = sgx_ecall(eid, 4, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_get_report_size(sgx_enclave_id_t eid, int* retval, uint32_t* report_len)
{
	sgx_status_t status;
	ms_ecall_get_report_size_t ms;
	ms.ms_report_len = report_len;
	status = sgx_ecall(eid, 5, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

sgx_status_t ecall_get_report(sgx_enclave_id_t eid, int* retval, uint32_t report_len, unsigned char* report)
{
	sgx_status_t status;
	ms_ecall_get_report_t ms;
	ms.ms_report_len = report_len;
	ms.ms_report = report;
	status = sgx_ecall(eid, 6, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

