#ifndef ENCLAVE_U_H__
#define ENCLAVE_U_H__

#include <stdint.h>
#include <wchar.h>
#include <stddef.h>
#include <string.h>

#include "user_types.h"
#include "/opt/intel/sgxsdk/include/sgx_edger8r.h"
#include "/opt/intel/sgxsdk/include/sgx_report.h"
#include "/opt/intel/sgxsdk/include/sgx_utils.h"
#include "/opt/intel/sgxsdk/include/sgx_tcrypto.h"

#include <stdlib.h> /* for size_t */

#define SGX_CAST(type, item) ((type)(item))

#ifdef __cplusplus
extern "C" {
#endif

#ifndef OCALL_PRINT_STRING_DEFINED__
#define OCALL_PRINT_STRING_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, ocall_print_string, (char* str));
#endif
#ifndef OCALL_GET_DEFINED__
#define OCALL_GET_DEFINED__
int SGX_UBRIDGE(SGX_NOCONVENTION, ocall_get, (char* contract_name, char* key, char* result, uint32_t* result_len));
#endif
#ifndef OCALL_EXPORT_CSR_DEFINED__
#define OCALL_EXPORT_CSR_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, ocall_export_csr, (char* csr));
#endif
#ifndef OCALL_EXPORT_PUBKEY_DEFINED__
#define OCALL_EXPORT_PUBKEY_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, ocall_export_pubkey, (char* key));
#endif
#ifndef OCALL_GET_TEECERT_DEFINED__
#define OCALL_GET_TEECERT_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, ocall_get_teecert, (char** cert, uint32_t* cert_len));
#endif
#ifndef OCALL_EXPORT_REPORT_DEFINED__
#define OCALL_EXPORT_REPORT_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, ocall_export_report, (char* report));
#endif
#ifndef U_SGXSSL_FTIME_DEFINED__
#define U_SGXSSL_FTIME_DEFINED__
void SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxssl_ftime, (void* timeptr, uint32_t timeb_len));
#endif
#ifndef SGX_OC_CPUIDEX_DEFINED__
#define SGX_OC_CPUIDEX_DEFINED__
void SGX_UBRIDGE(SGX_CDECL, sgx_oc_cpuidex, (int cpuinfo[4], int leaf, int subleaf));
#endif
#ifndef SGX_THREAD_WAIT_UNTRUSTED_EVENT_OCALL_DEFINED__
#define SGX_THREAD_WAIT_UNTRUSTED_EVENT_OCALL_DEFINED__
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_wait_untrusted_event_ocall, (const void* self));
#endif
#ifndef SGX_THREAD_SET_UNTRUSTED_EVENT_OCALL_DEFINED__
#define SGX_THREAD_SET_UNTRUSTED_EVENT_OCALL_DEFINED__
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_set_untrusted_event_ocall, (const void* waiter));
#endif
#ifndef SGX_THREAD_SETWAIT_UNTRUSTED_EVENTS_OCALL_DEFINED__
#define SGX_THREAD_SETWAIT_UNTRUSTED_EVENTS_OCALL_DEFINED__
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_setwait_untrusted_events_ocall, (const void* waiter, const void* self));
#endif
#ifndef SGX_THREAD_SET_MULTIPLE_UNTRUSTED_EVENTS_OCALL_DEFINED__
#define SGX_THREAD_SET_MULTIPLE_UNTRUSTED_EVENTS_OCALL_DEFINED__
int SGX_UBRIDGE(SGX_CDECL, sgx_thread_set_multiple_untrusted_events_ocall, (const void** waiters, size_t total));
#endif
#ifndef U_SGXPROTECTEDFS_EXCLUSIVE_FILE_OPEN_DEFINED__
#define U_SGXPROTECTEDFS_EXCLUSIVE_FILE_OPEN_DEFINED__
void* SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_exclusive_file_open, (const char* filename, uint8_t read_only, int64_t* file_size, int32_t* error_code));
#endif
#ifndef U_SGXPROTECTEDFS_CHECK_IF_FILE_EXISTS_DEFINED__
#define U_SGXPROTECTEDFS_CHECK_IF_FILE_EXISTS_DEFINED__
uint8_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_check_if_file_exists, (const char* filename));
#endif
#ifndef U_SGXPROTECTEDFS_FREAD_NODE_DEFINED__
#define U_SGXPROTECTEDFS_FREAD_NODE_DEFINED__
int32_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_fread_node, (void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size));
#endif
#ifndef U_SGXPROTECTEDFS_FWRITE_NODE_DEFINED__
#define U_SGXPROTECTEDFS_FWRITE_NODE_DEFINED__
int32_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_fwrite_node, (void* f, uint64_t node_number, uint8_t* buffer, uint32_t node_size));
#endif
#ifndef U_SGXPROTECTEDFS_FCLOSE_DEFINED__
#define U_SGXPROTECTEDFS_FCLOSE_DEFINED__
int32_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_fclose, (void* f));
#endif
#ifndef U_SGXPROTECTEDFS_FFLUSH_DEFINED__
#define U_SGXPROTECTEDFS_FFLUSH_DEFINED__
uint8_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_fflush, (void* f));
#endif
#ifndef U_SGXPROTECTEDFS_REMOVE_DEFINED__
#define U_SGXPROTECTEDFS_REMOVE_DEFINED__
int32_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_remove, (const char* filename));
#endif
#ifndef U_SGXPROTECTEDFS_RECOVERY_FILE_OPEN_DEFINED__
#define U_SGXPROTECTEDFS_RECOVERY_FILE_OPEN_DEFINED__
void* SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_recovery_file_open, (const char* filename));
#endif
#ifndef U_SGXPROTECTEDFS_FWRITE_RECOVERY_NODE_DEFINED__
#define U_SGXPROTECTEDFS_FWRITE_RECOVERY_NODE_DEFINED__
uint8_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_fwrite_recovery_node, (void* f, uint8_t* data, uint32_t data_length));
#endif
#ifndef U_SGXPROTECTEDFS_DO_FILE_RECOVERY_DEFINED__
#define U_SGXPROTECTEDFS_DO_FILE_RECOVERY_DEFINED__
int32_t SGX_UBRIDGE(SGX_NOCONVENTION, u_sgxprotectedfs_do_file_recovery, (const char* filename, const char* recovery_filename, uint32_t node_size));
#endif

sgx_status_t sign_data(sgx_enclave_id_t eid, char** retval, char* data, uint32_t data_len);
sgx_status_t ecall_invoke(sgx_enclave_id_t eid, sgx_status_t* retval, char* contract_name, char* version, unsigned char* code_hash, uint32_t hash_len, unsigned char* private_rlp_data, uint32_t data_len, unsigned char* password, uint32_t password_len, uint8_t* byte_code, uint32_t code_len, char* private_req, uint32_t private_req_len, uint32_t is_deployment, evm_result_data_t* evm_result);
sgx_status_t ecall_crypt_init(sgx_enclave_id_t eid, int* retval, char* signAlgorithm);
sgx_status_t ecall_get_proof_size(sgx_enclave_id_t eid, int* retval, char* challenge, uint32_t challenge_len, uint32_t* proof_len);
sgx_status_t ecall_remote_attestation_prove(sgx_enclave_id_t eid, int* retval, char* challenge, uint32_t challenge_len, char* proof, uint32_t proof_len, uint32_t* actual_proof_len);
sgx_status_t ecall_get_report_size(sgx_enclave_id_t eid, int* retval, uint32_t* report_len);
sgx_status_t ecall_get_report(sgx_enclave_id_t eid, int* retval, uint32_t report_len, unsigned char* report);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
