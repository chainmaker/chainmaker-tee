/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package bridge

import "C"
import (
	"encoding/hex"
	"fmt"
	"gateway/cmclient"
	"gateway/loggers"
	"io/ioutil"
	"runtime"
	"runtime/debug"
	"unsafe"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

//#cgo CFLAGS: -I/opt/intel/sgxsdk/include -I.
//#include <stdio.h>
//#include <stdlib.h>
//#include <stddef.h>
//#include "/opt/intel/sgxsdk/include/sgx_error.h"
//#include "/opt/intel/sgxsdk/include/sgx_eid.h"
//#include "/opt/intel/sgxsdk/include/sgx_urts.h"
//#include "/opt/intel/sgxsdk/include/sgx_edger8r.h"
//#include "/opt/intel/sgxsdk/include/sgx_uae_service.h"
//#cgo LDFLAGS: -L/home/sgx/sgx-intsall/linux-sgx/psw/uae_service/linux -lsgx_uae_service
//#cgo LDFLAGS: -L/opt/intel/sgxsdk/lib64 -L../Enclave -lsgx_urts
//#cgo LDFLAGS: -L../../Enclave/openssl/lib64 -lsgx_usgxssl -lsgx_uprotected_fs
//#include "./Enclave_u.h"
import "C"

type SgxEnclaveId C.sgx_enclave_id_t
type EvmResultData C.evm_result_data_t
type EvmResult struct {
	code   int32
	result []byte
}

type EnclaveResult struct {
	ContractName string
	Version      string
	CodeHash     []byte
	Code         int32
	Result       []byte
	CodeHeader   []byte
	Gas          int32
	RwSet        *common.TxRWSet
	ReportHash   []byte
	Sign         []byte
	PrivateReq   []byte
}

var (
	EnclaveFileName              = "../Enclave/enclave.signed.so"
	TeeCertSignAlg               = "RSA"
	TeePubKeyFile                = "./out_pubkey.pem"
	TeeCsrFile                   = "./out_csr.pem"
	TeeCertFile                  = "./in_teecert.pem"
	TeeReportFile                = "./out_report.dat"
	SgxDebugFlag                 = 1
	GlobalEid       SgxEnclaveId = 0
)

func InitializeEnclave() int {
	var ret C.sgx_status_t
	var iRet C.int
	var lanchToken = C.sgx_launch_token_t{0}
	var update C.int = 0
	//var targetInfo C.sgx_target_info_t
	//var report C.sgx_report_t
	//var quoteSize C.uint32_t = 0
	//var quote *C.sgx_quote_t
	//var p_spid C.sgx_spid_t
	//var p_gid C.sgx_epid_group_id_t
	//var linkable C.sgx_quote_sign_type_t = C.SGX_UNLINKABLE_SIGNATURE
	ret = C.sgx_create_enclave((*C.char)(C.CString(EnclaveFileName)),
		(C.int)(1),
		(*C.sgx_launch_token_t)(&lanchToken),
		(*C.int)(&update),
		(*C.sgx_enclave_id_t)(&GlobalEid),
		(*C.sgx_misc_attribute_t)(C.NULL))
	if ret != C.SGX_SUCCESS {
		loggers.Info(ret)
		return -1
	}

	C.ecall_crypt_init((C.sgx_enclave_id_t)(GlobalEid), (*C.int)(&iRet), (*C.char)(C.CString(TeeCertSignAlg)))
	if iRet != 0 {
		loggers.Info("ecall_crypt_init ret = ", iRet)
		return -1
	}
	loggers.Info("ecall_crypt_init ret = ", iRet)
	return 0
}

func EcallInvoke(globalEid SgxEnclaveId, contractName, version string, codeHash, privateRlpData, password,
	byteCodes []byte, privateReq []byte, isDeployment bool) (enclaveResult *EnclaveResult, err error) {

	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case runtime.Error:
				fmt.Println("runtime error:", err)
			default:
				fmt.Println("error:", err)
			}
			debug.PrintStack()
		}
	}()

	var instanceOfEvmResultData C.evm_result_data_t
	evmResultData := (*C.evm_result_data_t)(C.malloc((C.ulong)(unsafe.Sizeof(instanceOfEvmResultData))))
	if evmResultData == nil {
		loggers.Error("malloc evm result failed")
		return nil, fmt.Errorf("malloc evm result failed")
	}
	C.memset(unsafe.Pointer(evmResultData), 0, (C.ulong)(unsafe.Sizeof(instanceOfEvmResultData)))
	//fmt.Printf("evm result temp address: %p\n", evmResultData)
	defer C.free(unsafe.Pointer(evmResultData))
	csContractName := C.CString(contractName)
	defer C.free(unsafe.Pointer(csContractName))
	csVersion := C.CString(version)
	defer C.free(unsafe.Pointer(csVersion))
	cbCodeHash := C.CBytes(codeHash)
	defer C.free(unsafe.Pointer(cbCodeHash))
	cbData := C.CBytes(privateRlpData)
	defer C.free(unsafe.Pointer(cbData))
	cbPassword := C.CBytes(password)
	defer C.free(unsafe.Pointer(cbPassword))
	cbCode := C.CBytes(byteCodes)
	defer C.free(cbCode)
	cbPrivateReq := C.CBytes(privateReq)
	defer C.free(unsafe.Pointer(cbPrivateReq))

	deployment := 0
	if isDeployment {
		deployment = 1
	}
	loggers.Debugf("original rlp data: %s, before call sgx", hex.EncodeToString(privateRlpData))
	var ret C.sgx_status_t
	retval := C.ecall_invoke((C.ulong)(globalEid), &ret, (*C.char)(csContractName), (*C.char)(csVersion),
		(*C.uchar)(cbCodeHash), (C.uint32_t)(len(codeHash)),
		(*C.uchar)(cbData), (C.uint32_t)(len(privateRlpData)),
		(*C.uchar)(cbPassword), (C.uint32_t)(len(password)),
		(*C.uint8_t)(cbCode), (C.uint32_t)(len(byteCodes)),
		(*C.char)(cbPrivateReq), (C.uint32_t)(len(privateReq)),
		(C.uint32_t)(deployment),
		evmResultData)
	if retval != C.SGX_SUCCESS || ret != C.SGX_SUCCESS {
		loggers.Errorf("contract exec failed, invoke code: %d, enclave code: %d", retval, ret)
		return nil, fmt.Errorf("contract exec failed, invoke code: %v, enclave code: %v", retval, ret)
	}
	loggers.Debugf("original rlp data: %s, ecall invoke ret: %d",
		hex.EncodeToString(privateRlpData), ret)
	result := make([]byte, evmResultData.result_len)
	for i := uint32(0); i < uint32(evmResultData.result_len); i++ {
		result[i] = byte(evmResultData.result[i])
	}
	rwSet := &common.TxRWSet{
		TxId:     "",
		TxReads:  make([]*common.TxRead, evmResultData.rset_pos),
		TxWrites: make([]*common.TxWrite, evmResultData.wset_pos),
	}
	for i := uint32(0); i < uint32(evmResultData.rset_pos); i++ {
		rset := &evmResultData.rsets[i]
		key := make([]byte, rset.key_len)
		for j := uint32(0); j < uint32(rset.key_len); j++ {
			key[j] = byte(rset.key[j])
		}
		value := make([]byte, rset.value_len)
		for j := uint32(0); j < uint32(rset.value_len); j++ {
			value[j] = byte(rset.value[j])
		}
		//version := make([]byte, rset.version_len)
		//for j := 0; j < rset.version_len; j++ {
		//	version[j] = rset.version_len[j]
		//}
		rwSet.TxReads[i] = &common.TxRead{
			Key:          key,
			Value:        value,
			ContractName: contractName,
			Version:      nil,
		}
	}
	for i := uint32(0); i < uint32(evmResultData.wset_pos); i++ {
		wset := &evmResultData.wsets[i]
		key := make([]byte, wset.key_len)
		for j := uint32(0); j < uint32(wset.key_len); j++ {
			key[j] = byte(wset.key[j])
		}
		value := make([]byte, wset.value_len)
		for j := uint32(0); j < uint32(wset.value_len); j++ {
			value[j] = byte(wset.value[j])
		}
		rwSet.TxWrites[i] = &common.TxWrite{
			Key:          key,
			Value:        value,
			ContractName: contractName,
		}
	}

	loggers.Debugf("original rlp data: %s, result sig len: %d",
		hex.EncodeToString(privateRlpData), evmResultData.sig_len)
	sign := make([]byte, evmResultData.sig_len)
	for i := uint32(0); i < uint32(evmResultData.sig_len); i++ {
		sign[i] = byte(evmResultData.sig[i])
	}

	resultContractName := make([]byte, evmResultData.name_len)
	for i := uint32(0); i < uint32(evmResultData.name_len); i++ {
		resultContractName[i] = byte(evmResultData.contract_name[i])
	}

	resultCodeVersion := make([]byte, evmResultData.version_len)
	for i := uint32(0); i < uint32(evmResultData.version_len); i++ {
		resultCodeVersion[i] = byte(evmResultData.code_version[i])
	}

	resultCodeHash := make([]byte, evmResultData.code_hash_len)
	for i := uint32(0); i < uint32(evmResultData.code_hash_len); i++ {
		resultCodeHash[i] = byte(evmResultData.code_hash[i])
	}
	resultReportHash := make([]byte, evmResultData.report_hash_len)
	for i := uint32(0); i < uint32(evmResultData.report_hash_len); i++ {
		resultReportHash[i] = byte(evmResultData.report_hash[i])
	}

	codeHeader := make([]byte, evmResultData.code_header_len)
	for i := uint32(0); i < uint32(evmResultData.code_header_len); i++ {
		codeHeader[i] = byte(evmResultData.code_header[i])
	}

	resultPrivateReq := make([]byte, evmResultData.private_req_len)
	for i := uint32(0); i < uint32(evmResultData.private_req_len); i++ {
		resultPrivateReq[i] = byte(evmResultData.private_req[i])
	}

	return &EnclaveResult{
		ContractName: string(resultContractName),
		Version:      string(resultCodeVersion),
		CodeHash:     resultCodeHash,
		Code:         int32(evmResultData.code),
		Result:       result,
		Gas:          int32(evmResultData.gas),
		RwSet:        rwSet,
		ReportHash:   resultReportHash,
		Sign:         sign,
		PrivateReq:   resultPrivateReq,
		CodeHeader:   codeHeader,
	}, nil
}

func EcallRemoteAttestationProve(globalEid SgxEnclaveId, challenge string) ([]byte, error) {

	csChallenge := C.CString(challenge)
	defer C.free(unsafe.Pointer(csChallenge))
	var cProofLen C.uint32_t
	var cActualProofLen C.uint32_t

	var retval C.int32_t
	var ret C.sgx_status_t
	ret = C.ecall_get_proof_size(
		(C.ulong)(globalEid), &retval,
		(*C.char)(csChallenge), (C.uint32_t)(len(challenge)),
		(*C.uint32_t)(&cProofLen))
	if ret != C.SGX_SUCCESS {
		loggers.Infof("C.ecall_get_proof_size failed. ret = %v \n", ret)
		return nil, fmt.Errorf("call ecall_get_proof_size() failed, ret: %v", ret)
	}
	loggers.Infof("proof size = %v \n", cProofLen)

	cProof := C.malloc((C.ulong)(cProofLen))
	if cProof == nil {
		loggers.Error("malloc proof memory failed")
		return nil, fmt.Errorf("malloc proof memory failed")
	}
	loggers.Infof("C.malloc %d bytes for proof data.\n", cProofLen)
	ret = C.ecall_remote_attestation_prove(
		(C.ulong)(globalEid), &retval,
		(*C.char)(csChallenge), (C.uint32_t)(len(challenge)),
		(*C.char)(cProof), (C.uint32_t)(cProofLen), (*C.uint32_t)(&cActualProofLen))
	if ret != C.SGX_SUCCESS {
		loggers.Infof("C.ecall_remote_attestation_prove() failed, ret = %v \n", ret)
		return nil, fmt.Errorf("call ecall_remote_attestation_prove() failed, ret: %v", ret)
	}

	loggers.Info("EcallRemoteAttestationProve() finished.")
	return C.GoBytes(cProof, (C.int32_t)(cProofLen)), nil
}

func SgxDestroyEnclave(globalEid SgxEnclaveId) {
	C.sgx_destroy_enclave((C.ulong)(globalEid))
}

//export ocall_get
func ocall_get(contractName, key *C.char, result *C.char, result_len *C.uint32_t) C.int {
	rspData, err := cmclient.CmC.GetData(C.GoString(contractName), C.GoString(key))
	loggers.Debugf("get data from chain resp: %v", rspData)
	if err != nil {
		loggers.Errorf("ocall get failed err is:%v", err.Error())
		return -1
	}
	if len(rspData) > 32 {
		loggers.Errorf("ocall get value too long, max len: 32, data len: %d", len(rspData))
		return -1
	}
	resultData := C.CBytes(rspData)
	defer C.free(unsafe.Pointer(resultData))

	C.memcpy(unsafe.Pointer(result), resultData, C.ulong(len(rspData)))
	*result_len = (C.uint32_t)(len(rspData))
	return 0
}

//export ocall_print_string
func ocall_print_string(str *C.char) {
	//fmt.Println("ocall print string: ", C.GoString(str))
	loggers.Debugf("ocall print string: %v", C.GoString(str))
}

//export ocall_export_csr
func ocall_export_csr(csr *C.char) {
	loggers.Info("in ocall_export_csr:")
	if err := ioutil.WriteFile(TeeCsrFile, []byte(C.GoString(csr)), 0664); err != nil {
		loggers.Info("Writefile Error =", err)
	}
}

//export ocall_export_pubkey
func ocall_export_pubkey(key *C.char) {
	loggers.Info("in ocall_export_pubkey:")
	_, err := ioutil.ReadFile(TeePubKeyFile)
	if err != nil {
		ioutil.WriteFile(TeePubKeyFile, []byte(C.GoString(key)), 0664)
	}
}

//export ocall_export_report
func ocall_export_report(report *C.char) {
	loggers.Info("in ocall_export_report:")
	if err := ioutil.WriteFile(TeeReportFile, []byte(C.GoString(report)), 0664); err != nil {
		loggers.Info("Writefile Error =", err)
		return
	}
}

//export ocall_get_teecert
func ocall_get_teecert(cert **C.char, cert_len *C.uint32_t) {
	loggers.Info("in ocall_get_teecert")
	data, err := ioutil.ReadFile(TeeCertFile)
	if err != nil {
		loggers.Info("no tee cert configured")
		*cert = nil
		*cert_len = 0
		return
	}
	*cert = (*C.char)(C.CBytes(data))
	*cert_len = (C.uint32_t)(len(data))
	return
}
