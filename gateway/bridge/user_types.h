/*
 * Copyright (C) 2011-2020 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


/* User defined types */

typedef void *buffer_t;
typedef int array_t[10];

#define MAX_RESULT_LENGTH 0x100000
#define MAX_CODE_HEADER_LENGTH 4096
#define MAX_DECODED_RLP_DATA_LENGTH 4096
#define MAX_RSET_LENGTH 64
#define MAX_WSET_LENGTH 64
#define MAX_KEY_LENGTH 64
#define MAX_VALUE_LENGTH 64
#define MAX_SIG_LENGTH 1024
#define MAX_NAME_LENGTH 64
#define MAX_VERSION_LENGTH 64
#define MAX_HASH_LENGTH 64
#define MAX_PRIVATE_REQ_LENGTH 32768

typedef struct rset {
    uint32_t key_len;
    char key[MAX_KEY_LENGTH];
    uint32_t value_len;
    char value[MAX_VALUE_LENGTH];
    uint32_t version_len;
    char version[MAX_VERSION_LENGTH];
} RSet;

typedef struct wset {
    char key[MAX_KEY_LENGTH];
    uint32_t key_len;
    char value[MAX_VALUE_LENGTH];
    uint32_t value_len;
} WSet;

typedef struct evm_result_data {
    int32_t code;
    uint32_t result_len;
    char result[MAX_RESULT_LENGTH];
    int64_t gas;
    uint32_t rset_pos;
    RSet rsets[MAX_RSET_LENGTH];
    uint32_t wset_pos;
    WSet wsets[MAX_WSET_LENGTH];
    uint32_t name_len;
    char contract_name[MAX_NAME_LENGTH];
    uint32_t version_len;
    char code_version[MAX_VERSION_LENGTH];
    uint32_t code_hash_len;
    char code_hash[MAX_HASH_LENGTH];
    uint32_t report_hash_len;
    char report_hash[MAX_HASH_LENGTH];
    uint32_t private_req_len;
    char private_req[MAX_PRIVATE_REQ_LENGTH];
    uint32_t code_header_len;
    char code_header[MAX_CODE_HEADER_LENGTH];
    uint32_t sig_len;
    char sig[MAX_SIG_LENGTH];
} evm_result_data_t;
