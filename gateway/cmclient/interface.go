/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmclient

import "chainmaker.org/chainmaker/pb-go/v2/common"

type Client interface {

	// ### 11.2 隐私目录上链
	// ```go
	SaveDir(orderId, txId string, privateDir *common.StrSlice, withSyncResult bool, timeout int64) (*common.TxResponse, error)
	//```

	// ### 11.3 隐私合约代码查询
	// ```go
	GetContract(contractName, codeHash string) (*common.PrivateGetContract, error)
	//```

	// ### 11.4 隐私计算结果上链
	// ```go
	SaveData(contractName, version string, codeHash, reportHash []byte, result *common.ContractResult, txId string,
		rwSet *common.TxRWSet, reportSign []byte, events *common.StrSlice, privateReq []byte, withSyncResult bool, timeout int64) (*common.TxResponse, error)
	//```

	// ### 11.5 隐私计算结果查询
	// ```go
	GetData(contractName, key string) ([]byte, error)
	//```

	// ### 11.9 隐私计算隐私目录查询
	// ```go
	GetDir(orderId string) ([]byte, error)
	// ```

	// ###  11.11 隐私计算调用者权限验证
	// ```go
	CheckCallerCertAuth(privateComputeRequest string) (*common.TxResponse, error)
	// ```

	SaveRemoteAttestationProof(proof, txId string, withSyncResult bool, timeout int64) (*common.TxResponse, error)

	GetEnclaveReport(enclaveId string) ([]byte, error)

	GetEnclaveCACert() ([]byte, error)

	GetEnclaveProof(enclaveId string) ([]byte, error)
}
