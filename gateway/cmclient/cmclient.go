/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package cmclient

import (
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"gateway/config"
)

var CmC *CmClient

type CmClient struct {
	chainClient *sdk.ChainClient
}

func InitCmClient() error {
	//初始化配置
	CmC = &CmClient{}

	var err error
	CmC.chainClient, err = sdk.NewChainClient(
		// 设置归属组织
		sdk.WithChainClientOrgId(config.CmcConfig.OrgId),
		// 设置链ID
		sdk.WithChainClientChainId(config.CmcConfig.ChainId),
		// 设置客户端用户私钥路径
		sdk.WithUserKeyFilePath(config.CmcConfig.UserKeyPath),
		// 设置客户端用户证书
		sdk.WithUserCrtFilePath(config.CmcConfig.UserCttPath),
		// 添加节点1
		sdk.AddChainClientNodeConfig(getNodeConfig()),
	)

	if err != nil {
		return err
	}

	//启用证书压缩（开启证书压缩可以减小交易包大小，提升处理性能）
	if err = CmC.chainClient.EnableCertHash(); err != nil {
		return err
	}

	return nil
}

func getNodeConfig() *sdk.NodeConfig {
	return sdk.NewNodeConfig(
		// 节点地址，格式：127.0.0.1:12301
		sdk.WithNodeAddr(config.CmcConfig.NodeAddr),
		// 节点连接数
		sdk.WithNodeConnCnt(config.CmcConfig.ConnCnt),
		// 节点是否启用TLS认证
		sdk.WithNodeUseTLS(true),
		// 根证书路径，支持多个
		sdk.WithNodeCAPaths(config.CmcConfig.CaPaths),
		// TLS Hostname
		sdk.WithNodeTLSHostName(config.CmcConfig.TlsHostName),
	)
}

//SaveDir
func (c *CmClient) SaveDir(orderId, txId string, privateDir *common.StrSlice, withSyncResult bool, timeout int64) (*common.TxResponse, error) {
	return c.chainClient.SaveDir(orderId, txId, privateDir, withSyncResult, timeout)
}

//GetContract
func (c *CmClient) GetContract(contractName, codeHash string) (*common.PrivateGetContract, error) {
	return c.chainClient.GetContract(contractName, codeHash)
}

//SaveData
func (c *CmClient) SaveData(contractName, version string, isDeployment bool, codeHash, reportHash []byte,
	result *common.ContractResult, codeHeader []byte, txId string, rwSet *common.TxRWSet, sign []byte,
	events *common.StrSlice, privateReq []byte, withSyncResult bool, timeout int64) (*common.TxResponse, error) {
	return c.chainClient.SaveData(contractName, version, isDeployment, codeHash, reportHash, result, codeHeader, txId,
		rwSet, sign, events, privateReq, withSyncResult, timeout)
}

//GetData
func (c *CmClient) GetData(contractName, key string) ([]byte, error) {
	return c.chainClient.GetData(contractName, key)
}

//GetDir
func (c *CmClient) GetDir(orderId string) ([]byte, error) {
	return c.chainClient.GetDir(orderId)
}

//CheckCallerCertAuth
//func (c *CmClient) CheckCallerCertAuth(privateComputeRequest string) (*common.TxResponse, error) {
//	return c.chainClient.CheckCallerCertAuth(privateComputeRequest)
//}

func (c *CmClient) CheckCallerCertAuth(payload string, orgIds []string, signPairs []*syscontract.SignInfo) (
	*common.TxResponse, error) {
	return c.chainClient.CheckCallerCertAuth(payload, orgIds, signPairs)
}

func (c *CmClient) SaveRemoteAttestationProof(proof, txId string, withSyncResult bool, timeout int64) (*common.TxResponse, error) {
	return c.chainClient.SaveRemoteAttestationProof(proof, txId, withSyncResult, timeout)
}

func (c *CmClient) GetEnclaveReport(enclaveId string) ([]byte, error) {
	return c.chainClient.GetEnclaveReport(enclaveId)
}

func (c *CmClient) GetEnclaveCACert() ([]byte, error) {
	return c.chainClient.GetEnclaveCACert()
}

func (c *CmClient) GetEnclaveProof(enclaveId string) ([]byte, error) {
	return c.chainClient.GetEnclaveProof(enclaveId)
}
