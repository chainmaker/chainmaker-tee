/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package config

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"github.com/spf13/viper"
)

var ApplicationConfig = new(Application)
var CmcConfig = new(Config)
var SslConfig = new(Ssl)

type Application struct {
	ReadTimeout   int
	WriterTimeout int
	Host          string
	Port          string
	Name          string
	Domain        string
	IsHttps       bool
	Concurrency   int
}

type Ssl struct {
	KeyStr string
	Pem    string
}

type Config struct {
	NodeAddr              string
	ConnCnt               int
	CaPaths               []string
	TlsHostName           string
	OrgId                 string
	ChainId               string
	UserKeyPath           string
	UserCttPath           string
	PrivateSystemContract string
	ValidTime             int64
}

func InitSsl(cfg *viper.Viper) *Ssl {
	return &Ssl{
		KeyStr: cfg.GetString("key"),
		Pem:    cfg.GetString("pem"),
	}
}

func InitApplication(cfg *viper.Viper) *Application {
	return &Application{
		ReadTimeout:   cfg.GetInt("readTimeout"),
		WriterTimeout: cfg.GetInt("writerTimeout"),
		Host:          cfg.GetString("host"),
		Port:          portDefault(cfg),
		Name:          cfg.GetString("name"),
		Domain:        cfg.GetString("domain"),
		IsHttps:       cfg.GetBool("ishttps"),
		Concurrency:   cfg.GetInt("concurrency"),
	}
}

func portDefault(cfg *viper.Viper) string {
	port := cfg.GetString("port")
	if port == "" {
		return "8000"
	}
	return port
}

func InitConfig(cfg *viper.Viper) *Config {
	return &Config{
		NodeAddr:              cfg.GetString("nodeaddr"),
		ConnCnt:               cfg.GetInt("conncnt"),
		CaPaths:               cfg.GetStringSlice("capaths"),
		TlsHostName:           cfg.GetString("tlshostname"),
		OrgId:                 cfg.GetString("orgid"),
		ChainId:               cfg.GetString("chainid"),
		UserKeyPath:           cfg.GetString("userkeypath"),
		UserCttPath:           cfg.GetString("usercttpath"),
		PrivateSystemContract: syscontract.SystemContract_PRIVATE_COMPUTE.String(),
		ValidTime:             cfg.GetInt64("validtime"),
	}
}

var CfgConfig *viper.Viper
var cfgApplication *viper.Viper
var cfgSsl *viper.Viper

// load config
func LoadConfig(path string) {
	viper.SetConfigFile(path)
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(fmt.Sprintf("Read config file fail: %s", err.Error()))
	}

	// Replace environment variables
	err = viper.ReadConfig(strings.NewReader(os.ExpandEnv(string(content))))
	if err != nil {
		log.Fatal(fmt.Sprintf("Parse config file fail: %s", err.Error()))
	}

	// cmClient init
	CfgConfig = viper.Sub("settings.config")
	if CfgConfig == nil {
		panic("config not found settings.config")
	}

	CmcConfig = InitConfig(CfgConfig)

	// server start params
	cfgApplication = viper.Sub("settings.application")
	if cfgApplication == nil {
		panic("config not found settings.application")
	}
	ApplicationConfig = InitApplication(cfgApplication)

	// ssl config
	cfgSsl = viper.Sub("settings.ssl")
	if cfgSsl == nil {
		panic("config not found settings.ssl")
	}

	SslConfig = InitSsl(cfgSsl)
}
