/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"errors"
	"fmt"
	"gateway/bridge"
	"gateway/cmd"
	"gateway/loggers"
	"gateway/verify"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var mainCmd = &cobra.Command{
	Use:               "help",
	Short:             "-h",
	SilenceUsage:      true,
	DisableAutoGenTag: true,
	Long:              `sgx gateway usage`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("require at least one arg")
		}
		return nil
	},
	PersistentPreRunE: func(*cobra.Command, []string) error { return nil },
	Run: func(cmd *cobra.Command, args []string) {
		usageStr := `welcome sgx gateway，you can user -h to show command`
		log.Printf("%s\n", usageStr)
	},
}

func main() {
    //load config
	cmd.PreServer()

	if bridge.InitializeEnclave() < 0 {
		fmt.Println("InitializeEnclave failed")
		return
	}
	defer bridge.SgxDestroyEnclave(bridge.GlobalEid)

	if err := verify.EnclaveVerify(); err != nil {
		loggers.Error(err.Error())
		fmt.Println(err)
		os.Exit(-1)
	}

	mainCmd.AddCommand(cmd.ServerCmd)
	if err := mainCmd.Execute(); err != nil {
		os.Exit(-1)
	}
}
