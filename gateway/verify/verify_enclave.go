/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package verify

import (
	"chainmaker.org/chainmaker/common/v2/crypto/tee"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/sdk-go/v2/utils"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"gateway/bridge"
	"gateway/cmclient"
	"gateway/loggers"
	"math/rand"
	"time"
)

func EnclaveVerify() error {

	// issue remote attestation
	challenge := fmt.Sprintf("This is a challenge message, choose rand float %v.", rand.Float64())
	proof, err := bridge.EcallRemoteAttestationProve(bridge.GlobalEid, challenge)
	if err != nil {
		return fmt.Errorf("bridge.EcallRemoteAttestationProve(...) error: %v", err)
	}
	//fmt.Printf("\nremote attestation proof ==> [%x]\n", proof)

	// get some data(report, ca_cert, proof) from blockchain
	reportFromChainHex, err := cmclient.CmC.GetEnclaveReport("global_enclave_id")
	if err != nil {
		return fmt.Errorf("get enclave report from chain error: %v", err)
	}
	reportFromChain, err := hex.DecodeString(string(reportFromChainHex))
	if err != nil {
		return fmt.Errorf("decode enclave report from chain error: %v", err)
	}
	//fmt.Printf("\nenclave report ==> [%x] \n", reportFromChain)

	caCertPem, err := cmclient.CmC.GetEnclaveCACert()
	if err != nil {
		return fmt.Errorf("get enclave ca cert from chain error: %v", err)
	}
	//fmt.Printf("\nenclave ca cert ==> [%x] \n", caCertPem)

	proofFromChainHex, err := cmclient.CmC.GetEnclaveProof("global_enclave_id")
	if err != nil {
		return fmt.Errorf("get enclave proof from chain error: %v", err)
	}
	proofFromChain, err := hex.DecodeString(string(proofFromChainHex))
	if err != nil {
		return fmt.Errorf("decode enclave proof from chain error: %v", err)
	}
	//fmt.Printf("\nenclave proof from chain  ==> [%x] \n", proofFromChain)

	// verify remote attestation proof
	caCertBlock, _ := pem.Decode(caCertPem)
	if caCertBlock == nil {
		return fmt.Errorf("decode enclave 'ca_cert' from pem format error: %v", err)
	}
	caCert, err := bcx509.ParseCertificate(caCertBlock.Bytes)
	if err != nil {
		return fmt.Errorf("parse enclave 'ca_cert' error: %v", err)
	}

	intermediateCAPool := bcx509.NewCertPool()
	intermediateCAPool.AddCert(caCert)
	verifyOptions := bcx509.VerifyOptions{
		DNSName:                   "",
		Roots:                     intermediateCAPool,
		CurrentTime:               time.Time{},
		KeyUsages:                 []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
		MaxConstraintComparisions: 0,
	}

	loggers.Info("begin verify remote attestation proof ==>")
	passed, _, err := tee.AttestationVerify(proof, verifyOptions, reportFromChain)
	if err != nil {
		return fmt.Errorf("verify remote attestation proof error: %v", err)
	}
	loggers.Info("finished verify remote attestation proof.")
	loggers.Infof("proof => %x\n", proof)

	if !passed {
		return errors.New("verify remote attestation proof failed")
	}

	loggers.Infof("proofFromChain = '%x' \n", proofFromChain)
	if proofFromChain == nil || len(proofFromChain) == 0 {
		resp, err := cmclient.CmC.SaveRemoteAttestationProof(hex.EncodeToString(proof), utils.GetRandTxId(), true, 10)
		if err != nil {
			return fmt.Errorf("save remote attestation proof error: %v", err)
		}
		if resp.Code != common.TxStatusCode_SUCCESS {
			return fmt.Errorf("save remote attestation proof error: %v", resp.Message)
		}
		if resp.ContractResult.Code != 0 {//0表示成功
			return fmt.Errorf("save remote attestation proof error: %v", resp.ContractResult.Message)
		}
		loggers.Info("save remote attestation proof into chain.")
	}
	loggers.Info("verify enlave procedure finished.")

	return nil
}
