/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package controller

import (
    "gateway/service"
    "github.com/gin-gonic/gin"
)

func Compute(c *gin.Context) {
    service.Compute(c)
}

func Deploy(c *gin.Context) {
    service.Deploy(c)
}

func Healthy(c *gin.Context) {
    service.Healthy(c)
}

func RemoteAttestation(c *gin.Context)  {
    service.RemoteAttestation(c)
}