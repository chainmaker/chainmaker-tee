/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

//import (
//    "chainmaker.org/chainmaker/pb-go/v2/common"
//    "encoding/hex"
//    "encoding/json"
//    "gateway/cmclient"
//    "io/ioutil"
//    "testing"
//)
//
//func TestDeploy(t *testing.T) {
//    codeBytes, err := ioutil.ReadFile("../tools/contract_with_string")
//    if err != nil {
//        t.Error(err.Error())
//    }
//    type args struct {
//        req *common.PrivateDeployRequest
//    }
//
//    if err := cmclient.InitCmClient(); err != nil {
//        panic(err)
//    }
//
//    tests := []struct {
//        name string
//        args args
//        want string
//    }{
//        {
//            name: "test1",
//            args: args{req: &common.PrivateDeployRequest{
//                Payload: &common.DeployPayload {
//                    CodeBytes:      hex.EncodeToString(codeBytes),
//                    PrivateRlpData: "",
//                    Passwd:         "",
//                    SigAlgo:        "",
//                    ContractName:   "",
//                    CodeHash:       "",
//                    OrgId:          []string{"wx-org1.chainmaker.org"},
//                },
//            },
//            },
//            want: "",
//        },
//    }
//
//    for _, tt := range tests {
//        t.Run(tt.name, func(t *testing.T) {
//            payLoadBytes, err := json.Marshal(tt.args.req.Payload)
//            if err != nil {
//                t.Error(err)
//                return
//            }
//
//            req, err := buildReq(tt.args.req.Payload.OrgId, payLoadBytes)
//            if err != nil {
//                t.Error(err)
//                return
//            }
//
//            txResponse, err := cmclient.CmC.CheckCallerCertAuth(tt.args.req.Cert, string(req.Signature), string(payLoadBytes), tt.args.req.PayLoad.OrgId)
//            if err != nil {
//                t.Error(err)
//                return
//            }
//
//            if txResponse.ContractResult == nil {
//                t.Error(txResponse.Message)
//            }
//
//            if len(txResponse.ContractResult.Message) != len(tt.want) {
//                t.Error("not passed")
//            }
//        })
//    }
//}
