/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"gateway/bridge"
	"gateway/cmclient"
	"gateway/config"
	"gateway/loggers"
	"gateway/modules"
	"github.com/gin-gonic/gin"
	"strconv"
	"time"
)

func Compute(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			modules.FailResponse(c, err.(error).Error())
		}
	}()
	//privateComputeReq := &modules.PrivateComputeRequest{}

	privateComputeReq := &syscontract.PrivateComputeRequest{}
	if err := c.ShouldBind(privateComputeReq); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	if err := verifyComputeRequest(privateComputeReq); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	payLoadBytes, err := privateComputeReq.Payload.Marshal()
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	//verify user client
	if err = multiVerifyUserClient(privateComputeReq.SignPair, payLoadBytes); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	//verify gateway client
	checkResult, err := cmclient.CmC.CheckCallerCertAuth(hex.EncodeToString(payLoadBytes),
		privateComputeReq.Payload.OrgId, privateComputeReq.SignPair)
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	if checkResult.ContractResult == nil {
		modules.FailResponse(c, checkResult.Message)
		return
	}

	if len(checkResult.ContractResult.Result) == 0 {
		modules.FailResponse(c, checkResult.ContractResult.Message)
		return
	}

	rlpData := make([]byte, hex.DecodedLen(len(privateComputeReq.Payload.PrivateRlpData)))
	_, err = hex.Decode(rlpData, []byte(privateComputeReq.Payload.PrivateRlpData))
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	codeHash, err := hex.DecodeString(privateComputeReq.Payload.CodeHash)
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}
	loggers.Debugf("Receive private compute request: %+v, original code hash:%v, after hex decoded hash: %v\n",
		privateComputeReq, []byte(privateComputeReq.Payload.CodeHash), codeHash)
	result, err := cmclient.CmC.GetContract(privateComputeReq.Payload.ContractName, string(codeHash))
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	if len(result.ContractCode) == 0 {
		modules.FailResponse(c, "Please create contract first")
		return
	}
	//byteCodes := make([]byte, hex.DecodedLen(len(result.ContractCode)))
	//_, err = hex.Decode(byteCodes, result.ContractCode)
	//if err != nil {
	//	modules.FailResponse(c, err.Error())
	//	return
	//}
	password := make([]byte, hex.DecodedLen(len(privateComputeReq.Payload.Passwd)))
	_, err = hex.Decode(password, []byte(privateComputeReq.Payload.Passwd))
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	reqBytes, err := privateComputeReq.Marshal()
	if err != nil {
		modules.FailResponse(c, err.Error())
		return
	}
	resultStruct := &common.ContractResult{}
	enclaveResult, err := bridge.EcallInvoke(bridge.GlobalEid, privateComputeReq.Payload.ContractName,
		result.Version, codeHash, rlpData, password, result.ContractCode, reqBytes, false)
	if err != nil {
		resultStruct.Message = err.Error()
		modules.FailResponse(c, err.Error())
		return
	}
	// don't save query request
	if len(enclaveResult.RwSet.TxWrites) == 0 {
		modules.SuccessResponse(c, "Success", string(enclaveResult.Result))
		return
	}
	//resultStruct.Code = common.ContractResultCode(enclaveResult.Code)
	resultStruct.Code = uint32(enclaveResult.Code)
	resultStruct.Result = enclaveResult.Result
	resultStruct.GasUsed = uint64(enclaveResult.Gas)

	//use sdk to send result data
	rsp, err := cmclient.CmC.SaveData(privateComputeReq.Payload.ContractName, enclaveResult.Version, false,
		enclaveResult.CodeHash, enclaveResult.ReportHash, resultStruct, nil, "", enclaveResult.RwSet,
		enclaveResult.Sign, nil, reqBytes, true, 10000)
	if err != nil {
		resultStruct.Message = err.Error()
		modules.FailResponse(c, err.Error())
		return
	}
	if rsp == nil {
		err := errors.New(" Sava result to chainMaker failed")
		modules.FailResponse(c, err.Error())
		return
	}
	loggers.Info(rsp)
	modules.SuccessResponse(c, "Success", hex.EncodeToString(enclaveResult.Result))
}

func verifyClientSign(clientCert []byte, payLoadBytes []byte, clientSign []byte) (bool, error) {
	cert, err := ParseCert(clientCert)
	if err != nil {
		loggers.Error("parse cert err is:", err.Error())
		return false, err
	}

	publicKeyPem, err := cert.PublicKey.String()
	if err != nil {
		loggers.Error("public key string err is:", err.Error())
		return false, err
	}

	pk, err := asym.PublicKeyFromPEM([]byte(publicKeyPem))
	if err != nil {
		loggers.Error("publicKey from Pem cert err is:", err.Error())
		return false, err
	}

	var opts crypto.SignOpts
	hashAlgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		return false, fmt.Errorf("invalid algorithm: %v", err.Error())
	}

	opts.Hash = hashAlgo
	opts.UID = crypto.CRYPTO_DEFAULT_UID

	return pk.VerifyWithOpts(payLoadBytes, clientSign, &opts)
}

func ParseCert(crtPEM []byte) (*bcx509.Certificate, error) {
	certBlock, _ := pem.Decode(crtPEM)
	if certBlock == nil {
		return nil, fmt.Errorf("decode pem failed, invalid certificate")
	}

	cert, err := bcx509.ParseCertificate(certBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("x509 parse cert failed, %s", err.Error())
	}

	return cert, nil
}

func multiVerifyUserClient(signPair []*syscontract.SignInfo, payLoadBytes []byte) error {
	for _, info := range signPair {
		//verify user client
		certBytes, err := hex.DecodeString(info.Cert)
		clientSignBytes, err := hex.DecodeString(info.ClientSign)

		loggers.Debug("server certBytes is:", certBytes)
		verifyResult, err := verifyClientSign(certBytes, payLoadBytes, clientSignBytes)
		if err != nil {
			return errors.New("verify user client err :" + err.Error())
		}

		if !verifyResult {
			return errors.New(" verify user client failed !")
		}
	}
	return nil
}

func verifyComputeRequest(request *syscontract.PrivateComputeRequest) error {
	if len(request.SignPair) == 0 {
		return fmt.Errorf("sign pair should not be nil, request: %v", request)
	}
	if request.Payload == nil {
		return fmt.Errorf("payload should not be nil, request: %v", request)
	}
	if len(request.Payload.ContractName) == 0 {
		return fmt.Errorf("contract name should not be empty, request: %v", request)
	}
	if len(request.Payload.CodeHash) == 0 {
		return fmt.Errorf("code hash should not be empty, request: %v", request)
	}
	if len(request.Payload.Passwd) == 0 {
		return fmt.Errorf("passwd should not be empty, request: %v", request)
	}
	if len(request.Payload.OrgId) == 0 {
		return fmt.Errorf("org id should not be empty, request: %v", request)
	}
	if len(request.Payload.TimeStamp) == 0 {
		return fmt.Errorf("time stamp should not be empty, request: %v", request)
	}
	timeStamp, err := strconv.ParseInt(request.Payload.TimeStamp, 10, 64)
	if err != nil {
		return fmt.Errorf("time stamp can not parse int, time stamp: %v", timeStamp)
	}
	currentTime := time.Now().Unix()
	loggers.Debugf("current timestamp: %v and request timestamp: %v", currentTime, timeStamp)
	if currentTime-timeStamp > config.CmcConfig.ValidTime || currentTime-timeStamp < -config.CmcConfig.ValidTime {
		return fmt.Errorf(" time stamp exceeded the valid time, time stamp: %v, now time:%v", timeStamp, currentTime)
	}
	return nil
}
