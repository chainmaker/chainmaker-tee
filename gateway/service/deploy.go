/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

import (
    "chainmaker.org/chainmaker/pb-go/v2/common"
    "chainmaker.org/chainmaker/pb-go/v2/syscontract"
    "encoding/hex"
    "errors"
    "fmt"
    "gateway/bridge"
    "gateway/cmclient"
    "gateway/config"
    "gateway/loggers"
    "gateway/modules"
    "github.com/gin-gonic/gin"
    "strconv"
    "time"
)

func Deploy(c *gin.Context)  {
    defer func() {
        if err := recover(); err != nil {
            modules.FailResponse(c, err.(error).Error())
        }
    }()

    privateDeployReq := &syscontract.PrivateDeployRequest{}
    if err := c.ShouldBind(privateDeployReq); err != nil {
        modules.FailResponse(c, err.Error())
        return
    }
    if err := verifyDeployRequest(privateDeployReq); err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    payloadBytes, err := privateDeployReq.Payload.Marshal()
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    //verify user client
    if err = multiVerifyUserClient(privateDeployReq.SignPair, payloadBytes); err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    reqBytes, err := privateDeployReq.Marshal()
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    //verify gateway client
    checkResult, err := cmclient.CmC.CheckCallerCertAuth(hex.EncodeToString(payloadBytes),
        privateDeployReq.Payload.OrgId, privateDeployReq.SignPair)
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    if checkResult.ContractResult == nil {
        modules.FailResponse(c, checkResult.Message)
        return
    }

    if len(checkResult.ContractResult.Result) == 0 {
        modules.FailResponse(c, checkResult.ContractResult.Message)
        return
    }

    rlpData := make([]byte, hex.DecodedLen(len(privateDeployReq.Payload.PrivateRlpData)))
    _, err = hex.Decode(rlpData, []byte(privateDeployReq.Payload.PrivateRlpData))
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }
    byteCodes := make([]byte, hex.DecodedLen(len(privateDeployReq.Payload.CodeBytes)))
    _, err = hex.Decode(byteCodes, []byte(privateDeployReq.Payload.CodeBytes))
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }
    password := make([]byte, hex.DecodedLen(len(privateDeployReq.Payload.Passwd)))
    _, err = hex.Decode(password, []byte(privateDeployReq.Payload.Passwd))
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }
    codeHash, err := hex.DecodeString(privateDeployReq.Payload.CodeHash)
    if err != nil {
        modules.FailResponse(c, err.Error())
        return
    }

    resultStruct := &common.ContractResult{}
    enclaveResult, err := bridge.EcallInvoke(bridge.GlobalEid, privateDeployReq.Payload.ContractName,
        privateDeployReq.Payload.ContractVersion, codeHash, rlpData, password, byteCodes, reqBytes, true)
    if err != nil {
        resultStruct.Message = err.Error()
        modules.FailResponse(c, err.Error())
        return
    }
    //resultStruct.Code = common.ContractResultCode(enclaveResult.Code)
    resultStruct.Code = uint32(enclaveResult.Code)
    resultStruct.Result = enclaveResult.Result
    resultStruct.GasUsed = uint64(enclaveResult.Gas)
    loggers.Infof("code header len: %d, code header: %v, \ncode body len: %d, code body: %v\n",
        len(enclaveResult.CodeHeader), enclaveResult.CodeHeader, len(resultStruct.Result), resultStruct.Result)
    //use sdk to send result data
    rsp, err := cmclient.CmC.SaveData(privateDeployReq.Payload.ContractName, enclaveResult.Version, true,
       enclaveResult.CodeHash, enclaveResult.ReportHash, resultStruct, enclaveResult.CodeHeader, "", enclaveResult.RwSet,
       enclaveResult.Sign, nil, reqBytes, true, 10000)
    if err != nil {
        resultStruct.Message = err.Error()
        modules.FailResponse(c, err.Error())
        return
    }
    if rsp == nil {
        err := errors.New(" Sava result to chainMaker failed")
        modules.FailResponse(c, err.Error())
        return
    }
    loggers.Info(rsp)
    modules.SuccessResponse(c, "Success", hex.EncodeToString(enclaveResult.Result))
}

func verifyDeployRequest(request *syscontract.PrivateDeployRequest) error {
    if len(request.SignPair) == 0 {
        return fmt.Errorf("sign pair should not be nil, request: %v", request)
    }
    if request.Payload == nil {
        return fmt.Errorf("payload should not be nil, request: %v", request)
    }
    if len(request.Payload.ContractName) == 0 {
        return fmt.Errorf("contract name should not be empty, request: %v", request)
    }
    if len(request.Payload.ContractVersion) == 0 {
        return fmt.Errorf("contract version should not be empty, request: %v", request)
    }
    if len(request.Payload.PrivateRlpData) == 0 {
        return fmt.Errorf("private_rlp_data should not be empty, request: %v", request)
    }
    if len(request.Payload.CodeBytes) == 0 {
        return fmt.Errorf("code bytes should not be empty, request: %v", request)
    }
    if len(request.Payload.CodeHash) == 0 {
        return fmt.Errorf("code hash should not be empty, request: %v", request)
    }
    if len(request.Payload.Passwd) == 0 {
        return fmt.Errorf("passwd should not be empty, request: %v", request)
    }
    if len(request.Payload.OrgId) == 0 {
        return fmt.Errorf("org id should not be empty, request: %v", request)
    }
    if len(request.Payload.TimeStamp) == 0 {
       return fmt.Errorf("time stamp should not be empty, request: %v", request)
    }
    timeStamp, err := strconv.ParseInt(request.Payload.TimeStamp, 10, 64)
    if err != nil{
        return fmt.Errorf("time stamp can not parse int, time stamp: %v",timeStamp)
    }
    currentTime := time.Now().Unix()
    loggers.Debugf("current timestamp: %v and request timestamp: %v",currentTime,timeStamp)
    if currentTime-timeStamp > config.CmcConfig.ValidTime || currentTime-timeStamp < -config.CmcConfig.ValidTime {
        return fmt.Errorf(" time stamp exceeded the valid time, time stamp: %v, now time:%v",timeStamp,currentTime)
    }
    return nil
}