/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/asym"
	"chainmaker.org/chainmaker/common/v2/crypto/asym/rsa"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/modes"
	"chainmaker.org/chainmaker/common/v2/crypto/x509"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"crypto/rand"
	rsa2 "crypto/rsa"
	"crypto/sha256"
	x509_2 "crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"gateway/cmclient"
	"gateway/loggers"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/golang/protobuf/proto"
	"io/ioutil"
	"math/big"
	"strings"
	"testing"
)

const (
	chainId          = "chain1"
	userKeyFilePath  = "cert/client1.tls.key"
	userCertFilePath = "cert/client1.tls.crt"
	contractName     = "private_compute"
	orgId            = "wx-org1.chainmaker.org"
)

func TestCompute(t *testing.T) {
	type args struct {
		req *syscontract.PrivateComputeRequest
	}

	if err := cmclient.InitCmClient(); err != nil {
		panic(err)
	}

	privateRlpData, codeHash, passwd := buildPrivateReq()
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "test1",
			args: args{
				req: &syscontract.PrivateComputeRequest{
					Payload: &syscontract.PrivateComputePayload{
						PrivateRlpData: privateRlpData,
						Passwd:         passwd,
						ContractName:   contractName,
						CodeHash:       codeHash,
						OrgId:          []string{orgId},
					},
				},
			},
			want: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pair, err := buildSignPair(tt.args.req)
			if err != nil {
				t.Error(err)
				return
			}

			tt.args.req.SignPair = append(tt.args.req.SignPair, pair...)
			payloadBytes, err := tt.args.req.Payload.Marshal()
			if err != nil {
				t.Error(err)
				return
			}

			txResponse, err := cmclient.CmC.CheckCallerCertAuth(hex.EncodeToString(payloadBytes),
				tt.args.req.Payload.OrgId, tt.args.req.SignPair)
			if err != nil {
				t.Error(err)
				return
			}

			if txResponse.ContractResult == nil {
				t.Error(txResponse.Message)
			}

			if len(txResponse.ContractResult.Message) != len(tt.want) {
				t.Error("not passed")
			}
		})
	}
}

func buildSignPair(req *syscontract.PrivateComputeRequest) ([]*syscontract.SignInfo, error) {
	payloadBytes, err := req.Payload.Marshal()
	if err != nil {
		loggers.Errorf("create payload bytes json marshal  err is:", err.Error())
		return nil, err
	}

	privateKey, err := getPrivateKey(userKeyFilePath)
	if err != nil {
		loggers.Errorf("get private key err is:%v", err.Error())
		return nil, err
	}

	userCertBytes, err := ioutil.ReadFile(userCertFilePath)
	if err != nil {
		loggers.Errorf("read user cert file bytes err is:%v", err.Error())
		return nil, err
	}

	loggers.Info("client user cert bytes is:", userCertBytes)

	userCert, err := ParseCert(userCertBytes)
	if err != nil {
		loggers.Errorf("parse cert err is:%v", err.Error())
		return nil, err
	}

	signedBytes, err := signCheckCallerPayload(payloadBytes, privateKey, userCert)
	if err != nil {
		loggers.Errorf("sign check caller payload err is:%v", err.Error())
		return nil, err
	}

	loggers.Infof("PayloadBytes: %v\n SignedBytes: %v ", payloadBytes, signedBytes)

	signInfo := &syscontract.SignInfo{
		ClientSign: hex.EncodeToString(signedBytes),
		Cert:       hex.EncodeToString(userCertBytes),
	}

	req.SignPair = append(req.SignPair, signInfo)

	return req.SignPair, nil
}

func signCheckCallerPayload(payLoadBytes []byte, privateKey crypto.PrivateKey, cert *x509.Certificate) ([]byte, error) {

	signBytes, err := signPayload(privateKey, cert, payLoadBytes)
	if err != nil {
		return nil, fmt.Errorf("SignPayload failed, %s", err.Error())
	}

	return signBytes, nil
}

func signPayload(privateKey crypto.PrivateKey, cert *bcx509.Certificate, msg []byte) ([]byte, error) {
	return SignTx(privateKey, cert, msg)
}

func buildPrivateReq() (privateRlpData, codeHash, Passwd string) {
	filePath := "tools/contract_with_string"
	abiPath := "tools/abi_with_string.json"
	codes, err := ioutil.ReadFile(filePath)
	if err != nil {
		loggers.Errorf("read contract file failed, ", err.Error())
		return
	}

	codeBytes := make([]byte, hex.DecodedLen(len(codes)))
	_, err = hex.Decode(codeBytes, codes)
	if err != nil {
		loggers.Error("hex decode codes failed, ", err.Error())
		return
	}

	loggers.Info("contract codes: ")
	for i := 0; i < len(codeBytes); i++ {
		loggers.Infof("0x%02x,", codeBytes[i])
	}
	loggers.Info("=======contract codes end======")

	abiJson, err := ioutil.ReadFile(abiPath)
	myAbi, err := abi.JSON(strings.NewReader(string(abiJson)))
	dataBytes, err := myAbi.Pack("add", big.NewInt(5), big.NewInt(5))

	loggers.Info("calldata: ")
	for i := 0; i < len(dataBytes); i++ {
		loggers.Infof("0x%02x,", dataBytes[i])
	}
	loggers.Info()

	rkey, cdata, err := cryptoData(dataBytes)
	if err != nil {
		loggers.Error("cryptoData failed: ", err)
		return
	}

	h := sha256.New()
	h.Write(codes)
	codesHash := h.Sum(nil)
	loggers.Info("code bytes: ", codes)
	loggers.Infof("code hash: %v:%v\n", codesHash, sha256.Sum256(codes))

	return hex.EncodeToString(cdata), hex.EncodeToString(codesHash[:]), hex.EncodeToString(rkey)
}

func cryptoData(dataBytes []byte) ([]byte, []byte, error) {
	var OidExtPubKey = asn1.ObjectIdentifier{1, 2, 840, 113549, 1, 12, 10, 1, 1}
	loggers.Infof("Encrypting data = \n")
	for _, v := range dataBytes {
		loggers.Infof("%02x ", v)
	}
	loggers.Infof("\n")

	pemCert, err := ioutil.ReadFile("in_teecert.pem")
	if err != nil {
		return nil, nil, errors.New("read cert error")
	}

	derCert, _ := pem.Decode(pemCert)
	if derCert == nil {
		return nil, nil, errors.New("decode cert error")
	}

	cert, err := x509.ParseCertificate(derCert.Bytes)
	if err != nil {
		return nil, nil, errors.New("parsecertificate error")
	}

	pubk, err := x509.GetExtByOid(OidExtPubKey, cert.Extensions)
	if err != nil {
		return nil, nil, err
	}
	loggers.Infof("pubkey = \n%s\n", pubk)

	block, _ := pem.Decode(pubk)
	if block == nil {
		return nil, nil, errors.New("decode pubk error")
	}

	//pk, err := x509.ParsePKCS1PublicKey(block.Bytes)
	pk, err := x509_2.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, nil, err
	}

	// 生成密钥—>aeskey
	ran := make([]byte, 16)
	_, err = rand.Read(ran)
	if err != nil {
		return nil, nil, err
	}

	aeskey := aes.AESKey{Key: ran}
	// RSA(key）—>Rkey（传）
	digest, err := aeskey.Bytes()
	if err != nil {
		return nil, nil, err
	}

	//publicKey := rsa.PublicKey{K: pk}
	publicKey := rsa.PublicKey{K: pk.(*rsa2.PublicKey)}
	rkey, err := publicKey.Encrypt(digest)
	if err != nil {
		return nil, nil, err
	}

	// AES(data+key)—>加密数据cdata（传）
	//cdata, err := aeskey.Encrypt(dataBytes)
	cdata, err := aeskey.EncryptWithOpts(dataBytes,
		&crypto.EncOpts{
			EncodingType: modes.PADDING_NONE,
			BlockMode:    modes.BLOCK_MODE_GCM,
			EnableMAC:    true,
			Hash:         0,
			Label:        nil,
			EnableASN1:   false,
		})
	if err != nil {
		return nil, nil, err
	}

	loggers.Info("AES encrypt result len:", len(cdata))
	loggers.Info("AES encrypt result (base64 encoded):", base64.StdEncoding.EncodeToString(cdata))
	loggers.Info("RSA encrypted aes key:", rkey)
	return rkey, cdata, nil
}

func CalcUnsignedTxRequestBytes(txReq *common.TxRequest) ([]byte, error) {
	if txReq == nil {
		return nil, errors.New("calc unsigned tx request bytes error, tx == nil")
	}

	return CalcUnsignedTxBytes(&common.Transaction{
		Payload: txReq.Payload,
	})
}

// calculate unsigned transaction bytes [header bytes || request payload bytes]
func CalcUnsignedTxBytes(t *common.Transaction) ([]byte, error) {
	if t == nil {
		return nil, errors.New("calc unsigned tx bytes error, tx == nil")
	}

	payloadBytes, err := proto.Marshal(t.Payload)
	if err != nil {
		return nil, err
	}

	return payloadBytes, nil
}

func SignTx(privateKey crypto.PrivateKey, cert *bcx509.Certificate, msg []byte) ([]byte, error) {
	var opts crypto.SignOpts
	hashalgo, err := bcx509.GetHashFromSignatureAlgorithm(cert.SignatureAlgorithm)
	if err != nil {
		return nil, fmt.Errorf("invalid algorithm: %v", err)
	}

	opts.Hash = hashalgo
	opts.UID = crypto.CRYPTO_DEFAULT_UID

	return privateKey.SignWithOpts(msg, &opts)
}

func getPrivateKey(userKeyFilePath string) (crypto.PrivateKey, error) {

	// 从私钥文件读取用户私钥，转换为privateKey对象
	skBytes, err := ioutil.ReadFile(userKeyFilePath)
	if err != nil {
		return nil, fmt.Errorf("read user key file failed, %s", err)
	}

	privateKey, err := asym.PrivateKeyFromPEM(skBytes, nil)
	if err != nil {
		return nil, fmt.Errorf("parse user key file to privateKey obj failed, %s", err)
	}

	return privateKey, nil
}
