/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

import (
    "gateway/modules"
    "github.com/gin-gonic/gin"
)

func Healthy (c *gin.Context) {
    modules.SuccessResponse(c, "OK", "I am healthy!")
}