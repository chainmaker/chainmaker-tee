/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package service

import (
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"encoding/hex"
	"encoding/json"
	"errors"
	"gateway/loggers"

	//"encoding/json"
	"fmt"
	"gateway/bridge"
	//"gateway/cmclient"
	"gateway/modules"
	"github.com/gin-gonic/gin"
)

func RemoteAttestation(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			modules.FailResponse(c, err.(error).Error())
		}
	}()

	loggers.Info("begin RemoteAttestation Handling ==> ")
	remoteAttestationReq := &syscontract.RemoteAttestationRequest{}
	if err := c.ShouldBind(remoteAttestationReq); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	loggers.Info("begin Argument checking ==> ")
	if err := verifyRemoteAttestationRequest(remoteAttestationReq); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	loggers.Info("verify user client sign")
	payloadBytes, err := json.Marshal(remoteAttestationReq.Payload)
	if err != nil {
		modules.FailResponse(c, fmt.Sprintf("json.Marshal(...) error: %v", err.Error()))
		return
	}

	if err = multiVerifyUserClient(remoteAttestationReq.SignPair, payloadBytes); err != nil {
		modules.FailResponse(c, err.Error())
		return
	}

	loggers.Info("call remote attestation procedure")
	proof, err := bridge.EcallRemoteAttestationProve(bridge.GlobalEid, remoteAttestationReq.Payload.Challenge)

	if err != nil {
		modules.FailResponse(c, fmt.Sprintf("bridge.EcallRemoteAttestationProve(...) error: %v", err))
		return
	}
	proofHex := hex.EncodeToString(proof)

	modules.SuccessResponse(c, "Success", string(proofHex))
}

func verifyRemoteAttestationRequest(request *syscontract.RemoteAttestationRequest) error {
	if request.Payload == nil {
		return errors.New("missing 'pay_load' parameter")
	}
	challenge := request.Payload.Challenge
	if len(challenge) == 0 {
		return errors.New("missing 'challenge' parameter")
	}

	return nil
}
