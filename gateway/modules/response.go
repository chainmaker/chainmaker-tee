/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package modules

import (
    "github.com/gin-gonic/gin"
    "net/http"
)

type Response struct {
    Code int32
    Message string
    Data string
}

var (
	CodeFailed int32 = -1
	CodeSuccess int32 = 0
)

func FailResponse(ctx *gin.Context, message string) {
    ctx.JSON(http.StatusOK, &Response{CodeFailed, message, ""})
}

func SuccessResponse(ctx *gin.Context, message string, data string) {
    ctx.JSON(http.StatusOK, &Response{CodeSuccess, message, data})
}