/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package modules

type RemoteAttestationRequest struct {
	ClientSign string `json:"client_sign"`
	Cert string `json:"cert"`
	Payload *RemoteAttestationRequestPayload `json:"payload"`
}

type RemoteAttestationRequestPayload struct {
	Challenge string `json:"challenge"`
	OrgId string `json:"org_id"`
}